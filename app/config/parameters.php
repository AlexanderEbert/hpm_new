<?php return array (
  'parameters' => 
  array (
    'database_host' => '127.0.0.1',
    'database_port' => '',
    'database_name' => 'shop',
    'database_user' => 'root',
    'database_password' => 'shop',
    'database_prefix' => 'ps_',
    'database_engine' => 'InnoDB',
    'mailer_transport' => 'smtp',
    'mailer_host' => '127.0.0.1',
    'mailer_user' => NULL,
    'mailer_password' => NULL,
    'secret' => 'sYI1d3XS7sxa7QuJxKn4r2fJLsSARTkN5fJB88kEgnBPNfRBRd9BD1Fs',
    'ps_caching' => 'CacheMemcache',
    'ps_cache_enable' => false,
    'ps_creation_date' => '2017-01-31',
    'locale' => 'de-DE',
    'cookie_key' => 'nKnqIX177ODDUsx0Qy2311Ytbxe8tLuWkQEcbZwW8HI332MsTOL65zBT',
    'cookie_iv' => 'mZ60y6Z9',
    'new_cookie_key' => 'def00000eed85c0b6d6dd8ecb7f621151af39f65c39e09e10f56be0c9206484a65343f1fa1782ffd7dd6e7def89c44960e814ad209313c5c2959fc3fd06c41d78e56d4fd',
  ),
);