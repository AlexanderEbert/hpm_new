<?php /* Smarty version Smarty-3.1.19, created on 2017-01-31 13:07:09
         compiled from "/var/www/warenkorb/modules/packlink/views/templates/hook/back.tpl" */ ?>
<?php /*%%SmartyHeaderCode:58952424558907ded851026-75908254%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7689deb48c38b43b0dced27e8bc57d76303ee9dd' => 
    array (
      0 => '/var/www/warenkorb/modules/packlink/views/templates/hook/back.tpl',
      1 => 1485864424,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '58952424558907ded851026-75908254',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'simple_link' => 0,
    'carrier_link' => 0,
    'tab_name' => 0,
    'pl_aide' => 0,
    'generate_api' => 0,
    'module_link' => 0,
    'PL_API_KEY' => 0,
    'link_pro_addr' => 0,
    'show_address' => 0,
    'warehouses' => 0,
    'warehouse' => 0,
    'link_status' => 0,
    'order_state' => 0,
    'state' => 0,
    'status_awaiting' => 0,
    'status_pending' => 0,
    'status_ready' => 0,
    'status_transit' => 0,
    'status_delivered' => 0,
    'link_units' => 0,
    'weight' => 0,
    'unit_weight' => 0,
    'length' => 0,
    'unit_length' => 0,
    'packlink_import' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58907dedad1472_67063007',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58907dedad1472_67063007')) {function content_58907dedad1472_67063007($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/var/www/warenkorb/vendor/prestashop/smarty/plugins/function.html_options.php';
?>


<div class="container-fluid pl_navigation">
<img src="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['simple_link']->value,'html','UTF-8');?>
views/img/logo-pl.svg" width="250px;"><br /><br />
  <h4 class="inscription"><?php echo smartyTranslate(array('s'=>'Ship your paid orders easily at the best prices with Packlink PRO. No account yet? It only takes few seconds to ','mod'=>'packlink'),$_smarty_tpl);?>

       <a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['carrier_link']->value,'htmlall','UTF-8');?>
" target="_blank"><?php echo smartyTranslate(array('s'=>'register online','mod'=>'packlink'),$_smarty_tpl);?>
</a><?php echo smartyTranslate(array('s'=>' .','mod'=>'packlink'),$_smarty_tpl);?>
</h4><br />
  <ul class="nav nav-pills">
    <li <?php if ($_smarty_tpl->tpl_vars['tab_name']->value=="home_settings") {?> class="active"<?php }?>><a data-toggle="pill" href="#pl_key"><span><?php echo smartyTranslate(array('s'=>'Setup','mod'=>'packlink'),$_smarty_tpl);?>
</span></a></li>
    <li <?php if ($_smarty_tpl->tpl_vars['tab_name']->value=="address_form") {?> class="active"<?php }?>><a data-toggle="pill" href="#pl_address"><span><?php echo smartyTranslate(array('s'=>'Sender address','mod'=>'packlink'),$_smarty_tpl);?>
</span></a></li>
    <li <?php if ($_smarty_tpl->tpl_vars['tab_name']->value=="status_form") {?> class="active"<?php }?>><a data-toggle="pill" href="#pl_status"><span><?php echo smartyTranslate(array('s'=>'Order statuses','mod'=>'packlink'),$_smarty_tpl);?>
</span></a></li>
    <li <?php if ($_smarty_tpl->tpl_vars['tab_name']->value=="units_form") {?> class="active"<?php }?> id="li-units"><a data-toggle="pill" href="#pl_units"><span><?php echo smartyTranslate(array('s'=>'Data unit','mod'=>'packlink'),$_smarty_tpl);?>
</span></a></li>
    <li id="pl-aide"><a target="_blank" href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['pl_aide']->value,'htmlall','UTF-8');?>
"><span><?php echo smartyTranslate(array('s'=>'Help center','mod'=>'packlink'),$_smarty_tpl);?>
</span></a></li>
  </ul>
  <br /><br />
  <div class="tab-content">
    <div id="pl_key" class="tab-pane fade <?php if ($_smarty_tpl->tpl_vars['tab_name']->value=='home_settings') {?> in active<?php }?>">
              <p class="tab_title"><span><?php echo smartyTranslate(array('s'=>'Packlink PRO connection','mod'=>'packlink'),$_smarty_tpl);?>
</span></p>
              <p class="tab_description"><?php echo smartyTranslate(array('s'=>'An API key associated with your Packlink PRO account must be indicated in the field below in order to import your paid orders automatically from PrestaShop. ','mod'=>'packlink'),$_smarty_tpl);?>

                   <a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['generate_api']->value,'htmlall','UTF-8');?>
" target="_blank"><?php echo smartyTranslate(array('s'=>'Generate API key now.','mod'=>'packlink'),$_smarty_tpl);?>
</a></p>
                <form class="form-horizontal" role="form" action = "<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_link']->value,'htmlall','UTF-8');?>
" method="post">
                  <div class="form-group col-sm-9">
                        <label class="control-label col-sm-3" for="PL_API_KEY"><?php echo smartyTranslate(array('s'=>'Packlink PRO API Key','mod'=>'packlink'),$_smarty_tpl);?>
</label>
                        <div class="col-sm-7">
                            <input id="PL_API_KEY" name="PL_API_KEY" value="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['PL_API_KEY']->value,'htmlall','UTF-8');?>
" size="100" maxlength="100" class="form-control" required="required"/>
                        </div>
                        <input type="hidden" name="PL_tab_name" value="home_settings" />
                  </div>
                  <div class="form-group col-sm-9"> 
                  <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" name="submit-query" value="submit" class="btn btn-info"><?php echo smartyTranslate(array('s'=>'Save','mod'=>'packlink'),$_smarty_tpl);?>
</button>
                  </div></div>
                </form>

    </div>
    <div id="pl_address" class="tab-pane fade <?php if ($_smarty_tpl->tpl_vars['tab_name']->value=='address_form') {?> in active<?php }?>">
           <p class="tab_title"><span><?php echo smartyTranslate(array('s'=>'“Ship from” address(es)','mod'=>'packlink'),$_smarty_tpl);?>
</span></p>
            <p class="tab_description"><?php echo smartyTranslate(array('s'=>'“Ship from” address(es) save(s) you time by prefilling shipping details in Packlink PRO. You can configure and edit them from','mod'=>'packlink'),$_smarty_tpl);?>
 <a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link_pro_addr']->value,'htmlall','UTF-8');?>
" target="_blank"><?php echo smartyTranslate(array('s'=>'Packlink PRO settings.','mod'=>'packlink'),$_smarty_tpl);?>
</a></p>
            <?php if ($_smarty_tpl->tpl_vars['show_address']->value) {?>
                <?php  $_smarty_tpl->tpl_vars['warehouse'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['warehouse']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['warehouses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['warehouse']->key => $_smarty_tpl->tpl_vars['warehouse']->value) {
$_smarty_tpl->tpl_vars['warehouse']->_loop = true;
?>
                    <div class="well col-sm-3 warehouse <?php if ($_smarty_tpl->tpl_vars['warehouse']->value->default_selection) {?>default-wh <?php } else { ?>other-wh<?php }?>">
                        <header>
                            <p class="title"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['warehouse']->value->alias,'htmlall','UTF-8');?>
</p>
                            <?php if ($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['warehouse']->value->default_selection,'htmlall','UTF-8')) {?>
                                <p class="btn-default"><?php echo smartyTranslate(array('s'=>'By default','mod'=>'packlink'),$_smarty_tpl);?>
</p>
                            <?php }?>
                        </header>
                        <p><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['warehouse']->value->address,'htmlall','UTF-8');?>
 <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['warehouse']->value->address2,'htmlall','UTF-8');?>
</p> 
                        <p><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['warehouse']->value->postal_code,'htmlall','UTF-8');?>
 <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['warehouse']->value->city,'htmlall','UTF-8');?>
</p> 
                        <p><?php echo smartyTranslate(array('s'=>'Telephone: ','mod'=>'packlink'),$_smarty_tpl);?>
<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['warehouse']->value->phone,'htmlall','UTF-8');?>
</p>   
                    </div>
                <?php } ?>
            <?php } else { ?>
                 
                 <div class="form-group col-sm-8"> 
                  <div class="col-sm-offset-2 col-sm-8">
                      <p><?php echo smartyTranslate(array('s'=>'No "ship from" address configured in Packlink PRO!','mod'=>'packlink'),$_smarty_tpl);?>
</p>
                      <a  href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link_pro_addr']->value,'htmlall','UTF-8');?>
" target="_blank" type="submit" value="submit" class="btn btn-info"><?php echo smartyTranslate(array('s'=>'Add','mod'=>'packlink'),$_smarty_tpl);?>
</a>
                </div></div>
            <?php }?>
    </div>


    <div id="pl_status" class="tab-pane fade <?php if ($_smarty_tpl->tpl_vars['tab_name']->value=='status_form') {?> in active<?php }?>">
          <p class="tab_title"><span><?php echo smartyTranslate(array('s'=>'Status synchronization','mod'=>'packlink'),$_smarty_tpl);?>
</span></p>
          <p class="tab_description"><a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link_status']->value,'htmlall','UTF-8');?>
" target="_blank"><?php echo smartyTranslate(array('s'=>'PrestaShop order statuses','mod'=>'packlink'),$_smarty_tpl);?>
</a> <?php echo smartyTranslate(array('s'=>'are synchronized with Packlink PRO shipping statuses as configured in the matching table below.','mod'=>'packlink'),$_smarty_tpl);?>
<br>
          <?php echo smartyTranslate(array('s'=>'Each time a shipment status changes in Packlink PRO, the status of its associated order in PrestaShop is updated accordingly.','mod'=>'packlink'),$_smarty_tpl);?>
</p>
            <form class="form-horizontal" role="form" action = "<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_link']->value,'htmlall','UTF-8');?>
" method="post">
              <div class="form-group col-sm-8">
                    <div style="margin-bottom: 35px;"><label class="control-label col-sm-2"></label>
                    <label class="control-label col-sm-4 col_title"><?php echo smartyTranslate(array('s'=>'Packlink PRO shipping status','mod'=>'packlink'),$_smarty_tpl);?>
</label>
                    <label class="control-label col-sm-1 egal"></label>
                    <label class="control-label col-sm-5 col_title"><?php echo smartyTranslate(array('s'=>'PrestaShop order status','mod'=>'packlink'),$_smarty_tpl);?>
</label></div>
                    <label class="control-label col-sm-2" for="select_awaiting"><?php echo smartyTranslate(array('s'=>'Status #1','mod'=>'packlink'),$_smarty_tpl);?>
</label>
                    <div class="col-sm-4"><input value="<?php echo smartyTranslate(array('s'=>'Pending','mod'=>'packlink'),$_smarty_tpl);?>
" class="form-control" readonly="readonly" /></div>
                    <label class="control-label col-sm-1 egal" for="select_awaiting">=</label>
                    <div class="col-sm-5">
                    <select class="form-control" id="select_awaiting" name="select_awaiting">
                        <option value="0"><?php echo smartyTranslate(array('s'=>'(None)','mod'=>'packlink'),$_smarty_tpl);?>
</option>
                        <?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_state']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
                            <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['state']->value['id_order_state'],'htmlall','UTF-8');?>
<?php $_tmp1=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['state']->value['name'],'htmlall','UTF-8');?>
<?php $_tmp2=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['status_awaiting']->value,"html","UTF-8");?>
<?php $_tmp3=ob_get_clean();?><?php echo smarty_function_html_options(array('values'=>$_tmp1,'output'=>$_tmp2,'selected'=>$_tmp3),$_smarty_tpl);?>

                        <?php } ?>
                    </select></div>
                    <label class="control-label col-sm-2" for="pending"><?php echo smartyTranslate(array('s'=>'Status #2','mod'=>'packlink'),$_smarty_tpl);?>
</label>
                    <div class="col-sm-4"><input value="<?php echo smartyTranslate(array('s'=>'Processing','mod'=>'packlink'),$_smarty_tpl);?>
" class="form-control" readonly="readonly" /></div>
                    <label class="control-label col-sm-1 egal" for="pending">=</label>
                    <div class="col-sm-5">
                    <select class="form-control" id="select_pending" name="select_pending">
                        <option value="0"><?php echo smartyTranslate(array('s'=>'(None)','mod'=>'packlink'),$_smarty_tpl);?>
</option>
                        <?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_state']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
                            <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['state']->value['id_order_state'],'htmlall','UTF-8');?>
<?php $_tmp4=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['state']->value['name'],'htmlall','UTF-8');?>
<?php $_tmp5=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['status_pending']->value,"html","UTF-8");?>
<?php $_tmp6=ob_get_clean();?><?php echo smarty_function_html_options(array('values'=>$_tmp4,'output'=>$_tmp5,'selected'=>$_tmp6),$_smarty_tpl);?>

                        <?php } ?>
                    </select></div>
                    <label class="control-label col-sm-2" for="ready"><?php echo smartyTranslate(array('s'=>'Status #3','mod'=>'packlink'),$_smarty_tpl);?>
</label>
                    <div class="col-sm-4"><input value="<?php echo smartyTranslate(array('s'=>'Ready for shipping','mod'=>'packlink'),$_smarty_tpl);?>
" class="form-control" readonly="readonly" /></div>
                    <label class="control-label col-sm-1 egal" for="select_ready">=</label>
                    <div class="col-sm-5">
                    <select class="form-control" id="select_ready" name="select_ready">
                        <option value="0"><?php echo smartyTranslate(array('s'=>'(None)','mod'=>'packlink'),$_smarty_tpl);?>
</option>
                        <?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_state']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
                            <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['state']->value['id_order_state'],'htmlall','UTF-8');?>
<?php $_tmp7=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['state']->value['name'],'htmlall','UTF-8');?>
<?php $_tmp8=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['status_ready']->value,"html","UTF-8");?>
<?php $_tmp9=ob_get_clean();?><?php echo smarty_function_html_options(array('values'=>$_tmp7,'output'=>$_tmp8,'selected'=>$_tmp9),$_smarty_tpl);?>

                        <?php } ?>
                    </select></div>
                    <label class="control-label col-sm-2" for="transit"><?php echo smartyTranslate(array('s'=>'Status #4','mod'=>'packlink'),$_smarty_tpl);?>
</label>
                    <div class="col-sm-4"><input value="<?php echo smartyTranslate(array('s'=>'In transit','mod'=>'packlink'),$_smarty_tpl);?>
" class="form-control" readonly="readonly" /></div>
                    <label class="control-label col-sm-1 egal" for="select_transit">=</label>
                    <div class="col-sm-5">
                    <select class="form-control" id="select_transit" name="select_transit">
                        <option value="0"><?php echo smartyTranslate(array('s'=>'(None)','mod'=>'packlink'),$_smarty_tpl);?>
</option>
                        <?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_state']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
                            <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['state']->value['id_order_state'],'htmlall','UTF-8');?>
<?php $_tmp10=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['state']->value['name'],'htmlall','UTF-8');?>
<?php $_tmp11=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['status_transit']->value,"html","UTF-8");?>
<?php $_tmp12=ob_get_clean();?><?php echo smarty_function_html_options(array('values'=>$_tmp10,'output'=>$_tmp11,'selected'=>$_tmp12),$_smarty_tpl);?>

                        <?php } ?>
                    </select></div>
                    <label class="control-label col-sm-2" for="delivered"><?php echo smartyTranslate(array('s'=>'Status #5','mod'=>'packlink'),$_smarty_tpl);?>
</label>
                    <div class="col-sm-4"><input value="<?php echo smartyTranslate(array('s'=>'Delivered','mod'=>'packlink'),$_smarty_tpl);?>
" class="form-control" readonly="readonly" /></div>
                    <label class="control-label col-sm-1 egal" for="select_delivered">=</label>
                    <div class="col-sm-5">
                    <select class="form-control" id="select_delivered" name="select_delivered">
                        <option value="0"><?php echo smartyTranslate(array('s'=>'(None)','mod'=>'packlink'),$_smarty_tpl);?>
</option>
                        <?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_state']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
                            <?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['state']->value['id_order_state'],'htmlall','UTF-8');?>
<?php $_tmp13=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['state']->value['name'],'htmlall','UTF-8');?>
<?php $_tmp14=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['status_delivered']->value,"html","UTF-8");?>
<?php $_tmp15=ob_get_clean();?><?php echo smarty_function_html_options(array('values'=>$_tmp13,'output'=>$_tmp14,'selected'=>$_tmp15),$_smarty_tpl);?>

                        <?php } ?>
                    </select></div>
                    <input type="hidden" name="PL_tab_name" value="status_form" />


              </div>
              <div class="form-group col-sm-8"> 
                  <div class="col-sm-offset-2 col-sm-8">
                      <button type="submit" name="submit-status" value="submit" class="btn btn-info"><?php echo smartyTranslate(array('s'=>'Save','mod'=>'packlink'),$_smarty_tpl);?>
</button>
              </div></div>
            </form>
    </div>



    <div id="pl_units" class="tab-pane fade <?php if ($_smarty_tpl->tpl_vars['tab_name']->value=='units_form') {?> in active<?php }?>">
        <p class="tab_title"><span><?php echo smartyTranslate(array('s'=>'Data unit conversion ','mod'=>'packlink'),$_smarty_tpl);?>
</span></p>
            <p class="tab_description"><?php echo smartyTranslate(array('s'=>'Packlink PRO works with kilograms and centimetres. Your PrestaShop might be configured with other','mod'=>'packlink'),$_smarty_tpl);?>
<a target="_blank" href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link_units']->value,'htmlall','UTF-8');?>
"> <?php echo smartyTranslate(array('s'=>'data unit.','mod'=>'packlink'),$_smarty_tpl);?>
</a><br/>
            <?php echo smartyTranslate(array('s'=>'Please make sure the matching table below makes sense so data imported from PrestaShop to Packlink PRO corresponds.','mod'=>'packlink'),$_smarty_tpl);?>
</p>
        <form class="form-horizontal col-sm-7 unit-block" role="form" action = "<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_link']->value,'htmlall','UTF-8');?>
" method="post">
          <div class="form-group unit">
                <div style="margin-bottom:35px;">
                    <label class="control-label col-sm-4"></label>
                    <label class="control-label col-sm-2 col_title"><?php echo smartyTranslate(array('s'=>'PrestaShop','mod'=>'packlink'),$_smarty_tpl);?>
</label>
                    <label class="control-label col-sm-1 small_col"></label>
                    <label class="control-label col-sm-3 col_title"><?php echo smartyTranslate(array('s'=>'Packlink PRO','mod'=>'packlink'),$_smarty_tpl);?>
</label>
                    <label class="control-label col-sm-1 small_col"></label>
                </div>
                <label class="control-label col-sm-4" for="weight"><?php echo smartyTranslate(array('s'=>'Weight unit ','mod'=>'packlink'),$_smarty_tpl);?>
</label>
                <div class="col-sm-2"><input id="weight" name="weight" value="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['weight']->value,'htmlall','UTF-8');?>
" class="form-control" /></div>
                <label class="control-label col-sm-1 small_col" for="weight"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['unit_weight']->value,'htmlall','UTF-8');?>
 = </label>
                <div class="col-sm-2"><input id="weight" name="weight" value="1" class="form-control" disabled/></div>
                <label class="control-label col-sm-1 small_col" for="weight"><?php echo smartyTranslate(array('s'=>'kg','mod'=>'packlink'),$_smarty_tpl);?>
</label>
                <label class="control-label col-sm-4" for="length"><?php echo smartyTranslate(array('s'=>'Dimension unit ','mod'=>'packlink'),$_smarty_tpl);?>
</label>
                <div class="col-sm-2"><input id="length" name="length" value="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['length']->value,'htmlall','UTF-8');?>
" class="form-control" /></div>
                <label class="control-label col-sm-1 small_col" for="length"><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['unit_length']->value,'htmlall','UTF-8');?>
 = </label>
                <div class="col-sm-2"><input id="length" name="length" value="1" class="form-control" disabled/></div>
                <label class="control-label col-sm-1 small_col" for="length"><?php echo smartyTranslate(array('s'=>'cm','mod'=>'packlink'),$_smarty_tpl);?>
</label>
                <input type="hidden" name="PL_tab_name" value="units_form" />
          </div>
          <div class="form-group"> 
             <div class="col-sm-offset-4 col-sm-7">
                <button type="submit" name="submit-conversion" value="submit" class="btn btn-info"><?php echo smartyTranslate(array('s'=>'Save','mod'=>'packlink'),$_smarty_tpl);?>
</button>
          </div></div>
        </form>

        <p class="tab_title"><span><?php echo smartyTranslate(array('s'=>'Auto-populate product data unit ','mod'=>'packlink'),$_smarty_tpl);?>
</span></p>
        <p class="tab_description"><?php echo smartyTranslate(array('s'=>'Automatically complete weight and dimension in PrestaShop catalog from Packlink PRO when such data is missing for a product you ship:','mod'=>'packlink'),$_smarty_tpl);?>
</p>
        <form class="form-horizontal col-sm-7" role="form" action = "<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['module_link']->value,'htmlall','UTF-8');?>
" method="post"> 
          <div class="form-group">              
                <label class="control-label col-sm-4" for="length" style="margin-right: 20px;"></label>
                <div class="col-sm-7 import-radio">
                    <label class="radio"><input type="radio"  value="1" name="import" <?php if ($_smarty_tpl->tpl_vars['packlink_import']->value==1) {?>checked="checked"<?php }?>><?php echo smartyTranslate(array('s'=>'Always','mod'=>'packlink'),$_smarty_tpl);?>
</label>
                    <label class="radio"><input type="radio" value="0" name="import" <?php if ($_smarty_tpl->tpl_vars['packlink_import']->value==0) {?>checked="checked"<?php }?>><?php echo smartyTranslate(array('s'=>'Never','mod'=>'packlink'),$_smarty_tpl);?>
</label>
                </div>
                <input type="hidden" name="PL_tab_name" value="units_form" />

          </div>
          <div class="form-group"> 
                  <div class="col-sm-offset-4 col-sm-7">
          <button type="submit" name="submit-import" value="submit" class="btn btn-info"><?php echo smartyTranslate(array('s'=>'Save','mod'=>'packlink'),$_smarty_tpl);?>
</button>
          </div></div>
        </form>
    </div>

  </div>
</div>

<script>

function equalHeight(group) {
    tallest = 0;
    group.each(function() {
        $(this).css("width", "");       
        thisHeight = $(this).outerWidth();
        if(thisHeight > tallest) {
            tallest = thisHeight;
        }
    });
    group.css('min-width', tallest);
}
$(window).ready(function() {
    equalHeight($(".small_col"));  
});
$("#li-units").click( function() {
    setTimeout(function() {
         equalHeight($(".small_col"));
    }, 220);
});


</script>


<?php }} ?>
