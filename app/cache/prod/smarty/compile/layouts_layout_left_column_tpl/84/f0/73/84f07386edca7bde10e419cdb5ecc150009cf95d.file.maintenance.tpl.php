<?php /* Smarty version Smarty-3.1.19, created on 2017-01-31 13:01:51
         compiled from "/var/www/warenkorb/themes/classic/templates/errors/maintenance.tpl" */ ?>
<?php /*%%SmartyHeaderCode:47608271758907cafd4d8d5-14301643%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '84f07386edca7bde10e419cdb5ecc150009cf95d' => 
    array (
      0 => '/var/www/warenkorb/themes/classic/templates/errors/maintenance.tpl',
      1 => 1485862998,
      2 => 'file',
    ),
    'eb4e16386a852e6373595b5167c9bb5aab10c76c' => 
    array (
      0 => '/var/www/warenkorb/themes/classic/templates/layouts/layout-error.tpl',
      1 => 1485862998,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '47608271758907cafd4d8d5-14301643',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58907cafe31d01_75706525',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58907cafe31d01_75706525')) {function content_58907cafe31d01_75706525($_smarty_tpl) {?><!doctype html>
<html lang="">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    
      <title></title>
      <meta name="description" content="">
      <meta name="keywords" content="">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">

    
  </head>

  <body>

    

  <section id="main">

    
      <header class="page-header">
        <div class="logo"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo'], ENT_QUOTES, 'UTF-8');?>
" alt="logo"></div>
        <?php echo $_smarty_tpl->tpl_vars['HOOK_MAINTENANCE']->value;?>

        
          <h1><?php echo smartyTranslate(array('s'=>'We\'ll be back soon.','d'=>'Shop.Theme'),$_smarty_tpl);?>
</h1>
        
      </header>
    

    
      <section id="content" class="page-content page-maintenance">
        
          <?php echo $_smarty_tpl->tpl_vars['maintenance_text']->value;?>

        
      </section>
    

    

    

  </section>



    <!-- Load JS files here -->

  </body>

</html>
<?php }} ?>
