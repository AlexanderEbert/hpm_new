<?php /*%%SmartyHeaderCode:118917788258907ab7275482-86051169%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '906548e89c8c6025457ddaeffb1980a0c743b872' => 
    array (
      0 => 'module:ps_linklist/views/templates/hook/linkblock.tpl',
      1 => 1485862999,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '118917788258907ab7275482-86051169',
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58907e75961ca2_48909048',
  'has_nocache_code' => false,
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58907e75961ca2_48909048')) {function content_58907e75961ca2_48909048($_smarty_tpl) {?><div class="col-md-4 links">
  <div class="row">
      <div class="col-md-6 wrapper">
      <h3 class="h3 hidden-sm-down">Products</h3>
            <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_24373" data-toggle="collapse">
        <span class="h3">Products</span>
        <span class="pull-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_24373" class="collapse">
                  <li>
            <a
                id="link-product-page-prices-drop-1"
                class="cms-page-link"
                href="https://warenkorb.pixabit.de/angebote"
                title="Unsere Sonderangebote">
              Angebote
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-new-products-1"
                class="cms-page-link"
                href="https://warenkorb.pixabit.de/neue-Produkte"
                title="Unsere neuen Produkte">
              Neue Produkte
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-best-sales-1"
                class="cms-page-link"
                href="https://warenkorb.pixabit.de/verkaufshits"
                title="Unsere Verkaufshits">
              Verkaufshits
            </a>
          </li>
              </ul>
    </div>
      <div class="col-md-6 wrapper">
      <h3 class="h3 hidden-sm-down">Our company</h3>
            <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_78672" data-toggle="collapse">
        <span class="h3">Our company</span>
        <span class="pull-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_78672" class="collapse">
                  <li>
            <a
                id="link-cms-page-1-2"
                class="cms-page-link"
                href="https://warenkorb.pixabit.de/content/1-Lieferung"
                title="Unsere Lieferbedingungen">
              Lieferung
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-2-2"
                class="cms-page-link"
                href="https://warenkorb.pixabit.de/content/2-rechtliche-hinweise"
                title="Rechtliche Hinweise">
              Rechtliche Hinweise
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-3-2"
                class="cms-page-link"
                href="https://warenkorb.pixabit.de/content/3-allgemeine-geschaeftsbedingungen"
                title="Unsere AGB">
              Allgemeine Geschäftsbedingungen
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-4-2"
                class="cms-page-link"
                href="https://warenkorb.pixabit.de/content/4-uber-uns"
                title="Learn more about us">
              Über uns
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-5-2"
                class="cms-page-link"
                href="https://warenkorb.pixabit.de/content/5-sichere-zahlung"
                title="Unsere Sicherheits-Zahlungsmethoden">
              Sichere Zahlung
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-contact-2"
                class="cms-page-link"
                href="https://warenkorb.pixabit.de/kontaktieren-sie-uns"
                title="Nutzen Sie unser Kontaktformular">
              Kontaktieren Sie uns
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-sitemap-2"
                class="cms-page-link"
                href="https://warenkorb.pixabit.de/sitemap"
                title="Verloren? Finden Sie, was Sie suchen">
              Sitemap
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-stores-2"
                class="cms-page-link"
                href="https://warenkorb.pixabit.de/shops"
                title="">
              Shops
            </a>
          </li>
              </ul>
    </div>
    </div>
</div>
<?php }} ?>
