<?php /*%%SmartyHeaderCode:40845296358907ab729b672-41299721%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '42f9461127ce7396a601c2484841253ea5ba658f' => 
    array (
      0 => 'module:ps_customeraccountlinks/ps_customeraccountlinks.tpl',
      1 => 1485862998,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '40845296358907ab729b672-41299721',
  'variables' => 
  array (
    'urls' => 0,
    'my_account_urls' => 0,
    'my_account_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_58907ab72afa83_66471098',
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58907ab72afa83_66471098')) {function content_58907ab72afa83_66471098($_smarty_tpl) {?>
<div id="block_myaccount_infos" class="col-md-2 links wrapper">
  <h3 class="myaccount-title hidden-sm-down">
    <a class="text-uppercase" href="https://warenkorb.pixabit.de/mein-Konto" rel="nofollow">
      Ihr Konto
    </a>
  </h3>
  <div class="title clearfix hidden-md-up" data-target="#footer_account_list" data-toggle="collapse">
    <span class="h3">Ihr Konto</span>
    <span class="pull-xs-right">
      <span class="navbar-toggler collapse-icons">
        <i class="material-icons add">&#xE313;</i>
        <i class="material-icons remove">&#xE316;</i>
      </span>
    </span>
  </div>
  <ul class="account-list collapse" id="footer_account_list">
            <li>
          <a href="https://warenkorb.pixabit.de/adressen" title="Adressen" rel="nofollow">
            Adressen
          </a>
        </li>
            <li>
          <a href="https://warenkorb.pixabit.de/bestellungsverlauf" title="Bestellungen" rel="nofollow">
            Bestellungen
          </a>
        </li>
            <li>
          <a href="https://warenkorb.pixabit.de/kennung" title="Persönliche Angaben" rel="nofollow">
            Persönliche Angaben
          </a>
        </li>
            <li>
          <a href="https://warenkorb.pixabit.de/bestellschein" title="Rückvergütungen" rel="nofollow">
            Rückvergütungen
          </a>
        </li>
        
	</ul>
</div>
<?php }} ?>
