<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_d7fc0ebc78ceff53900494eb4f9268a769967471bc4e3954dd07340315b83178 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_58faec5769bf95b07d0051941bb1fb3950959e7cb1d5cb57de98bb72f5fe8d0e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_58faec5769bf95b07d0051941bb1fb3950959e7cb1d5cb57de98bb72f5fe8d0e->enter($__internal_58faec5769bf95b07d0051941bb1fb3950959e7cb1d5cb57de98bb72f5fe8d0e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_58faec5769bf95b07d0051941bb1fb3950959e7cb1d5cb57de98bb72f5fe8d0e->leave($__internal_58faec5769bf95b07d0051941bb1fb3950959e7cb1d5cb57de98bb72f5fe8d0e_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_b764454fb1c988afaac889cc7ab94ff70976085e33ea87d31181cf0e3f48db0b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b764454fb1c988afaac889cc7ab94ff70976085e33ea87d31181cf0e3f48db0b->enter($__internal_b764454fb1c988afaac889cc7ab94ff70976085e33ea87d31181cf0e3f48db0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_b764454fb1c988afaac889cc7ab94ff70976085e33ea87d31181cf0e3f48db0b->leave($__internal_b764454fb1c988afaac889cc7ab94ff70976085e33ea87d31181cf0e3f48db0b_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_db12681c2019e674d066c180e41d916c528cbd4b9e385e3a67a0a3037cdaafa6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_db12681c2019e674d066c180e41d916c528cbd4b9e385e3a67a0a3037cdaafa6->enter($__internal_db12681c2019e674d066c180e41d916c528cbd4b9e385e3a67a0a3037cdaafa6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_db12681c2019e674d066c180e41d916c528cbd4b9e385e3a67a0a3037cdaafa6->leave($__internal_db12681c2019e674d066c180e41d916c528cbd4b9e385e3a67a0a3037cdaafa6_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_a58dc7c7861761d7fbe8f45b203d1d2e0ebe6a70b8279e76d09928ba3c26e324 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a58dc7c7861761d7fbe8f45b203d1d2e0ebe6a70b8279e76d09928ba3c26e324->enter($__internal_a58dc7c7861761d7fbe8f45b203d1d2e0ebe6a70b8279e76d09928ba3c26e324_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_a58dc7c7861761d7fbe8f45b203d1d2e0ebe6a70b8279e76d09928ba3c26e324->leave($__internal_a58dc7c7861761d7fbe8f45b203d1d2e0ebe6a70b8279e76d09928ba3c26e324_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
";
    }
}
