<?php

/* PrestaShopBundle:Admin/Translations/include:translations-forms.html.twig */
class __TwigTemplate_5d055cbf8dcf045c3b20e5a512366a85b55893ebdc6fa99a99449a3438667145 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bc24c51a99ca27604199675e08356ffe796a4437f9781d89fdeede48185b9939 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc24c51a99ca27604199675e08356ffe796a4437f9781d89fdeede48185b9939->enter($__internal_bc24c51a99ca27604199675e08356ffe796a4437f9781d89fdeede48185b9939_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PrestaShopBundle:Admin/Translations/include:translations-forms.html.twig"));

        // line 25
        echo "<div id=\"jetsContent\">
  ";
        // line 26
        echo $this->env->getExtension('PrestaShopBundle\Twig\TranslationsExtension')->getTranslationsForms((isset($context["translationsTree"]) ? $context["translationsTree"] : $this->getContext($context, "translationsTree")), (isset($context["theme"]) ? $context["theme"] : $this->getContext($context, "theme")));
        echo "
</div>
";
        
        $__internal_bc24c51a99ca27604199675e08356ffe796a4437f9781d89fdeede48185b9939->leave($__internal_bc24c51a99ca27604199675e08356ffe796a4437f9781d89fdeede48185b9939_prof);

    }

    public function getTemplateName()
    {
        return "PrestaShopBundle:Admin/Translations/include:translations-forms.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 26,  22 => 25,);
    }

    public function getSource()
    {
        return "{#**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *#}
<div id=\"jetsContent\">
  {{ getTranslationsForms(translationsTree, theme)|raw }}
</div>
";
    }
}
