<?php

/* __string_template__58dea12349c4c8c854dcb58d0869bb0cf4fdbbb6a81d8d5b4fa4b2f4d0e3cd51 */
class __TwigTemplate_565404fa52ea9dcd6da852bdce4a6fd6fb768cab28b73804a7bf0a9afea7034d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'extra_stylesheets' => array($this, 'block_extra_stylesheets'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
            'content_footer' => array($this, 'block_content_footer'),
            'sidebar_right' => array($this, 'block_sidebar_right'),
            'javascripts' => array($this, 'block_javascripts'),
            'extra_javascripts' => array($this, 'block_extra_javascripts'),
            'translate_javascripts' => array($this, 'block_translate_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f09c935b8c7713768f1a3ba0748c435ed7ba55f404a7ef771c49bcadc840b0cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f09c935b8c7713768f1a3ba0748c435ed7ba55f404a7ef771c49bcadc840b0cd->enter($__internal_f09c935b8c7713768f1a3ba0748c435ed7ba55f404a7ef771c49bcadc840b0cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "__string_template__58dea12349c4c8c854dcb58d0869bb0cf4fdbbb6a81d8d5b4fa4b2f4d0e3cd51"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"de\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>Produkte • HPM Warenkorbsystem</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminProducts';
    var iso_user = 'de';
    var full_language_code = 'de';
    var full_cldr_language_code = 'de-DE';
    var country_iso_code = 'DE';
    var _PS_VERSION_ = '1.7.0.4';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Eine neue Bestellung ist in Ihrem Shop eingegangen.';
    var order_number_msg = 'Bestell-Nr. ';
    var total_msg = 'Gesamt: ';
    var from_msg = 'von ';
    var see_order_msg = 'Diese Bestellung anzeigen';
    var new_customer_msg = 'Neue Kundenregistrierung im Shop.';
    var customer_name_msg = 'Kunde ';
    var new_msg = 'Ihr Shop hat eine neue Nachricht erhalten.';
    var see_msg = 'Nachricht lesen';
    var token = 'ef25b569a81db49cdfe80c131cc0c01e';
    var token_admin_orders = 'b01eb3c17bf5276b47f520209f4fb42e';
    var token_admin_customers = 'b29986c9257774b2eabb9e00799b2c1d';
    var token_admin_customer_threads = '8dc344220e1bffa5476ed51faa28af61';
    var currentIndex = 'index.php?controller=AdminProducts';
    var employee_token = 'c08e7d39a63850332deb01c9bc1f0e5b';
    var choose_language_translate = 'Wählen Sie eine Sprache';
    var default_language = '1';
    var admin_modules_link = '/admin975acnmvl/index.php/module/catalog/recommended?_token=NUX2LCLw4RurwJqw4aCz_2yDuJxq2yGFc4TMsWRBXtQ';
    var tab_modules_list = 'prestagiftvouchers,dmuassocprodcat,etranslation,apiway,prestashoptoquickbooks';
    var update_success_msg = 'Aktualisierung durchgeführt!';
    var errorLogin = 'PrestaShop konnte sich nicht bei Addons anmelden. Überprüfen Sie bitte Ihre Zugangsdaten und Ihre Internetverbindung.';
    var search_product_msg = 'Artikel suchen';
  </script>

      <link href=\"/admin975acnmvl/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/admin975acnmvl/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/admin975acnmvl\\/\";
var baseDir = \"\\/\";
var currency = {\"iso_code\":\"EUR\",\"sign\":\"\\u20ac\",\"name\":\"Euro\",\"format\":\"#,##0.00\\u00a0\\u00a4\"};
var host_mode = false;
</script>
<script type=\"text/javascript\" src=\"/admin975acnmvl/themes/new-theme/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=1.7.0.4\"></script>
<script type=\"text/javascript\" src=\"/js/cldr.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=1.7.0.4\"></script>
<script type=\"text/javascript\" src=\"/admin975acnmvl/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/admin975acnmvl/themes/default/js/vendor/nv.d3.min.js\"></script>


  

";
        // line 69
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>
<body class=\"adminproducts\">



<header>
  <nav class=\"main-header\">

    
    

    
    <a class=\"logo pull-left\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminDashboard&amp;token=210c7db1f0fe26cd152dae092443a683\"></a>

    <div class=\"component pull-left\"><div class=\"ps-dropdown dropdown\">
  <span type=\"button\" id=\"quick-access\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
    <span class=\"selected-item\">Schnellzugriff</span> <i class=\"material-icons arrow-down\">keyboard_arrow_down</i>
  </span>
  <div class=\"ps-dropdown-menu dropdown-menu\" aria-labelledby=\"quick-access\">
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCategories&amp;addcategory&amp;token=b80462e71b9a6c06d647e0774b83dc36\"
                 data-item=\"Neue Kategorie\"
      >Neue Kategorie</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php/productnew?token=8093f4e0d6b5daad8c2455bf9039425e\"
                 data-item=\"Neuer Artikel\"
      >Neuer Artikel</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=cd1ce143dc32417c4451c970a3d2635d\"
                 data-item=\"Neuer Ermäßigungsgutschein\"
      >Neuer Ermäßigungsgutschein</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=b01eb3c17bf5276b47f520209f4fb42e\"
                 data-item=\"Orders\"
      >Orders</a>
        <hr>
        <a
      class=\"dropdown-item js-quick-link\"
      data-rand=\"105\"
      data-icon=\"icon-AdminCatalog\"
      data-method=\"add\"
      data-url=\"index.php/productform14\"
      data-post-link=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminQuickAccesses&token=f7de6343f7a4221b74fab7bc7e154dd1\"
      data-prompt-text=\"Bitte dieses Kürzel angeben:\"
      data-link=\"Produkte - Liste\"
    >
      <i class=\"material-icons\">add_circle_outline</i>
      Zu Favoriten hinzufügen
    </a>
    <a class=\"dropdown-item\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminQuickAccesses&token=f7de6343f7a4221b74fab7bc7e154dd1\">
      <i class=\"material-icons\">settings</i>
      Schnellzugänge verwalten
    </a>
  </div>
</div>
</div>
    <div class=\"component\">

<form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form\"
      method=\"post\"
      action=\"/admin975acnmvl/index.php?controller=AdminSearch&amp;token=0a9475a8a0b937e3eac42baab2cc5806\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input id=\"bo_query\" name=\"bo_query\" type=\"search\" class=\"form-control dropdown-form-search js-form-search\" value=\"\" placeholder=\"Suche (z.B. Bestell-Nr., Kundenname ...)\" />
    <div class=\"input-group-addon\">
      <div class=\"dropdown\">
        <span class=\"dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
          Überall
        </span>
        <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu\">
          <ul class=\"items-list js-items-list\">
            <li class=\"search-all search-option active\">
              <a class=\"dropdown-item\" data-item=\"Überall\" href=\"#\" data-value=\"0\" data-placeholder=\"Wonach suchen Sie?\" data-icon=\"icon-search\">
              <i class=\"material-icons\">search</i> Überall</a>
            </li>
            <hr>
            <li class=\"search-book search-option\">
              <a class=\"dropdown-item\" data-item=\"Katalog\" href=\"#\" data-value=\"1\" data-placeholder=\"Artikelname, Bestandseinheit, Artikel-Nr. ...\" data-icon=\"icon-book\">
                <i class=\"material-icons\">library_books</i> Katalog
              </a>
            </li>
            <li class=\"search-customers-name search-option\">
              <a class=\"dropdown-item\" data-item=\"Kunden nach Name\" href=\"#\" data-value=\"2\" data-placeholder=\"E-Mail , Name...\" data-icon=\"icon-group\">
                <i class=\"material-icons\">group</i> Kunden nach Name
              </a>
            </li>
            <li class=\"search-customers-addresses search-option\">
              <a class=\"dropdown-item\" data-item=\"Kunden nach IP-Adresse\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\">
                <i class=\"material-icons\">desktop_windows</i>Kunden nach IP-Adresse</a>
            </li>
            <li class=\"search-orders search-option\">
              <a class=\"dropdown-item\" data-item=\"Bestellungen\" href=\"#\" data-value=\"3\" data-placeholder=\"Bestell-Nr.\" data-icon=\"icon-credit-card\">
                <i class=\"material-icons\">credit_card</i> Bestellungen
              </a>
            </li>
            <li class=\"search-invoices search-option\">
              <a class=\"dropdown-item\" data-item=\"Rechnungen\" href=\"#\" data-value=\"4\" data-placeholder=\"Rechnungsnummer\" data-icon=\"icon-book\">
                <i class=\"material-icons\">book</i></i> Rechnungen
              </a>
            </li>
            <li class=\"search-carts search-option\">
              <a class=\"dropdown-item\" data-item=\"Warenkörbe\" href=\"#\" data-value=\"5\" data-placeholder=\"Warenkorb-ID\" data-icon=\"icon-shopping-cart\">
                <i class=\"material-icons\">shopping_cart</i> Warenkörbe
              </a>
            </li>
            <li class=\"search-modules search-option\">
              <a class=\"dropdown-item\" data-item=\"Module\" href=\"#\" data-value=\"7\" data-placeholder=\"Modul-Name\" data-icon=\"icon-puzzle-piece\">
                <i class=\"material-icons\">view_module</i> Module
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class=\"input-group-addon search-bar\">
      <button type=\"submit\">SUCHE<i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
  });
</script>
</div>


    <div class=\"component pull-md-right -norightmargin\"><div class=\"employee-dropdown dropdown\">
      <div class=\"img-circle person\" data-toggle=\"dropdown\">
      <i class=\"material-icons\">person</i>
    </div>
    <div class=\"dropdown-menu dropdown-menu-right p-a-1 m-r-2\">
    <div class=\"text-xs-center\">
      <img class=\"avatar img-circle\" src=\"https://profile.prestashop.com/alexander.ebert%40pixabit.de.jpg\" /><br>
      Alexander Ebert
    </div>
    <hr>
    <a class=\"employee-link\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminEmployees&amp;token=c08e7d39a63850332deb01c9bc1f0e5b&amp;id_employee=1&amp;updateemployee\" target=\"_blank\">
      <i class=\"material-icons\">settings_applications</i> Ihr Profil
    </a>
    <a class=\"employee-link m-t-1\" id=\"header_logout\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLogin&amp;token=7adca306320cf62d2ef33b0352922b85&amp;logout\">
      <i class=\"material-icons\">power_settings_new</i> Abmelden
    </a>
  </div>
</div>
</div>
          <div class=\"component pull-md-right\"><div class=\"notification-center dropdown\">
  <div class=\"notification dropdown-toggle\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count\">0</span>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Bestellungen<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Kunden<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Nachrichten<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell kein neuer Kunde!<br>
              Haben Sie die <strong><a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarts&token=5106885aa0591a74fe56268cb188b26c&action=filterOnlyAbandonedCarts\">verwaisten Warenkörbe</a></strong> überprüft?<br>
              Ihre nächste Bestellung könnte sich dort verstecken!
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell kein neuer Kunde!<br>
              Haben Sie in der letzten Zeit Werbe-Mails versandt?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell keine neuen Nachrichten <br>
              Ihre Kunden sind offenbar alle zufrieden.
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      von <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"pull-xs-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - Anmelden <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
</div>
        <div class=\"component pull-md-right\">  <div class=\"shop-list\">
    <a class=\"link\" href=\"http://warenkorb.pixabit.de/\" target= \"_blank\">HPM Warenkorbsystem</a>
  </div>
</div>
              <div class=\"component pull-right\">
        <div class=\"shop-state\" id=\"debug-mode\">
          <i class=\"material-icons\">bug_report</i>
          <span class=\"label-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" data-html=\"true\"
            title=\"<p class='text-left text-nowrap'><strong>Ihr Shop befindet sich im Debug-Modus.</strong></p><p class='text-left'>Alle PHP-Fehler und -Nachrichten werden angezeigt. Wenn Sie dies nicht mehr benötigen, wählen Sie die Option <strong>Abschalten</strong>.</p>\">Debug-Modus</span>
        </div>
      </div>
        

    

    
    
  </nav>
</header>

<nav class=\"nav-bar\">
  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\">
            <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminDashboard&amp;token=210c7db1f0fe26cd152dae092443a683\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Dashboard</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title -active\" data-submenu=\"2\">
              <span class=\"title\">Sell</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"3\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=b01eb3c17bf5276b47f520209f4fb42e\" class=\"link\">
                    <i class=\"material-icons\">shopping_basket</i> <span>Bestellungen</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=b01eb3c17bf5276b47f520209f4fb42e\" class=\"link\"> Bestellungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminInvoices&amp;token=f878d78075254ca0fc91ba060e234781\" class=\"link\"> Rechnungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminSlip&amp;token=9abcacc862261147d8dec759c81fc4d8\" class=\"link\"> Gutschriften
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminDeliverySlip&amp;token=20123a583304637cb47ed287ab60a381\" class=\"link\"> Lieferscheine
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarts&amp;token=5106885aa0591a74fe56268cb188b26c\" class=\"link\"> Warenkörbe
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone -active\" data-submenu=\"9\">
                  <a href=\"/admin975acnmvl/index.php/product/catalog?_token=NUX2LCLw4RurwJqw4aCz_2yDuJxq2yGFc4TMsWRBXtQ\" class=\"link\">
                    <i class=\"material-icons\">store</i> <span>Katalog</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"10\">
                              <a href=\"/admin975acnmvl/index.php/product/catalog?_token=NUX2LCLw4RurwJqw4aCz_2yDuJxq2yGFc4TMsWRBXtQ\" class=\"link\"> Produkte
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCategories&amp;token=b80462e71b9a6c06d647e0774b83dc36\" class=\"link\"> Kategorien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminTracking&amp;token=6f0eb25a650d1f5e583e65954a8e8c56\" class=\"link\"> Kontrollübersicht
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminTabPacklink&amp;token=f4e91d8ea16fb5a9ff776e52e383f18e\" class=\"link\"> Attributes &amp; Features
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminManufacturers&amp;token=99665acfea1d5afb40145996117b1e4b\" class=\"link\"> Brands &amp; Suppliers
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAttachments&amp;token=9adf85a6bdf100ca2d33c1db0693eea4\" class=\"link\"> Anhänge
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCartRules&amp;token=cd1ce143dc32417c4451c970a3d2635d\" class=\"link\"> Discounts
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"23\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomers&amp;token=b29986c9257774b2eabb9e00799b2c1d\" class=\"link\">
                    <i class=\"material-icons\">account_circle</i> <span>Kunden</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"24\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomers&amp;token=b29986c9257774b2eabb9e00799b2c1d\" class=\"link\"> Kunden
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAddresses&amp;token=0b6161f50443de30ceb04c606fd3fe38\" class=\"link\"> Adressen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"26\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOutstanding&amp;token=f038726d65fe9402c258647b1898fb0d\" class=\"link\"> Offene Forderungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"27\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomerThreads&amp;token=8dc344220e1bffa5476ed51faa28af61\" class=\"link\">
                    <i class=\"material-icons\">chat</i> <span>Kundenservice</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"28\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomerThreads&amp;token=8dc344220e1bffa5476ed51faa28af61\" class=\"link\"> Customer Service
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrderMessage&amp;token=9fb6919b7b40e4754482cde6fbc226f9\" class=\"link\"> Bestellnachrichten
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminReturn&amp;token=4d22afc04aa9ea7a2a68488c06fad5a5\" class=\"link\"> Warenrücksendungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"31\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminStats&amp;token=96c9f3fa17d6f8e6c9f5201b0c534582\" class=\"link\">
                    <i class=\"material-icons\">assessment</i> <span>Statistiken</span>
                  </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"41\">
              <span class=\"title\">Improve</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"42\">
                  <a href=\"/admin975acnmvl/index.php/module/catalog?_token=NUX2LCLw4RurwJqw4aCz_2yDuJxq2yGFc4TMsWRBXtQ\" class=\"link\">
                    <i class=\"material-icons\">extension</i> <span>Modules</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"43\">
                              <a href=\"/admin975acnmvl/index.php/module/catalog?_token=NUX2LCLw4RurwJqw4aCz_2yDuJxq2yGFc4TMsWRBXtQ\" class=\"link\"> Modules &amp; Services
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"45\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAddonsCatalog&amp;token=4237449fcb27c89f54b3e13aec2cd128\" class=\"link\"> Modules Catalog
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"46\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminThemes&amp;token=6751b6504f415a361515f6fe8a2abe64\" class=\"link\">
                    <i class=\"material-icons\">desktop_mac</i> <span>Design</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"47\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminThemes&amp;token=6751b6504f415a361515f6fe8a2abe64\" class=\"link\"> Templates &amp; Vorlagen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"48\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminThemesCatalog&amp;token=2a0eaa5a6d221a4505ea9f7b7c8e6f8a\" class=\"link\"> Theme Catalog
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"49\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCmsContent&amp;token=13638b2941d379f40273acd81e0372c0\" class=\"link\"> Pages
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"50\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminModulesPositions&amp;token=c2996aa94efb77a65c4f792df019f0fe\" class=\"link\"> Positionen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"51\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminImages&amp;token=0144749fef768d427d5d2692d7661737\" class=\"link\"> Image Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"116\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLinkWidget&amp;token=1d0178b2a7846a641f155e43bc9dd75b\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"52\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarriers&amp;token=abd2445b010b36d19f7f320cdebbc8be\" class=\"link\">
                    <i class=\"material-icons\">local_shipping</i> <span>Versand</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"53\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarriers&amp;token=abd2445b010b36d19f7f320cdebbc8be\" class=\"link\"> Versanddienste
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"54\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminShipping&amp;token=4644d3d8754f40b38c14d74b2ae22b0f\" class=\"link\"> Voreinstellungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"55\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPayment&amp;token=2273df9a32a2ffb2a45a3317e5d67f70\" class=\"link\">
                    <i class=\"material-icons\">payment</i> <span>Zahlung</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"56\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPayment&amp;token=2273df9a32a2ffb2a45a3317e5d67f70\" class=\"link\"> Payment Methods
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPaymentPreferences&amp;token=3a987bd863941fa3ae5d9dbb2acd84e5\" class=\"link\"> Preferences
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"58\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLocalization&amp;token=876531de0a977e75a6ae93ccad36dc5b\" class=\"link\">
                    <i class=\"material-icons\">language</i> <span>International</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"59\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLocalization&amp;token=876531de0a977e75a6ae93ccad36dc5b\" class=\"link\"> Lokalisierung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"64\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCountries&amp;token=d439564e0fd75460c1600bbbbf8035b8\" class=\"link\"> Locations
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"68\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminTaxes&amp;token=0c82df51bf9159d689ec816526626279\" class=\"link\"> Taxes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"71\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminTranslations&amp;token=a4ac548dc866c9f1201ab2cbec134c2b\" class=\"link\"> Übersetzungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"72\">
              <span class=\"title\">Configure</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"73\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPreferences&amp;token=84b8473e6147cb7b4038346e16df366f\" class=\"link\">
                    <i class=\"material-icons\">settings</i> <span>Shop Parameters</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"74\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPreferences&amp;token=84b8473e6147cb7b4038346e16df366f\" class=\"link\"> Allgemein
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"77\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrderPreferences&amp;token=aaab9238d1fc257104745333e4b3bfdc\" class=\"link\"> Order Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"80\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPPreferences&amp;token=54b154dfb4bd0f9a80243f276e27706f\" class=\"link\"> Produkte
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"81\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomerPreferences&amp;token=bba1002d18d4302ec91cad33fe101b1d\" class=\"link\"> Customer Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"85\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminContacts&amp;token=b624796bee2804b57531e5526da7846f\" class=\"link\"> Contact
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"88\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminMeta&amp;token=8c48f4f17145abf3e29ced7eb6279f1b\" class=\"link\"> Traffic
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"92\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminSearchConf&amp;token=2328779981529f1ada0cc5a1db74b57c\" class=\"link\"> Suche
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"95\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminInformation&amp;token=d086dc7cda5e105b8728905129c861ac\" class=\"link\">
                    <i class=\"material-icons\">settings_applications</i> <span>Erweiterte Einstellungen</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"96\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminInformation&amp;token=d086dc7cda5e105b8728905129c861ac\" class=\"link\"> Information
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"97\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPerformance&amp;token=9f21d46c95366dee2c7b1110e17870e7\" class=\"link\"> Leistung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"98\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAdminPreferences&amp;token=913836dfd029e70d929052b060e6b8a6\" class=\"link\"> Verwaltung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"99\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminEmails&amp;token=0a8c2436b5be61035d4bc4a5160ccbb3\" class=\"link\"> E-Mail
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"100\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminImport&amp;token=cc9c20abe1b7e255019999fd9c0812cf\" class=\"link\"> Import
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"101\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminEmployees&amp;token=c08e7d39a63850332deb01c9bc1f0e5b\" class=\"link\"> Mitarbeiter
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"105\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminRequestSql&amp;token=a03ff532cebd1e73a641aad997e20011\" class=\"link\"> Database
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"108\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLogs&amp;token=1d5fad2ddc6fb468bc9864d3a2263b49\" class=\"link\"> Log-Dateien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminWebservice&amp;token=4a799f855feaf376ba589242e4b6bd63\" class=\"link\"> Webdienste
                              </a>
                            </li>

                                                                                                                                                                            </ul>
                                    </li>
                          
        
            </ul>

  <span class=\"menu-collapse\">
    <i class=\"material-icons\">&#xE8EE;</i>
  </span>

  

</nav>


<div id=\"main-div\">

  
        
    <div class=\"content-div -notoolbar\">

      

      

      

      
      
      
      

      <div class=\"row \">
        <div class=\"col-xs-12\">
          <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>




  ";
        // line 932
        $this->displayBlock('content_header', $context, $blocks);
        // line 933
        echo "                 ";
        $this->displayBlock('content', $context, $blocks);
        // line 934
        echo "                 ";
        $this->displayBlock('content_footer', $context, $blocks);
        // line 935
        echo "                 ";
        $this->displayBlock('sidebar_right', $context, $blocks);
        // line 936
        echo "
        </div>
      </div>

    </div>

  
</div>


  <div id=\"footer\" class=\"bootstrap hide\">
<!--
  <div class=\"col-sm-2 hidden-xs\">
    <a href=\"http://www.prestashop.com/\" class=\"_blank\">PrestaShop&trade;</a>
    -
    <span id=\"footer-load-time\"><i class=\"icon-time\" title=\"Ladezeit \"></i> 1.779s</span>
  </div>

  <div class=\"col-sm-2 hidden-xs\">
    <div class=\"social-networks\">
      <a class=\"link-social link-twitter _blank\" href=\"https://twitter.com/PrestaShop\" title=\"Twitter\">
        <i class=\"icon-twitter\"></i>
      </a>
      <a class=\"link-social link-facebook _blank\" href=\"https://www.facebook.com/prestashop\" title=\"Facebook\">
        <i class=\"icon-facebook\"></i>
      </a>
      <a class=\"link-social link-github _blank\" href=\"https://www.prestashop.com/github\" title=\"Github\">
        <i class=\"icon-github\"></i>
      </a>
      <a class=\"link-social link-google _blank\" href=\"https://plus.google.com/+prestashop/\" title=\"Google\">
        <i class=\"icon-google-plus\"></i>
      </a>
    </div>
  </div>
  <div class=\"col-sm-5\">
    <div class=\"footer-contact\">
      <a href=\"http://www.prestashop.com/en/contact_us?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-envelope\"></i>
        Kontakt
      </a>
      /&nbsp;
      <a href=\"http://forge.prestashop.com/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-bug\"></i>
        Bug-Tracker
      </a>
      /&nbsp;
      <a href=\"http://www.prestashop.com/forums/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-comments\"></i>
        Forum
      </a>
      /&nbsp;
      <a href=\"http://addons.prestashop.com/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-puzzle-piece\"></i>
        Addons
      </a>
      /&nbsp;
      <a href=\"http://www.prestashop.com/en/training-prestashop?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-book\"></i>
        Training
      </a>
                </div>
  </div>

  <div class=\"col-sm-3\">
    
  </div>

  <div id=\"go-top\" class=\"hide\"><i class=\"icon-arrow-up\"></i></div>
  -->
</div>



  <div class=\"bootstrap\">
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"http://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-DE&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t
<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/de/login?email=alexander.ebert%40pixabit.de&amp;firstname=Alexander&amp;lastname=Ebert&amp;website=http%3A%2F%2Fwarenkorb.pixabit.de%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/admin975acnmvl/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Verbinden Sie Ihren Shop mit Prestashops Marktplatz, um automatisch alle Kaufmodule zu importieren.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Sie haben noch keinen Account?</h4>
\t\t\t\t\t\t<p class='text-justify'>Entdecken Sie die Vielfalt der PrestaShop Addons! Stöbern Sie im offiziellen PrestaShop Martkplatz mit aktuell über 3 500 innovativen Templates und modularen Erweiterungen - ob es sich nun um Optimierung der Wechselkurse, Erhöhung der Zugriffsrate, Maßnahmen zur Kundenbindung oder Rentabilitätssteigerung handelt.</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Wechseln Sie zu PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link pull-right _blank\" href=\"//addons.prestashop.com/de/forgot-your-password\">Ich habe mein Passwort vergessen</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/de/login?email=alexander.ebert%40pixabit.de&amp;firstname=Alexander&amp;lastname=Ebert&amp;website=http%3A%2F%2Fwarenkorb.pixabit.de%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tErstellen Sie ein Konto
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Anmelden
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>
  </div>

";
        // line 1081
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>
</html>";
        
        $__internal_f09c935b8c7713768f1a3ba0748c435ed7ba55f404a7ef771c49bcadc840b0cd->leave($__internal_f09c935b8c7713768f1a3ba0748c435ed7ba55f404a7ef771c49bcadc840b0cd_prof);

    }

    // line 69
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_86ab5f01a42ceff04ff61107e4309d1a705012d16654baa06e9da54014606b7d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_86ab5f01a42ceff04ff61107e4309d1a705012d16654baa06e9da54014606b7d->enter($__internal_86ab5f01a42ceff04ff61107e4309d1a705012d16654baa06e9da54014606b7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_86ab5f01a42ceff04ff61107e4309d1a705012d16654baa06e9da54014606b7d->leave($__internal_86ab5f01a42ceff04ff61107e4309d1a705012d16654baa06e9da54014606b7d_prof);

    }

    public function block_extra_stylesheets($context, array $blocks = array())
    {
        $__internal_35555f76b5804efebd21a71063d5046a4d14cb2dc44c35d86ff783a65c72aea6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_35555f76b5804efebd21a71063d5046a4d14cb2dc44c35d86ff783a65c72aea6->enter($__internal_35555f76b5804efebd21a71063d5046a4d14cb2dc44c35d86ff783a65c72aea6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "extra_stylesheets"));

        
        $__internal_35555f76b5804efebd21a71063d5046a4d14cb2dc44c35d86ff783a65c72aea6->leave($__internal_35555f76b5804efebd21a71063d5046a4d14cb2dc44c35d86ff783a65c72aea6_prof);

    }

    // line 932
    public function block_content_header($context, array $blocks = array())
    {
        $__internal_564e9c299ab7a8d7014c7a9a6111a61b2a7c058ba09b6b4d6c10d3ea29b977ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_564e9c299ab7a8d7014c7a9a6111a61b2a7c058ba09b6b4d6c10d3ea29b977ac->enter($__internal_564e9c299ab7a8d7014c7a9a6111a61b2a7c058ba09b6b4d6c10d3ea29b977ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        
        $__internal_564e9c299ab7a8d7014c7a9a6111a61b2a7c058ba09b6b4d6c10d3ea29b977ac->leave($__internal_564e9c299ab7a8d7014c7a9a6111a61b2a7c058ba09b6b4d6c10d3ea29b977ac_prof);

    }

    // line 933
    public function block_content($context, array $blocks = array())
    {
        $__internal_864b7279e825593ea570dae505c3d02539713570d40bd731a614e92ce26bc2cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_864b7279e825593ea570dae505c3d02539713570d40bd731a614e92ce26bc2cf->enter($__internal_864b7279e825593ea570dae505c3d02539713570d40bd731a614e92ce26bc2cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_864b7279e825593ea570dae505c3d02539713570d40bd731a614e92ce26bc2cf->leave($__internal_864b7279e825593ea570dae505c3d02539713570d40bd731a614e92ce26bc2cf_prof);

    }

    // line 934
    public function block_content_footer($context, array $blocks = array())
    {
        $__internal_839b8c32ff8d6f568cac0924152f773e250e4e511f4499ec1f73d04955264be2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_839b8c32ff8d6f568cac0924152f773e250e4e511f4499ec1f73d04955264be2->enter($__internal_839b8c32ff8d6f568cac0924152f773e250e4e511f4499ec1f73d04955264be2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_footer"));

        
        $__internal_839b8c32ff8d6f568cac0924152f773e250e4e511f4499ec1f73d04955264be2->leave($__internal_839b8c32ff8d6f568cac0924152f773e250e4e511f4499ec1f73d04955264be2_prof);

    }

    // line 935
    public function block_sidebar_right($context, array $blocks = array())
    {
        $__internal_3ba74666e3112684430ba115179e81594631770bcceaf70928e7848be1d12b06 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ba74666e3112684430ba115179e81594631770bcceaf70928e7848be1d12b06->enter($__internal_3ba74666e3112684430ba115179e81594631770bcceaf70928e7848be1d12b06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar_right"));

        
        $__internal_3ba74666e3112684430ba115179e81594631770bcceaf70928e7848be1d12b06->leave($__internal_3ba74666e3112684430ba115179e81594631770bcceaf70928e7848be1d12b06_prof);

    }

    // line 1081
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_9c3edc7fe0387ae2a2ba1069d649b4f08b854ff63a63597631172bc30faf62e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c3edc7fe0387ae2a2ba1069d649b4f08b854ff63a63597631172bc30faf62e0->enter($__internal_9c3edc7fe0387ae2a2ba1069d649b4f08b854ff63a63597631172bc30faf62e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_9c3edc7fe0387ae2a2ba1069d649b4f08b854ff63a63597631172bc30faf62e0->leave($__internal_9c3edc7fe0387ae2a2ba1069d649b4f08b854ff63a63597631172bc30faf62e0_prof);

    }

    public function block_extra_javascripts($context, array $blocks = array())
    {
        $__internal_e8ae3f8414c216252c5f40eaa37a0c54915609179250686c556c644732eebf7d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e8ae3f8414c216252c5f40eaa37a0c54915609179250686c556c644732eebf7d->enter($__internal_e8ae3f8414c216252c5f40eaa37a0c54915609179250686c556c644732eebf7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "extra_javascripts"));

        
        $__internal_e8ae3f8414c216252c5f40eaa37a0c54915609179250686c556c644732eebf7d->leave($__internal_e8ae3f8414c216252c5f40eaa37a0c54915609179250686c556c644732eebf7d_prof);

    }

    public function block_translate_javascripts($context, array $blocks = array())
    {
        $__internal_28889bba363c904abf6d00a532746051f082203b275d5ce6cb390cc92a37d564 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28889bba363c904abf6d00a532746051f082203b275d5ce6cb390cc92a37d564->enter($__internal_28889bba363c904abf6d00a532746051f082203b275d5ce6cb390cc92a37d564_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "translate_javascripts"));

        
        $__internal_28889bba363c904abf6d00a532746051f082203b275d5ce6cb390cc92a37d564->leave($__internal_28889bba363c904abf6d00a532746051f082203b275d5ce6cb390cc92a37d564_prof);

    }

    public function getTemplateName()
    {
        return "__string_template__58dea12349c4c8c854dcb58d0869bb0cf4fdbbb6a81d8d5b4fa4b2f4d0e3cd51";
    }

    public function getDebugInfo()
    {
        return array (  1202 => 1081,  1191 => 935,  1180 => 934,  1169 => 933,  1158 => 932,  1137 => 69,  1126 => 1081,  979 => 936,  976 => 935,  973 => 934,  970 => 933,  968 => 932,  101 => 69,  31 => 1,);
    }

    public function getSource()
    {
        return "<!DOCTYPE html>
<html lang=\"de\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>Produkte • HPM Warenkorbsystem</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminProducts';
    var iso_user = 'de';
    var full_language_code = 'de';
    var full_cldr_language_code = 'de-DE';
    var country_iso_code = 'DE';
    var _PS_VERSION_ = '1.7.0.4';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Eine neue Bestellung ist in Ihrem Shop eingegangen.';
    var order_number_msg = 'Bestell-Nr. ';
    var total_msg = 'Gesamt: ';
    var from_msg = 'von ';
    var see_order_msg = 'Diese Bestellung anzeigen';
    var new_customer_msg = 'Neue Kundenregistrierung im Shop.';
    var customer_name_msg = 'Kunde ';
    var new_msg = 'Ihr Shop hat eine neue Nachricht erhalten.';
    var see_msg = 'Nachricht lesen';
    var token = 'ef25b569a81db49cdfe80c131cc0c01e';
    var token_admin_orders = 'b01eb3c17bf5276b47f520209f4fb42e';
    var token_admin_customers = 'b29986c9257774b2eabb9e00799b2c1d';
    var token_admin_customer_threads = '8dc344220e1bffa5476ed51faa28af61';
    var currentIndex = 'index.php?controller=AdminProducts';
    var employee_token = 'c08e7d39a63850332deb01c9bc1f0e5b';
    var choose_language_translate = 'Wählen Sie eine Sprache';
    var default_language = '1';
    var admin_modules_link = '/admin975acnmvl/index.php/module/catalog/recommended?_token=NUX2LCLw4RurwJqw4aCz_2yDuJxq2yGFc4TMsWRBXtQ';
    var tab_modules_list = 'prestagiftvouchers,dmuassocprodcat,etranslation,apiway,prestashoptoquickbooks';
    var update_success_msg = 'Aktualisierung durchgeführt!';
    var errorLogin = 'PrestaShop konnte sich nicht bei Addons anmelden. Überprüfen Sie bitte Ihre Zugangsdaten und Ihre Internetverbindung.';
    var search_product_msg = 'Artikel suchen';
  </script>

      <link href=\"/admin975acnmvl/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/admin975acnmvl/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/admin975acnmvl\\/\";
var baseDir = \"\\/\";
var currency = {\"iso_code\":\"EUR\",\"sign\":\"\\u20ac\",\"name\":\"Euro\",\"format\":\"#,##0.00\\u00a0\\u00a4\"};
var host_mode = false;
</script>
<script type=\"text/javascript\" src=\"/admin975acnmvl/themes/new-theme/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=1.7.0.4\"></script>
<script type=\"text/javascript\" src=\"/js/cldr.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=1.7.0.4\"></script>
<script type=\"text/javascript\" src=\"/admin975acnmvl/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/admin975acnmvl/themes/default/js/vendor/nv.d3.min.js\"></script>


  

{% block stylesheets %}{% endblock %}{% block extra_stylesheets %}{% endblock %}</head>
<body class=\"adminproducts\">



<header>
  <nav class=\"main-header\">

    
    

    
    <a class=\"logo pull-left\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminDashboard&amp;token=210c7db1f0fe26cd152dae092443a683\"></a>

    <div class=\"component pull-left\"><div class=\"ps-dropdown dropdown\">
  <span type=\"button\" id=\"quick-access\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
    <span class=\"selected-item\">Schnellzugriff</span> <i class=\"material-icons arrow-down\">keyboard_arrow_down</i>
  </span>
  <div class=\"ps-dropdown-menu dropdown-menu\" aria-labelledby=\"quick-access\">
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCategories&amp;addcategory&amp;token=b80462e71b9a6c06d647e0774b83dc36\"
                 data-item=\"Neue Kategorie\"
      >Neue Kategorie</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php/productnew?token=8093f4e0d6b5daad8c2455bf9039425e\"
                 data-item=\"Neuer Artikel\"
      >Neuer Artikel</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=cd1ce143dc32417c4451c970a3d2635d\"
                 data-item=\"Neuer Ermäßigungsgutschein\"
      >Neuer Ermäßigungsgutschein</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=b01eb3c17bf5276b47f520209f4fb42e\"
                 data-item=\"Orders\"
      >Orders</a>
        <hr>
        <a
      class=\"dropdown-item js-quick-link\"
      data-rand=\"105\"
      data-icon=\"icon-AdminCatalog\"
      data-method=\"add\"
      data-url=\"index.php/productform14\"
      data-post-link=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminQuickAccesses&token=f7de6343f7a4221b74fab7bc7e154dd1\"
      data-prompt-text=\"Bitte dieses Kürzel angeben:\"
      data-link=\"Produkte - Liste\"
    >
      <i class=\"material-icons\">add_circle_outline</i>
      Zu Favoriten hinzufügen
    </a>
    <a class=\"dropdown-item\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminQuickAccesses&token=f7de6343f7a4221b74fab7bc7e154dd1\">
      <i class=\"material-icons\">settings</i>
      Schnellzugänge verwalten
    </a>
  </div>
</div>
</div>
    <div class=\"component\">

<form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form\"
      method=\"post\"
      action=\"/admin975acnmvl/index.php?controller=AdminSearch&amp;token=0a9475a8a0b937e3eac42baab2cc5806\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input id=\"bo_query\" name=\"bo_query\" type=\"search\" class=\"form-control dropdown-form-search js-form-search\" value=\"\" placeholder=\"Suche (z.B. Bestell-Nr., Kundenname ...)\" />
    <div class=\"input-group-addon\">
      <div class=\"dropdown\">
        <span class=\"dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
          Überall
        </span>
        <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu\">
          <ul class=\"items-list js-items-list\">
            <li class=\"search-all search-option active\">
              <a class=\"dropdown-item\" data-item=\"Überall\" href=\"#\" data-value=\"0\" data-placeholder=\"Wonach suchen Sie?\" data-icon=\"icon-search\">
              <i class=\"material-icons\">search</i> Überall</a>
            </li>
            <hr>
            <li class=\"search-book search-option\">
              <a class=\"dropdown-item\" data-item=\"Katalog\" href=\"#\" data-value=\"1\" data-placeholder=\"Artikelname, Bestandseinheit, Artikel-Nr. ...\" data-icon=\"icon-book\">
                <i class=\"material-icons\">library_books</i> Katalog
              </a>
            </li>
            <li class=\"search-customers-name search-option\">
              <a class=\"dropdown-item\" data-item=\"Kunden nach Name\" href=\"#\" data-value=\"2\" data-placeholder=\"E-Mail , Name...\" data-icon=\"icon-group\">
                <i class=\"material-icons\">group</i> Kunden nach Name
              </a>
            </li>
            <li class=\"search-customers-addresses search-option\">
              <a class=\"dropdown-item\" data-item=\"Kunden nach IP-Adresse\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\">
                <i class=\"material-icons\">desktop_windows</i>Kunden nach IP-Adresse</a>
            </li>
            <li class=\"search-orders search-option\">
              <a class=\"dropdown-item\" data-item=\"Bestellungen\" href=\"#\" data-value=\"3\" data-placeholder=\"Bestell-Nr.\" data-icon=\"icon-credit-card\">
                <i class=\"material-icons\">credit_card</i> Bestellungen
              </a>
            </li>
            <li class=\"search-invoices search-option\">
              <a class=\"dropdown-item\" data-item=\"Rechnungen\" href=\"#\" data-value=\"4\" data-placeholder=\"Rechnungsnummer\" data-icon=\"icon-book\">
                <i class=\"material-icons\">book</i></i> Rechnungen
              </a>
            </li>
            <li class=\"search-carts search-option\">
              <a class=\"dropdown-item\" data-item=\"Warenkörbe\" href=\"#\" data-value=\"5\" data-placeholder=\"Warenkorb-ID\" data-icon=\"icon-shopping-cart\">
                <i class=\"material-icons\">shopping_cart</i> Warenkörbe
              </a>
            </li>
            <li class=\"search-modules search-option\">
              <a class=\"dropdown-item\" data-item=\"Module\" href=\"#\" data-value=\"7\" data-placeholder=\"Modul-Name\" data-icon=\"icon-puzzle-piece\">
                <i class=\"material-icons\">view_module</i> Module
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class=\"input-group-addon search-bar\">
      <button type=\"submit\">SUCHE<i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
  });
</script>
</div>


    <div class=\"component pull-md-right -norightmargin\"><div class=\"employee-dropdown dropdown\">
      <div class=\"img-circle person\" data-toggle=\"dropdown\">
      <i class=\"material-icons\">person</i>
    </div>
    <div class=\"dropdown-menu dropdown-menu-right p-a-1 m-r-2\">
    <div class=\"text-xs-center\">
      <img class=\"avatar img-circle\" src=\"https://profile.prestashop.com/alexander.ebert%40pixabit.de.jpg\" /><br>
      Alexander Ebert
    </div>
    <hr>
    <a class=\"employee-link\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminEmployees&amp;token=c08e7d39a63850332deb01c9bc1f0e5b&amp;id_employee=1&amp;updateemployee\" target=\"_blank\">
      <i class=\"material-icons\">settings_applications</i> Ihr Profil
    </a>
    <a class=\"employee-link m-t-1\" id=\"header_logout\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLogin&amp;token=7adca306320cf62d2ef33b0352922b85&amp;logout\">
      <i class=\"material-icons\">power_settings_new</i> Abmelden
    </a>
  </div>
</div>
</div>
          <div class=\"component pull-md-right\"><div class=\"notification-center dropdown\">
  <div class=\"notification dropdown-toggle\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count\">0</span>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Bestellungen<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Kunden<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Nachrichten<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell kein neuer Kunde!<br>
              Haben Sie die <strong><a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarts&token=5106885aa0591a74fe56268cb188b26c&action=filterOnlyAbandonedCarts\">verwaisten Warenkörbe</a></strong> überprüft?<br>
              Ihre nächste Bestellung könnte sich dort verstecken!
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell kein neuer Kunde!<br>
              Haben Sie in der letzten Zeit Werbe-Mails versandt?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell keine neuen Nachrichten <br>
              Ihre Kunden sind offenbar alle zufrieden.
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      von <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"pull-xs-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - Anmelden <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
</div>
        <div class=\"component pull-md-right\">  <div class=\"shop-list\">
    <a class=\"link\" href=\"http://warenkorb.pixabit.de/\" target= \"_blank\">HPM Warenkorbsystem</a>
  </div>
</div>
              <div class=\"component pull-right\">
        <div class=\"shop-state\" id=\"debug-mode\">
          <i class=\"material-icons\">bug_report</i>
          <span class=\"label-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" data-html=\"true\"
            title=\"<p class='text-left text-nowrap'><strong>Ihr Shop befindet sich im Debug-Modus.</strong></p><p class='text-left'>Alle PHP-Fehler und -Nachrichten werden angezeigt. Wenn Sie dies nicht mehr benötigen, wählen Sie die Option <strong>Abschalten</strong>.</p>\">Debug-Modus</span>
        </div>
      </div>
        

    

    
    
  </nav>
</header>

<nav class=\"nav-bar\">
  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\">
            <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminDashboard&amp;token=210c7db1f0fe26cd152dae092443a683\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Dashboard</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title -active\" data-submenu=\"2\">
              <span class=\"title\">Sell</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"3\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=b01eb3c17bf5276b47f520209f4fb42e\" class=\"link\">
                    <i class=\"material-icons\">shopping_basket</i> <span>Bestellungen</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=b01eb3c17bf5276b47f520209f4fb42e\" class=\"link\"> Bestellungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminInvoices&amp;token=f878d78075254ca0fc91ba060e234781\" class=\"link\"> Rechnungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminSlip&amp;token=9abcacc862261147d8dec759c81fc4d8\" class=\"link\"> Gutschriften
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminDeliverySlip&amp;token=20123a583304637cb47ed287ab60a381\" class=\"link\"> Lieferscheine
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarts&amp;token=5106885aa0591a74fe56268cb188b26c\" class=\"link\"> Warenkörbe
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone -active\" data-submenu=\"9\">
                  <a href=\"/admin975acnmvl/index.php/product/catalog?_token=NUX2LCLw4RurwJqw4aCz_2yDuJxq2yGFc4TMsWRBXtQ\" class=\"link\">
                    <i class=\"material-icons\">store</i> <span>Katalog</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"10\">
                              <a href=\"/admin975acnmvl/index.php/product/catalog?_token=NUX2LCLw4RurwJqw4aCz_2yDuJxq2yGFc4TMsWRBXtQ\" class=\"link\"> Produkte
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCategories&amp;token=b80462e71b9a6c06d647e0774b83dc36\" class=\"link\"> Kategorien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminTracking&amp;token=6f0eb25a650d1f5e583e65954a8e8c56\" class=\"link\"> Kontrollübersicht
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminTabPacklink&amp;token=f4e91d8ea16fb5a9ff776e52e383f18e\" class=\"link\"> Attributes &amp; Features
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminManufacturers&amp;token=99665acfea1d5afb40145996117b1e4b\" class=\"link\"> Brands &amp; Suppliers
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAttachments&amp;token=9adf85a6bdf100ca2d33c1db0693eea4\" class=\"link\"> Anhänge
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCartRules&amp;token=cd1ce143dc32417c4451c970a3d2635d\" class=\"link\"> Discounts
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"23\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomers&amp;token=b29986c9257774b2eabb9e00799b2c1d\" class=\"link\">
                    <i class=\"material-icons\">account_circle</i> <span>Kunden</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"24\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomers&amp;token=b29986c9257774b2eabb9e00799b2c1d\" class=\"link\"> Kunden
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAddresses&amp;token=0b6161f50443de30ceb04c606fd3fe38\" class=\"link\"> Adressen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"26\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOutstanding&amp;token=f038726d65fe9402c258647b1898fb0d\" class=\"link\"> Offene Forderungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"27\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomerThreads&amp;token=8dc344220e1bffa5476ed51faa28af61\" class=\"link\">
                    <i class=\"material-icons\">chat</i> <span>Kundenservice</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"28\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomerThreads&amp;token=8dc344220e1bffa5476ed51faa28af61\" class=\"link\"> Customer Service
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrderMessage&amp;token=9fb6919b7b40e4754482cde6fbc226f9\" class=\"link\"> Bestellnachrichten
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminReturn&amp;token=4d22afc04aa9ea7a2a68488c06fad5a5\" class=\"link\"> Warenrücksendungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"31\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminStats&amp;token=96c9f3fa17d6f8e6c9f5201b0c534582\" class=\"link\">
                    <i class=\"material-icons\">assessment</i> <span>Statistiken</span>
                  </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"41\">
              <span class=\"title\">Improve</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"42\">
                  <a href=\"/admin975acnmvl/index.php/module/catalog?_token=NUX2LCLw4RurwJqw4aCz_2yDuJxq2yGFc4TMsWRBXtQ\" class=\"link\">
                    <i class=\"material-icons\">extension</i> <span>Modules</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"43\">
                              <a href=\"/admin975acnmvl/index.php/module/catalog?_token=NUX2LCLw4RurwJqw4aCz_2yDuJxq2yGFc4TMsWRBXtQ\" class=\"link\"> Modules &amp; Services
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"45\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAddonsCatalog&amp;token=4237449fcb27c89f54b3e13aec2cd128\" class=\"link\"> Modules Catalog
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"46\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminThemes&amp;token=6751b6504f415a361515f6fe8a2abe64\" class=\"link\">
                    <i class=\"material-icons\">desktop_mac</i> <span>Design</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"47\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminThemes&amp;token=6751b6504f415a361515f6fe8a2abe64\" class=\"link\"> Templates &amp; Vorlagen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"48\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminThemesCatalog&amp;token=2a0eaa5a6d221a4505ea9f7b7c8e6f8a\" class=\"link\"> Theme Catalog
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"49\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCmsContent&amp;token=13638b2941d379f40273acd81e0372c0\" class=\"link\"> Pages
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"50\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminModulesPositions&amp;token=c2996aa94efb77a65c4f792df019f0fe\" class=\"link\"> Positionen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"51\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminImages&amp;token=0144749fef768d427d5d2692d7661737\" class=\"link\"> Image Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"116\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLinkWidget&amp;token=1d0178b2a7846a641f155e43bc9dd75b\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"52\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarriers&amp;token=abd2445b010b36d19f7f320cdebbc8be\" class=\"link\">
                    <i class=\"material-icons\">local_shipping</i> <span>Versand</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"53\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarriers&amp;token=abd2445b010b36d19f7f320cdebbc8be\" class=\"link\"> Versanddienste
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"54\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminShipping&amp;token=4644d3d8754f40b38c14d74b2ae22b0f\" class=\"link\"> Voreinstellungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"55\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPayment&amp;token=2273df9a32a2ffb2a45a3317e5d67f70\" class=\"link\">
                    <i class=\"material-icons\">payment</i> <span>Zahlung</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"56\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPayment&amp;token=2273df9a32a2ffb2a45a3317e5d67f70\" class=\"link\"> Payment Methods
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPaymentPreferences&amp;token=3a987bd863941fa3ae5d9dbb2acd84e5\" class=\"link\"> Preferences
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"58\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLocalization&amp;token=876531de0a977e75a6ae93ccad36dc5b\" class=\"link\">
                    <i class=\"material-icons\">language</i> <span>International</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"59\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLocalization&amp;token=876531de0a977e75a6ae93ccad36dc5b\" class=\"link\"> Lokalisierung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"64\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCountries&amp;token=d439564e0fd75460c1600bbbbf8035b8\" class=\"link\"> Locations
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"68\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminTaxes&amp;token=0c82df51bf9159d689ec816526626279\" class=\"link\"> Taxes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"71\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminTranslations&amp;token=a4ac548dc866c9f1201ab2cbec134c2b\" class=\"link\"> Übersetzungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"72\">
              <span class=\"title\">Configure</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"73\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPreferences&amp;token=84b8473e6147cb7b4038346e16df366f\" class=\"link\">
                    <i class=\"material-icons\">settings</i> <span>Shop Parameters</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"74\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPreferences&amp;token=84b8473e6147cb7b4038346e16df366f\" class=\"link\"> Allgemein
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"77\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrderPreferences&amp;token=aaab9238d1fc257104745333e4b3bfdc\" class=\"link\"> Order Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"80\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPPreferences&amp;token=54b154dfb4bd0f9a80243f276e27706f\" class=\"link\"> Produkte
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"81\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomerPreferences&amp;token=bba1002d18d4302ec91cad33fe101b1d\" class=\"link\"> Customer Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"85\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminContacts&amp;token=b624796bee2804b57531e5526da7846f\" class=\"link\"> Contact
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"88\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminMeta&amp;token=8c48f4f17145abf3e29ced7eb6279f1b\" class=\"link\"> Traffic
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"92\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminSearchConf&amp;token=2328779981529f1ada0cc5a1db74b57c\" class=\"link\"> Suche
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"95\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminInformation&amp;token=d086dc7cda5e105b8728905129c861ac\" class=\"link\">
                    <i class=\"material-icons\">settings_applications</i> <span>Erweiterte Einstellungen</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"96\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminInformation&amp;token=d086dc7cda5e105b8728905129c861ac\" class=\"link\"> Information
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"97\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPerformance&amp;token=9f21d46c95366dee2c7b1110e17870e7\" class=\"link\"> Leistung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"98\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAdminPreferences&amp;token=913836dfd029e70d929052b060e6b8a6\" class=\"link\"> Verwaltung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"99\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminEmails&amp;token=0a8c2436b5be61035d4bc4a5160ccbb3\" class=\"link\"> E-Mail
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"100\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminImport&amp;token=cc9c20abe1b7e255019999fd9c0812cf\" class=\"link\"> Import
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"101\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminEmployees&amp;token=c08e7d39a63850332deb01c9bc1f0e5b\" class=\"link\"> Mitarbeiter
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"105\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminRequestSql&amp;token=a03ff532cebd1e73a641aad997e20011\" class=\"link\"> Database
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"108\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLogs&amp;token=1d5fad2ddc6fb468bc9864d3a2263b49\" class=\"link\"> Log-Dateien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminWebservice&amp;token=4a799f855feaf376ba589242e4b6bd63\" class=\"link\"> Webdienste
                              </a>
                            </li>

                                                                                                                                                                            </ul>
                                    </li>
                          
        
            </ul>

  <span class=\"menu-collapse\">
    <i class=\"material-icons\">&#xE8EE;</i>
  </span>

  

</nav>


<div id=\"main-div\">

  
        
    <div class=\"content-div -notoolbar\">

      

      

      

      
      
      
      

      <div class=\"row \">
        <div class=\"col-xs-12\">
          <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>




  {% block content_header %}{% endblock %}
                 {% block content %}{% endblock %}
                 {% block content_footer %}{% endblock %}
                 {% block sidebar_right %}{% endblock %}

        </div>
      </div>

    </div>

  
</div>


  <div id=\"footer\" class=\"bootstrap hide\">
<!--
  <div class=\"col-sm-2 hidden-xs\">
    <a href=\"http://www.prestashop.com/\" class=\"_blank\">PrestaShop&trade;</a>
    -
    <span id=\"footer-load-time\"><i class=\"icon-time\" title=\"Ladezeit \"></i> 1.779s</span>
  </div>

  <div class=\"col-sm-2 hidden-xs\">
    <div class=\"social-networks\">
      <a class=\"link-social link-twitter _blank\" href=\"https://twitter.com/PrestaShop\" title=\"Twitter\">
        <i class=\"icon-twitter\"></i>
      </a>
      <a class=\"link-social link-facebook _blank\" href=\"https://www.facebook.com/prestashop\" title=\"Facebook\">
        <i class=\"icon-facebook\"></i>
      </a>
      <a class=\"link-social link-github _blank\" href=\"https://www.prestashop.com/github\" title=\"Github\">
        <i class=\"icon-github\"></i>
      </a>
      <a class=\"link-social link-google _blank\" href=\"https://plus.google.com/+prestashop/\" title=\"Google\">
        <i class=\"icon-google-plus\"></i>
      </a>
    </div>
  </div>
  <div class=\"col-sm-5\">
    <div class=\"footer-contact\">
      <a href=\"http://www.prestashop.com/en/contact_us?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-envelope\"></i>
        Kontakt
      </a>
      /&nbsp;
      <a href=\"http://forge.prestashop.com/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-bug\"></i>
        Bug-Tracker
      </a>
      /&nbsp;
      <a href=\"http://www.prestashop.com/forums/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-comments\"></i>
        Forum
      </a>
      /&nbsp;
      <a href=\"http://addons.prestashop.com/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-puzzle-piece\"></i>
        Addons
      </a>
      /&nbsp;
      <a href=\"http://www.prestashop.com/en/training-prestashop?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-book\"></i>
        Training
      </a>
                </div>
  </div>

  <div class=\"col-sm-3\">
    
  </div>

  <div id=\"go-top\" class=\"hide\"><i class=\"icon-arrow-up\"></i></div>
  -->
</div>



  <div class=\"bootstrap\">
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"http://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-DE&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t
<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/de/login?email=alexander.ebert%40pixabit.de&amp;firstname=Alexander&amp;lastname=Ebert&amp;website=http%3A%2F%2Fwarenkorb.pixabit.de%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/admin975acnmvl/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Verbinden Sie Ihren Shop mit Prestashops Marktplatz, um automatisch alle Kaufmodule zu importieren.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Sie haben noch keinen Account?</h4>
\t\t\t\t\t\t<p class='text-justify'>Entdecken Sie die Vielfalt der PrestaShop Addons! Stöbern Sie im offiziellen PrestaShop Martkplatz mit aktuell über 3 500 innovativen Templates und modularen Erweiterungen - ob es sich nun um Optimierung der Wechselkurse, Erhöhung der Zugriffsrate, Maßnahmen zur Kundenbindung oder Rentabilitätssteigerung handelt.</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Wechseln Sie zu PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link pull-right _blank\" href=\"//addons.prestashop.com/de/forgot-your-password\">Ich habe mein Passwort vergessen</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/de/login?email=alexander.ebert%40pixabit.de&amp;firstname=Alexander&amp;lastname=Ebert&amp;website=http%3A%2F%2Fwarenkorb.pixabit.de%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tErstellen Sie ein Konto
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Anmelden
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>
  </div>

{% block javascripts %}{% endblock %}{% block extra_javascripts %}{% endblock %}{% block translate_javascripts %}{% endblock %}</body>
</html>";
    }
}
