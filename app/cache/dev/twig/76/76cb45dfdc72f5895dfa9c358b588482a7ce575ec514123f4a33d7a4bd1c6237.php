<?php

/* PrestaShopBundle:Admin:Translations/include/button-toggle-messages-visibility.html.twig */
class __TwigTemplate_212046f9821ec39de32be44f7af576ce0d5109b4075f5e57bd2b486da5838b65 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_93086f06f28055683629b375480f1a0ef01aefddea5083c6af9d6322755fd405 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93086f06f28055683629b375480f1a0ef01aefddea5083c6af9d6322755fd405->enter($__internal_93086f06f28055683629b375480f1a0ef01aefddea5083c6af9d6322755fd405_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PrestaShopBundle:Admin:Translations/include/button-toggle-messages-visibility.html.twig"));

        // line 25
        echo "<div class=\"translation-domain\">
    <button class=\"btn btn-default btn-sm show-translation-messages\">";
        // line 26
        echo twig_escape_filter($this->env, (isset($context["label_show_messages"]) ? $context["label_show_messages"] : $this->getContext($context, "label_show_messages")), "html", null, true);
        echo "</button>
";
        
        $__internal_93086f06f28055683629b375480f1a0ef01aefddea5083c6af9d6322755fd405->leave($__internal_93086f06f28055683629b375480f1a0ef01aefddea5083c6af9d6322755fd405_prof);

    }

    public function getTemplateName()
    {
        return "PrestaShopBundle:Admin:Translations/include/button-toggle-messages-visibility.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 26,  22 => 25,);
    }

    public function getSource()
    {
        return "{#**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *#}
<div class=\"translation-domain\">
    <button class=\"btn btn-default btn-sm show-translation-messages\">{{ label_show_messages }}</button>
";
    }
}
