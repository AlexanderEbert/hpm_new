<?php

/* @Security/Collector/icon.svg */
class __TwigTemplate_6cc1d38631be5514a35e04cc042c8e6f06e58c5ef0521b29c70244abcb5aca0e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_420a024c439885c3d9bc11410f60e293b738aa7b79ebe80ad6f5efd64a5510c4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_420a024c439885c3d9bc11410f60e293b738aa7b79ebe80ad6f5efd64a5510c4->enter($__internal_420a024c439885c3d9bc11410f60e293b738aa7b79ebe80ad6f5efd64a5510c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
        
        $__internal_420a024c439885c3d9bc11410f60e293b738aa7b79ebe80ad6f5efd64a5510c4->leave($__internal_420a024c439885c3d9bc11410f60e293b738aa7b79ebe80ad6f5efd64a5510c4_prof);

    }

    public function getTemplateName()
    {
        return "@Security/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
    }
}
