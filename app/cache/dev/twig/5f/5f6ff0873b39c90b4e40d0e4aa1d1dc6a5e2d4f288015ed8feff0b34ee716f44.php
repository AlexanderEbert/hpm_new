<?php

/* PrestaShopBundle:Admin:Translations/include/pagination-bar.html.twig */
class __TwigTemplate_d9cb9346e3c03ba92c9d4cc4d30f6981373925ef811ba1083d4cf080380eb1ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1b6f28f38f9dcae5021c174f74a3dc0b66f2cb2ab3c4272af728164fa07f081c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b6f28f38f9dcae5021c174f74a3dc0b66f2cb2ab3c4272af728164fa07f081c->enter($__internal_1b6f28f38f9dcae5021c174f74a3dc0b66f2cb2ab3c4272af728164fa07f081c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PrestaShopBundle:Admin:Translations/include/pagination-bar.html.twig"));

        // line 25
        echo "<div class=\"navigation-container\">
  <nav class=\"hide\">
    <ul class=\"pagination\">
      <li class=\"page-item active\" data-page-index=\"1\"><a class=\"page-link\" href=\"#_";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["page_id"]) ? $context["page_id"] : $this->getContext($context, "page_id")), "html", null, true);
        echo "\">1</a></li>
      <li class=\"page-item tpl hide\"><a class=\"page-link\" href=\"#_";
        // line 29
        echo twig_escape_filter($this->env, (isset($context["page_id"]) ? $context["page_id"] : $this->getContext($context, "page_id")), "html", null, true);
        echo "\"></a></li>
    </ul>
  </nav>
</div>
";
        
        $__internal_1b6f28f38f9dcae5021c174f74a3dc0b66f2cb2ab3c4272af728164fa07f081c->leave($__internal_1b6f28f38f9dcae5021c174f74a3dc0b66f2cb2ab3c4272af728164fa07f081c_prof);

    }

    public function getTemplateName()
    {
        return "PrestaShopBundle:Admin:Translations/include/pagination-bar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 29,  27 => 28,  22 => 25,);
    }

    public function getSource()
    {
        return "{#**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *#}
<div class=\"navigation-container\">
  <nav class=\"hide\">
    <ul class=\"pagination\">
      <li class=\"page-item active\" data-page-index=\"1\"><a class=\"page-link\" href=\"#_{{ page_id }}\">1</a></li>
      <li class=\"page-item tpl hide\"><a class=\"page-link\" href=\"#_{{ page_id }}\"></a></li>
    </ul>
  </nav>
</div>
";
    }
}
