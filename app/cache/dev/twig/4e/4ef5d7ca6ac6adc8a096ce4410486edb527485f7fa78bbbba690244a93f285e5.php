<?php

/* PrestaShopBundle:Admin:Translations/include/translations-form-end.html.twig */
class __TwigTemplate_883e7ea45e812023080760e24bf65712890f9f54930eeb0be27126b87445dfd5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_af8e545f738f34503ba416a31a5ed51c3a4798057183437be3cc8f65175dc57e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_af8e545f738f34503ba416a31a5ed51c3a4798057183437be3cc8f65175dc57e->enter($__internal_af8e545f738f34503ba416a31a5ed51c3a4798057183437be3cc8f65175dc57e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PrestaShopBundle:Admin:Translations/include/translations-form-end.html.twig"));

        // line 25
        echo (isset($context["form_start"]) ? $context["form_start"] : $this->getContext($context, "form_start"));
        echo "
";
        // line 26
        echo (isset($context["subtree"]) ? $context["subtree"] : $this->getContext($context, "subtree"));
        echo "
  </div>
</div>
";
        
        $__internal_af8e545f738f34503ba416a31a5ed51c3a4798057183437be3cc8f65175dc57e->leave($__internal_af8e545f738f34503ba416a31a5ed51c3a4798057183437be3cc8f65175dc57e_prof);

    }

    public function getTemplateName()
    {
        return "PrestaShopBundle:Admin:Translations/include/translations-form-end.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 26,  22 => 25,);
    }

    public function getSource()
    {
        return "{#**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *#}
{{ form_start|raw }}
{{ subtree|raw }}
  </div>
</div>
";
    }
}
