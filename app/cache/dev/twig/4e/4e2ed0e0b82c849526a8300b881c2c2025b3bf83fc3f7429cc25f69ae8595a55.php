<?php

/* __string_template__3d56a97ca71a785f9de38f7952bdfa1fa1855e54ba6b7a2e16d425e99a183a01 */
class __TwigTemplate_45509d5b9813d08870a8b7c32deb4eb410a13f964da9a4c93aa013bc43d1770d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'extra_stylesheets' => array($this, 'block_extra_stylesheets'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
            'content_footer' => array($this, 'block_content_footer'),
            'sidebar_right' => array($this, 'block_sidebar_right'),
            'javascripts' => array($this, 'block_javascripts'),
            'extra_javascripts' => array($this, 'block_extra_javascripts'),
            'translate_javascripts' => array($this, 'block_translate_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b14f172946712f4b3c26591d6533ca32083a95c992dd699a76c02a1af94dc5f2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b14f172946712f4b3c26591d6533ca32083a95c992dd699a76c02a1af94dc5f2->enter($__internal_b14f172946712f4b3c26591d6533ca32083a95c992dd699a76c02a1af94dc5f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "__string_template__3d56a97ca71a785f9de38f7952bdfa1fa1855e54ba6b7a2e16d425e99a183a01"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"de\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>Produkte • HPM Warenkorbsystem</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminProducts';
    var iso_user = 'de';
    var full_language_code = 'de';
    var full_cldr_language_code = 'de-DE';
    var country_iso_code = 'DE';
    var _PS_VERSION_ = '1.7.0.4';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Eine neue Bestellung ist in Ihrem Shop eingegangen.';
    var order_number_msg = 'Bestell-Nr. ';
    var total_msg = 'Gesamt: ';
    var from_msg = 'von ';
    var see_order_msg = 'Diese Bestellung anzeigen';
    var new_customer_msg = 'Neue Kundenregistrierung im Shop.';
    var customer_name_msg = 'Kunde ';
    var new_msg = 'Ihr Shop hat eine neue Nachricht erhalten.';
    var see_msg = 'Nachricht lesen';
    var token = 'd890451df43ab16db8eaee1070cc55ac';
    var token_admin_orders = 'a46cf6d99e083739ea7752e56a6f5780';
    var token_admin_customers = 'fbc3e4e5d43e41c3437c1f72c3cfdcd9';
    var token_admin_customer_threads = 'a47777da39a221ce84096ed8bb8ff8a1';
    var currentIndex = 'index.php?controller=AdminProducts';
    var employee_token = '635b83c9af33dc717a89a7b48bef9b4b';
    var choose_language_translate = 'Wählen Sie eine Sprache';
    var default_language = '1';
    var admin_modules_link = '/admin975acnmvl/index.php/module/catalog/recommended?_token=lK7TVmYvKVUUasG8rZT_9DJ6pGCBDmCUF9J0NJAW_AE';
    var tab_modules_list = 'prestagiftvouchers,dmuassocprodcat,etranslation,apiway,prestashoptoquickbooks';
    var update_success_msg = 'Aktualisierung durchgeführt!';
    var errorLogin = 'PrestaShop konnte sich nicht bei Addons anmelden. Überprüfen Sie bitte Ihre Zugangsdaten und Ihre Internetverbindung.';
    var search_product_msg = 'Artikel suchen';
  </script>

      <link href=\"/admin975acnmvl/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/admin975acnmvl\\/\";
var baseDir = \"\\/\";
var currency = {\"iso_code\":\"EUR\",\"sign\":\"\\u20ac\",\"name\":\"Euro\",\"format\":\"#,##0.00\\u00a0\\u00a4\"};
var host_mode = false;
</script>
<script type=\"text/javascript\" src=\"/admin975acnmvl/themes/new-theme/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=1.7.0.4\"></script>
<script type=\"text/javascript\" src=\"/js/cldr.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=1.7.0.4\"></script>
<script type=\"text/javascript\" src=\"/admin975acnmvl/public/bundle.js\"></script>


  

";
        // line 66
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>
<body class=\"adminproducts\">



<header>
  <nav class=\"main-header\">

    
    

    
    <a class=\"logo pull-left\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=a46cf6d99e083739ea7752e56a6f5780\"></a>

    <div class=\"component pull-left\"><div class=\"ps-dropdown dropdown\">
  <span type=\"button\" id=\"quick-access\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
    <span class=\"selected-item\">Schnellzugriff</span> <i class=\"material-icons arrow-down\">keyboard_arrow_down</i>
  </span>
  <div class=\"ps-dropdown-menu dropdown-menu\" aria-labelledby=\"quick-access\">
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCategories&amp;addcategory&amp;token=433b7fdedc7ce5908e41e74d80bc5dba\"
                 data-item=\"Neue Kategorie\"
      >Neue Kategorie</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php/productnew?token=62c8de05834eb7b1922cfc7b566a3173\"
                 data-item=\"Neuer Artikel\"
      >Neuer Artikel</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=db4df5b97a9eba5ac0214fb4f30d68f0\"
                 data-item=\"Neuer Ermäßigungsgutschein\"
      >Neuer Ermäßigungsgutschein</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=a46cf6d99e083739ea7752e56a6f5780\"
                 data-item=\"Orders\"
      >Orders</a>
        <hr>
        <a
      class=\"dropdown-item js-quick-link\"
      data-rand=\"64\"
      data-icon=\"icon-AdminCatalog\"
      data-method=\"add\"
      data-url=\"index.php/productcatalog\"
      data-post-link=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminQuickAccesses&token=904acec762d821a6c5365ae3ee0b886a\"
      data-prompt-text=\"Bitte dieses Kürzel angeben:\"
      data-link=\"Produkte - Liste\"
    >
      <i class=\"material-icons\">add_circle_outline</i>
      Zu Favoriten hinzufügen
    </a>
    <a class=\"dropdown-item\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminQuickAccesses&token=904acec762d821a6c5365ae3ee0b886a\">
      <i class=\"material-icons\">settings</i>
      Schnellzugänge verwalten
    </a>
  </div>
</div>
</div>
    <div class=\"component\">

<form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form\"
      method=\"post\"
      action=\"/admin975acnmvl/index.php?controller=AdminSearch&amp;token=51472f3402fc5c6d5e86218cbec4f6c9\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input id=\"bo_query\" name=\"bo_query\" type=\"search\" class=\"form-control dropdown-form-search js-form-search\" value=\"\" placeholder=\"Suche (z.B. Bestell-Nr., Kundenname ...)\" />
    <div class=\"input-group-addon\">
      <div class=\"dropdown\">
        <span class=\"dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
          Überall
        </span>
        <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu\">
          <ul class=\"items-list js-items-list\">
            <li class=\"search-all search-option active\">
              <a class=\"dropdown-item\" data-item=\"Überall\" href=\"#\" data-value=\"0\" data-placeholder=\"Wonach suchen Sie?\" data-icon=\"icon-search\">
              <i class=\"material-icons\">search</i> Überall</a>
            </li>
            <hr>
            <li class=\"search-book search-option\">
              <a class=\"dropdown-item\" data-item=\"Katalog\" href=\"#\" data-value=\"1\" data-placeholder=\"Artikelname, Bestandseinheit, Artikel-Nr. ...\" data-icon=\"icon-book\">
                <i class=\"material-icons\">library_books</i> Katalog
              </a>
            </li>
            <li class=\"search-customers-name search-option\">
              <a class=\"dropdown-item\" data-item=\"Kunden nach Name\" href=\"#\" data-value=\"2\" data-placeholder=\"E-Mail , Name...\" data-icon=\"icon-group\">
                <i class=\"material-icons\">group</i> Kunden nach Name
              </a>
            </li>
            <li class=\"search-customers-addresses search-option\">
              <a class=\"dropdown-item\" data-item=\"Kunden nach IP-Adresse\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\">
                <i class=\"material-icons\">desktop_windows</i>Kunden nach IP-Adresse</a>
            </li>
            <li class=\"search-orders search-option\">
              <a class=\"dropdown-item\" data-item=\"Bestellungen\" href=\"#\" data-value=\"3\" data-placeholder=\"Bestell-Nr.\" data-icon=\"icon-credit-card\">
                <i class=\"material-icons\">credit_card</i> Bestellungen
              </a>
            </li>
            <li class=\"search-invoices search-option\">
              <a class=\"dropdown-item\" data-item=\"Rechnungen\" href=\"#\" data-value=\"4\" data-placeholder=\"Rechnungsnummer\" data-icon=\"icon-book\">
                <i class=\"material-icons\">book</i></i> Rechnungen
              </a>
            </li>
            <li class=\"search-carts search-option\">
              <a class=\"dropdown-item\" data-item=\"Warenkörbe\" href=\"#\" data-value=\"5\" data-placeholder=\"Warenkorb-ID\" data-icon=\"icon-shopping-cart\">
                <i class=\"material-icons\">shopping_cart</i> Warenkörbe
              </a>
            </li>
            <li class=\"search-modules search-option\">
              <a class=\"dropdown-item\" data-item=\"Module\" href=\"#\" data-value=\"7\" data-placeholder=\"Modul-Name\" data-icon=\"icon-puzzle-piece\">
                <i class=\"material-icons\">view_module</i> Module
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class=\"input-group-addon search-bar\">
      <button type=\"submit\">SUCHE<i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
  });
</script>
</div>


    <div class=\"component pull-md-right -norightmargin\"><div class=\"employee-dropdown dropdown\">
      <div class=\"img-circle person\" data-toggle=\"dropdown\">
      <i class=\"material-icons\">person</i>
    </div>
    <div class=\"dropdown-menu dropdown-menu-right p-a-1 m-r-2\">
    <div class=\"text-xs-center\">
      <img class=\"avatar img-circle\" src=\"https://profile.prestashop.com/ugur.%C3%B6zen%40pixabit.de.jpg\" /><br>
      Ugur Özen
    </div>
    <hr>
    <a class=\"employee-link\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminEmployees&amp;token=635b83c9af33dc717a89a7b48bef9b4b&amp;id_employee=7&amp;updateemployee\" target=\"_blank\">
      <i class=\"material-icons\">settings_applications</i> Ihr Profil
    </a>
    <a class=\"employee-link m-t-1\" id=\"header_logout\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLogin&amp;token=7d3ab74119cf05f1524c694a01e7fcb4&amp;logout\">
      <i class=\"material-icons\">power_settings_new</i> Abmelden
    </a>
  </div>
</div>
</div>
        <div class=\"component pull-md-right\">  <div class=\"shop-list\">
    <a class=\"link\" href=\"http://warenkorb.pixabit.de/\" target= \"_blank\">HPM Warenkorbsystem</a>
  </div>
</div>
              <div class=\"component pull-right\">
        <div class=\"shop-state\" id=\"debug-mode\">
          <i class=\"material-icons\">bug_report</i>
          <span class=\"label-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" data-html=\"true\"
            title=\"<p class='text-left text-nowrap'><strong>Ihr Shop befindet sich im Debug-Modus.</strong></p><p class='text-left'>Alle PHP-Fehler und -Nachrichten werden angezeigt. Wenn Sie dies nicht mehr benötigen, wählen Sie die Option <strong>Abschalten</strong>.</p>\">Debug-Modus</span>
        </div>
      </div>
        

    

    
    
  </nav>
</header>

<nav class=\"nav-bar\">
  <ul class=\"main-menu\">

          
                                  
                
        
          <li class=\"category-title -active\" data-submenu=\"2\">
              <span class=\"title\">Sell</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"3\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=a46cf6d99e083739ea7752e56a6f5780\" class=\"link\">
                    <i class=\"material-icons\">shopping_basket</i> <span>Bestellungen</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=a46cf6d99e083739ea7752e56a6f5780\" class=\"link\"> Bestellungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminInvoices&amp;token=ab5320e2e2699310fbbeb30081dcb70d\" class=\"link\"> Rechnungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminSlip&amp;token=fa88bd6ac9490f08ae51f7321db115ef\" class=\"link\"> Gutschriften
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarts&amp;token=8a2c2bbe21289d373b5dd8e04580451e\" class=\"link\"> Warenkörbe
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone -active\" data-submenu=\"9\">
                  <a href=\"/admin975acnmvl/index.php/product/catalog?_token=lK7TVmYvKVUUasG8rZT_9DJ6pGCBDmCUF9J0NJAW_AE\" class=\"link\">
                    <i class=\"material-icons\">store</i> <span>Katalog</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"10\">
                              <a href=\"/admin975acnmvl/index.php/product/catalog?_token=lK7TVmYvKVUUasG8rZT_9DJ6pGCBDmCUF9J0NJAW_AE\" class=\"link\"> Produkte
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCategories&amp;token=433b7fdedc7ce5908e41e74d80bc5dba\" class=\"link\"> Kategorien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminManufacturers&amp;token=2ee8cae449cb7edda26379551f5bb17c\" class=\"link\"> Brands &amp; Suppliers
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"23\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAddresses&amp;token=b720f17732bacdd8f24945b7f6aa6800\" class=\"link\">
                    <i class=\"material-icons\">account_circle</i> <span>Kunden</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAddresses&amp;token=b720f17732bacdd8f24945b7f6aa6800\" class=\"link\"> Adressen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"27\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrderMessage&amp;token=83e2c709c238a0bd8f8e6eebbf24e263\" class=\"link\">
                    <i class=\"material-icons\">chat</i> <span>Kundenservice</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrderMessage&amp;token=83e2c709c238a0bd8f8e6eebbf24e263\" class=\"link\"> Bestellnachrichten
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"72\">
              <span class=\"title\">Configure</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"73\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminReferrers&amp;token=b89db223daf4627e307e36814180a214\" class=\"link\">
                    <i class=\"material-icons\">settings</i> <span>Shop Parameters</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"88\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminReferrers&amp;token=b89db223daf4627e307e36814180a214\" class=\"link\"> Traffic
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"92\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminSearchConf&amp;token=de3c9f4c16d27a71971bedcd26011b47\" class=\"link\"> Suche
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"95\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminWebservice&amp;token=cb8151eb5f2b5ed6de40f44242a55c8a\" class=\"link\">
                    <i class=\"material-icons\">settings_applications</i> <span>Erweiterte Einstellungen</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminWebservice&amp;token=cb8151eb5f2b5ed6de40f44242a55c8a\" class=\"link\"> Webdienste
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
            </ul>

  <span class=\"menu-collapse\">
    <i class=\"material-icons\">&#xE8EE;</i>
  </span>

  

</nav>


<div id=\"main-div\">

  
    
<div class=\"header-toolbar\">

  
    <ol class=\"breadcrumb\">

              <li>
                      <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCatalog&amp;token=4272d0f3722cc78e3ec8e88d9bd54503\">Katalog</a>
                  </li>
      
              <li>
                      <a href=\"/admin975acnmvl/index.php/product/catalog?_token=lK7TVmYvKVUUasG8rZT_9DJ6pGCBDmCUF9J0NJAW_AE\">Produkte</a>
                  </li>
      
    </ol>
  

  
    <h2 class=\"title\">
      Produkte    </h2>
  

  
    <div class=\"toolbar-icons\">
                                      
          <a
            class=\"m-b-2 m-r-1 btn btn-primary  pointer\"            id=\"page-header-desc-configuration-add\"
            href=\"/admin975acnmvl/index.php/product/new?_token=lK7TVmYvKVUUasG8rZT_9DJ6pGCBDmCUF9J0NJAW_AE\"            title=\"Speichern und nächsten Artikel anlegen: STRG + P\"            data-toggle=\"tooltip\"
            data-placement=\"bottom\"          >
            <i class=\"material-icons\">add_circle_outline</i>
            <span class=\"title\">Neuer Artikel</span>
          </a>
                            
        <a
          class=\"toolbar-button toolbar_btn\"
          id=\"page-header-desc-configuration-modules-list\"
          href=\"/admin975acnmvl/index.php/module/catalog?_token=lK7TVmYvKVUUasG8rZT_9DJ6pGCBDmCUF9J0NJAW_AE\"          title=\"Empfohlene Module und Dienste\"
                  >
                      <i class=\"material-icons\">extension</i>
                    <span class=\"title\">Empfohlene Module und Dienste</span>
        </a>
            
                  <a class=\"toolbar-button btn-help btn-sidebar\" href=\"#\"
             title=\"Hilfe\"
             data-toggle=\"sidebar\"
             data-target=\"#right-sidebar\"
             data-url=\"/admin975acnmvl/index.php/common/sidebar/http%253A%252F%252Fhelp.prestashop.com%252Fde%252Fdoc%252FAdminProducts%253Fversion%253D1.7.0.4%2526country%253Dde/Hilfe?_token=lK7TVmYvKVUUasG8rZT_9DJ6pGCBDmCUF9J0NJAW_AE\"
             id=\"product_form_open_help\"
          >
            <i class=\"material-icons\">help</i>
            <span class=\"title\">Hilfe</span>
          </a>
                  </div>
  
    
</div>
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"http://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-DE&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t
<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/de/login?email=ugur.%C3%B6zen%40pixabit.de&amp;firstname=Ugur&amp;lastname=%C3%96zen&amp;website=http%3A%2F%2Fwarenkorb.pixabit.de%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/admin975acnmvl/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Verbinden Sie Ihren Shop mit Prestashops Marktplatz, um automatisch alle Kaufmodule zu importieren.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Sie haben noch keinen Account?</h4>
\t\t\t\t\t\t<p class='text-justify'>Entdecken Sie die Vielfalt der PrestaShop Addons! Stöbern Sie im offiziellen PrestaShop Martkplatz mit aktuell über 3 500 innovativen Templates und modularen Erweiterungen - ob es sich nun um Optimierung der Wechselkurse, Erhöhung der Zugriffsrate, Maßnahmen zur Kundenbindung oder Rentabilitätssteigerung handelt.</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Wechseln Sie zu PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link pull-right _blank\" href=\"//addons.prestashop.com/de/forgot-your-password\">Ich habe mein Passwort vergessen</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/de/login?email=ugur.%C3%B6zen%40pixabit.de&amp;firstname=Ugur&amp;lastname=%C3%96zen&amp;website=http%3A%2F%2Fwarenkorb.pixabit.de%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tErstellen Sie ein Konto
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Anmelden
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>
    <div class=\"content-div \">

      

      

      

      
      
      
      

      <div class=\"row \">
        <div class=\"col-xs-12\">
          <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>




  ";
        // line 572
        $this->displayBlock('content_header', $context, $blocks);
        // line 573
        echo "                 ";
        $this->displayBlock('content', $context, $blocks);
        // line 574
        echo "                 ";
        $this->displayBlock('content_footer', $context, $blocks);
        // line 575
        echo "                 ";
        $this->displayBlock('sidebar_right', $context, $blocks);
        // line 576
        echo "
        </div>
      </div>

    </div>

  
</div>


  <div id=\"footer\" class=\"bootstrap hide\">
<!--
  <div class=\"col-sm-2 hidden-xs\">
    <a href=\"http://www.prestashop.com/\" class=\"_blank\">PrestaShop&trade;</a>
    -
    <span id=\"footer-load-time\"><i class=\"icon-time\" title=\"Ladezeit \"></i> 2.638s</span>
  </div>

  <div class=\"col-sm-2 hidden-xs\">
    <div class=\"social-networks\">
      <a class=\"link-social link-twitter _blank\" href=\"https://twitter.com/PrestaShop\" title=\"Twitter\">
        <i class=\"icon-twitter\"></i>
      </a>
      <a class=\"link-social link-facebook _blank\" href=\"https://www.facebook.com/prestashop\" title=\"Facebook\">
        <i class=\"icon-facebook\"></i>
      </a>
      <a class=\"link-social link-github _blank\" href=\"https://www.prestashop.com/github\" title=\"Github\">
        <i class=\"icon-github\"></i>
      </a>
      <a class=\"link-social link-google _blank\" href=\"https://plus.google.com/+prestashop/\" title=\"Google\">
        <i class=\"icon-google-plus\"></i>
      </a>
    </div>
  </div>
  <div class=\"col-sm-5\">
    <div class=\"footer-contact\">
      <a href=\"http://www.prestashop.com/en/contact_us?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-envelope\"></i>
        Kontakt
      </a>
      /&nbsp;
      <a href=\"http://forge.prestashop.com/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-bug\"></i>
        Bug-Tracker
      </a>
      /&nbsp;
      <a href=\"http://www.prestashop.com/forums/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-comments\"></i>
        Forum
      </a>
      /&nbsp;
      <a href=\"http://addons.prestashop.com/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-puzzle-piece\"></i>
        Addons
      </a>
      /&nbsp;
      <a href=\"http://www.prestashop.com/en/training-prestashop?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-book\"></i>
        Training
      </a>
                </div>
  </div>

  <div class=\"col-sm-3\">
    
  </div>

  <div id=\"go-top\" class=\"hide\"><i class=\"icon-arrow-up\"></i></div>
  -->
</div>



  <div class=\"bootstrap\">
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"http://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-DE&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t
<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/de/login?email=ugur.%C3%B6zen%40pixabit.de&amp;firstname=Ugur&amp;lastname=%C3%96zen&amp;website=http%3A%2F%2Fwarenkorb.pixabit.de%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/admin975acnmvl/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Verbinden Sie Ihren Shop mit Prestashops Marktplatz, um automatisch alle Kaufmodule zu importieren.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Sie haben noch keinen Account?</h4>
\t\t\t\t\t\t<p class='text-justify'>Entdecken Sie die Vielfalt der PrestaShop Addons! Stöbern Sie im offiziellen PrestaShop Martkplatz mit aktuell über 3 500 innovativen Templates und modularen Erweiterungen - ob es sich nun um Optimierung der Wechselkurse, Erhöhung der Zugriffsrate, Maßnahmen zur Kundenbindung oder Rentabilitätssteigerung handelt.</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Wechseln Sie zu PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link pull-right _blank\" href=\"//addons.prestashop.com/de/forgot-your-password\">Ich habe mein Passwort vergessen</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/de/login?email=ugur.%C3%B6zen%40pixabit.de&amp;firstname=Ugur&amp;lastname=%C3%96zen&amp;website=http%3A%2F%2Fwarenkorb.pixabit.de%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tErstellen Sie ein Konto
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Anmelden
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>
  </div>

";
        // line 721
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>
</html>";
        
        $__internal_b14f172946712f4b3c26591d6533ca32083a95c992dd699a76c02a1af94dc5f2->leave($__internal_b14f172946712f4b3c26591d6533ca32083a95c992dd699a76c02a1af94dc5f2_prof);

    }

    // line 66
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_29b06dde39331457b35485ebf72c0a7ea5b660dfd947ac6248e21d93fcc27af8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_29b06dde39331457b35485ebf72c0a7ea5b660dfd947ac6248e21d93fcc27af8->enter($__internal_29b06dde39331457b35485ebf72c0a7ea5b660dfd947ac6248e21d93fcc27af8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_29b06dde39331457b35485ebf72c0a7ea5b660dfd947ac6248e21d93fcc27af8->leave($__internal_29b06dde39331457b35485ebf72c0a7ea5b660dfd947ac6248e21d93fcc27af8_prof);

    }

    public function block_extra_stylesheets($context, array $blocks = array())
    {
        $__internal_442ee45c5f3442a043d77e55fb78ba39614debc91b88d46a724cc5be195c38e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_442ee45c5f3442a043d77e55fb78ba39614debc91b88d46a724cc5be195c38e7->enter($__internal_442ee45c5f3442a043d77e55fb78ba39614debc91b88d46a724cc5be195c38e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "extra_stylesheets"));

        
        $__internal_442ee45c5f3442a043d77e55fb78ba39614debc91b88d46a724cc5be195c38e7->leave($__internal_442ee45c5f3442a043d77e55fb78ba39614debc91b88d46a724cc5be195c38e7_prof);

    }

    // line 572
    public function block_content_header($context, array $blocks = array())
    {
        $__internal_ec1d38fc8a53da90f86e775f404b2ee321506997ef15b3463018557f1cecb1b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ec1d38fc8a53da90f86e775f404b2ee321506997ef15b3463018557f1cecb1b1->enter($__internal_ec1d38fc8a53da90f86e775f404b2ee321506997ef15b3463018557f1cecb1b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        
        $__internal_ec1d38fc8a53da90f86e775f404b2ee321506997ef15b3463018557f1cecb1b1->leave($__internal_ec1d38fc8a53da90f86e775f404b2ee321506997ef15b3463018557f1cecb1b1_prof);

    }

    // line 573
    public function block_content($context, array $blocks = array())
    {
        $__internal_7798164a5f24050a5dbd697233cb6d2039b27cec0a487f6a612557bca069583b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7798164a5f24050a5dbd697233cb6d2039b27cec0a487f6a612557bca069583b->enter($__internal_7798164a5f24050a5dbd697233cb6d2039b27cec0a487f6a612557bca069583b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_7798164a5f24050a5dbd697233cb6d2039b27cec0a487f6a612557bca069583b->leave($__internal_7798164a5f24050a5dbd697233cb6d2039b27cec0a487f6a612557bca069583b_prof);

    }

    // line 574
    public function block_content_footer($context, array $blocks = array())
    {
        $__internal_5b5bc77a303954f830c51abe41b45d06c1c1ca490e7d1dbb08012b13bb51c273 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b5bc77a303954f830c51abe41b45d06c1c1ca490e7d1dbb08012b13bb51c273->enter($__internal_5b5bc77a303954f830c51abe41b45d06c1c1ca490e7d1dbb08012b13bb51c273_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_footer"));

        
        $__internal_5b5bc77a303954f830c51abe41b45d06c1c1ca490e7d1dbb08012b13bb51c273->leave($__internal_5b5bc77a303954f830c51abe41b45d06c1c1ca490e7d1dbb08012b13bb51c273_prof);

    }

    // line 575
    public function block_sidebar_right($context, array $blocks = array())
    {
        $__internal_0438fdcce913c98efa068e11f4bb8073693408545c1a7bc9d964ac4a1e492c32 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0438fdcce913c98efa068e11f4bb8073693408545c1a7bc9d964ac4a1e492c32->enter($__internal_0438fdcce913c98efa068e11f4bb8073693408545c1a7bc9d964ac4a1e492c32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar_right"));

        
        $__internal_0438fdcce913c98efa068e11f4bb8073693408545c1a7bc9d964ac4a1e492c32->leave($__internal_0438fdcce913c98efa068e11f4bb8073693408545c1a7bc9d964ac4a1e492c32_prof);

    }

    // line 721
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_f8b5fa049d80d8f2a5762c1efa48ccaf081a73ca07a8bd7c7d3b6d2074f23090 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f8b5fa049d80d8f2a5762c1efa48ccaf081a73ca07a8bd7c7d3b6d2074f23090->enter($__internal_f8b5fa049d80d8f2a5762c1efa48ccaf081a73ca07a8bd7c7d3b6d2074f23090_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_f8b5fa049d80d8f2a5762c1efa48ccaf081a73ca07a8bd7c7d3b6d2074f23090->leave($__internal_f8b5fa049d80d8f2a5762c1efa48ccaf081a73ca07a8bd7c7d3b6d2074f23090_prof);

    }

    public function block_extra_javascripts($context, array $blocks = array())
    {
        $__internal_8c321aa0319bfead09bf08925a0bdbd1bb96e962d7175774800f0d84a0d06a84 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c321aa0319bfead09bf08925a0bdbd1bb96e962d7175774800f0d84a0d06a84->enter($__internal_8c321aa0319bfead09bf08925a0bdbd1bb96e962d7175774800f0d84a0d06a84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "extra_javascripts"));

        
        $__internal_8c321aa0319bfead09bf08925a0bdbd1bb96e962d7175774800f0d84a0d06a84->leave($__internal_8c321aa0319bfead09bf08925a0bdbd1bb96e962d7175774800f0d84a0d06a84_prof);

    }

    public function block_translate_javascripts($context, array $blocks = array())
    {
        $__internal_6a564acf15079fa1abf479cbf7cf2ca15946ef5342641cd542d7170ed700a259 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6a564acf15079fa1abf479cbf7cf2ca15946ef5342641cd542d7170ed700a259->enter($__internal_6a564acf15079fa1abf479cbf7cf2ca15946ef5342641cd542d7170ed700a259_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "translate_javascripts"));

        
        $__internal_6a564acf15079fa1abf479cbf7cf2ca15946ef5342641cd542d7170ed700a259->leave($__internal_6a564acf15079fa1abf479cbf7cf2ca15946ef5342641cd542d7170ed700a259_prof);

    }

    public function getTemplateName()
    {
        return "__string_template__3d56a97ca71a785f9de38f7952bdfa1fa1855e54ba6b7a2e16d425e99a183a01";
    }

    public function getDebugInfo()
    {
        return array (  842 => 721,  831 => 575,  820 => 574,  809 => 573,  798 => 572,  777 => 66,  766 => 721,  619 => 576,  616 => 575,  613 => 574,  610 => 573,  608 => 572,  98 => 66,  31 => 1,);
    }

    public function getSource()
    {
        return "<!DOCTYPE html>
<html lang=\"de\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>Produkte • HPM Warenkorbsystem</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminProducts';
    var iso_user = 'de';
    var full_language_code = 'de';
    var full_cldr_language_code = 'de-DE';
    var country_iso_code = 'DE';
    var _PS_VERSION_ = '1.7.0.4';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Eine neue Bestellung ist in Ihrem Shop eingegangen.';
    var order_number_msg = 'Bestell-Nr. ';
    var total_msg = 'Gesamt: ';
    var from_msg = 'von ';
    var see_order_msg = 'Diese Bestellung anzeigen';
    var new_customer_msg = 'Neue Kundenregistrierung im Shop.';
    var customer_name_msg = 'Kunde ';
    var new_msg = 'Ihr Shop hat eine neue Nachricht erhalten.';
    var see_msg = 'Nachricht lesen';
    var token = 'd890451df43ab16db8eaee1070cc55ac';
    var token_admin_orders = 'a46cf6d99e083739ea7752e56a6f5780';
    var token_admin_customers = 'fbc3e4e5d43e41c3437c1f72c3cfdcd9';
    var token_admin_customer_threads = 'a47777da39a221ce84096ed8bb8ff8a1';
    var currentIndex = 'index.php?controller=AdminProducts';
    var employee_token = '635b83c9af33dc717a89a7b48bef9b4b';
    var choose_language_translate = 'Wählen Sie eine Sprache';
    var default_language = '1';
    var admin_modules_link = '/admin975acnmvl/index.php/module/catalog/recommended?_token=lK7TVmYvKVUUasG8rZT_9DJ6pGCBDmCUF9J0NJAW_AE';
    var tab_modules_list = 'prestagiftvouchers,dmuassocprodcat,etranslation,apiway,prestashoptoquickbooks';
    var update_success_msg = 'Aktualisierung durchgeführt!';
    var errorLogin = 'PrestaShop konnte sich nicht bei Addons anmelden. Überprüfen Sie bitte Ihre Zugangsdaten und Ihre Internetverbindung.';
    var search_product_msg = 'Artikel suchen';
  </script>

      <link href=\"/admin975acnmvl/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/admin975acnmvl\\/\";
var baseDir = \"\\/\";
var currency = {\"iso_code\":\"EUR\",\"sign\":\"\\u20ac\",\"name\":\"Euro\",\"format\":\"#,##0.00\\u00a0\\u00a4\"};
var host_mode = false;
</script>
<script type=\"text/javascript\" src=\"/admin975acnmvl/themes/new-theme/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=1.7.0.4\"></script>
<script type=\"text/javascript\" src=\"/js/cldr.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=1.7.0.4\"></script>
<script type=\"text/javascript\" src=\"/admin975acnmvl/public/bundle.js\"></script>


  

{% block stylesheets %}{% endblock %}{% block extra_stylesheets %}{% endblock %}</head>
<body class=\"adminproducts\">



<header>
  <nav class=\"main-header\">

    
    

    
    <a class=\"logo pull-left\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=a46cf6d99e083739ea7752e56a6f5780\"></a>

    <div class=\"component pull-left\"><div class=\"ps-dropdown dropdown\">
  <span type=\"button\" id=\"quick-access\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
    <span class=\"selected-item\">Schnellzugriff</span> <i class=\"material-icons arrow-down\">keyboard_arrow_down</i>
  </span>
  <div class=\"ps-dropdown-menu dropdown-menu\" aria-labelledby=\"quick-access\">
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCategories&amp;addcategory&amp;token=433b7fdedc7ce5908e41e74d80bc5dba\"
                 data-item=\"Neue Kategorie\"
      >Neue Kategorie</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php/productnew?token=62c8de05834eb7b1922cfc7b566a3173\"
                 data-item=\"Neuer Artikel\"
      >Neuer Artikel</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=db4df5b97a9eba5ac0214fb4f30d68f0\"
                 data-item=\"Neuer Ermäßigungsgutschein\"
      >Neuer Ermäßigungsgutschein</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=a46cf6d99e083739ea7752e56a6f5780\"
                 data-item=\"Orders\"
      >Orders</a>
        <hr>
        <a
      class=\"dropdown-item js-quick-link\"
      data-rand=\"64\"
      data-icon=\"icon-AdminCatalog\"
      data-method=\"add\"
      data-url=\"index.php/productcatalog\"
      data-post-link=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminQuickAccesses&token=904acec762d821a6c5365ae3ee0b886a\"
      data-prompt-text=\"Bitte dieses Kürzel angeben:\"
      data-link=\"Produkte - Liste\"
    >
      <i class=\"material-icons\">add_circle_outline</i>
      Zu Favoriten hinzufügen
    </a>
    <a class=\"dropdown-item\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminQuickAccesses&token=904acec762d821a6c5365ae3ee0b886a\">
      <i class=\"material-icons\">settings</i>
      Schnellzugänge verwalten
    </a>
  </div>
</div>
</div>
    <div class=\"component\">

<form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form\"
      method=\"post\"
      action=\"/admin975acnmvl/index.php?controller=AdminSearch&amp;token=51472f3402fc5c6d5e86218cbec4f6c9\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input id=\"bo_query\" name=\"bo_query\" type=\"search\" class=\"form-control dropdown-form-search js-form-search\" value=\"\" placeholder=\"Suche (z.B. Bestell-Nr., Kundenname ...)\" />
    <div class=\"input-group-addon\">
      <div class=\"dropdown\">
        <span class=\"dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
          Überall
        </span>
        <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu\">
          <ul class=\"items-list js-items-list\">
            <li class=\"search-all search-option active\">
              <a class=\"dropdown-item\" data-item=\"Überall\" href=\"#\" data-value=\"0\" data-placeholder=\"Wonach suchen Sie?\" data-icon=\"icon-search\">
              <i class=\"material-icons\">search</i> Überall</a>
            </li>
            <hr>
            <li class=\"search-book search-option\">
              <a class=\"dropdown-item\" data-item=\"Katalog\" href=\"#\" data-value=\"1\" data-placeholder=\"Artikelname, Bestandseinheit, Artikel-Nr. ...\" data-icon=\"icon-book\">
                <i class=\"material-icons\">library_books</i> Katalog
              </a>
            </li>
            <li class=\"search-customers-name search-option\">
              <a class=\"dropdown-item\" data-item=\"Kunden nach Name\" href=\"#\" data-value=\"2\" data-placeholder=\"E-Mail , Name...\" data-icon=\"icon-group\">
                <i class=\"material-icons\">group</i> Kunden nach Name
              </a>
            </li>
            <li class=\"search-customers-addresses search-option\">
              <a class=\"dropdown-item\" data-item=\"Kunden nach IP-Adresse\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\">
                <i class=\"material-icons\">desktop_windows</i>Kunden nach IP-Adresse</a>
            </li>
            <li class=\"search-orders search-option\">
              <a class=\"dropdown-item\" data-item=\"Bestellungen\" href=\"#\" data-value=\"3\" data-placeholder=\"Bestell-Nr.\" data-icon=\"icon-credit-card\">
                <i class=\"material-icons\">credit_card</i> Bestellungen
              </a>
            </li>
            <li class=\"search-invoices search-option\">
              <a class=\"dropdown-item\" data-item=\"Rechnungen\" href=\"#\" data-value=\"4\" data-placeholder=\"Rechnungsnummer\" data-icon=\"icon-book\">
                <i class=\"material-icons\">book</i></i> Rechnungen
              </a>
            </li>
            <li class=\"search-carts search-option\">
              <a class=\"dropdown-item\" data-item=\"Warenkörbe\" href=\"#\" data-value=\"5\" data-placeholder=\"Warenkorb-ID\" data-icon=\"icon-shopping-cart\">
                <i class=\"material-icons\">shopping_cart</i> Warenkörbe
              </a>
            </li>
            <li class=\"search-modules search-option\">
              <a class=\"dropdown-item\" data-item=\"Module\" href=\"#\" data-value=\"7\" data-placeholder=\"Modul-Name\" data-icon=\"icon-puzzle-piece\">
                <i class=\"material-icons\">view_module</i> Module
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class=\"input-group-addon search-bar\">
      <button type=\"submit\">SUCHE<i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
  });
</script>
</div>


    <div class=\"component pull-md-right -norightmargin\"><div class=\"employee-dropdown dropdown\">
      <div class=\"img-circle person\" data-toggle=\"dropdown\">
      <i class=\"material-icons\">person</i>
    </div>
    <div class=\"dropdown-menu dropdown-menu-right p-a-1 m-r-2\">
    <div class=\"text-xs-center\">
      <img class=\"avatar img-circle\" src=\"https://profile.prestashop.com/ugur.%C3%B6zen%40pixabit.de.jpg\" /><br>
      Ugur Özen
    </div>
    <hr>
    <a class=\"employee-link\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminEmployees&amp;token=635b83c9af33dc717a89a7b48bef9b4b&amp;id_employee=7&amp;updateemployee\" target=\"_blank\">
      <i class=\"material-icons\">settings_applications</i> Ihr Profil
    </a>
    <a class=\"employee-link m-t-1\" id=\"header_logout\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLogin&amp;token=7d3ab74119cf05f1524c694a01e7fcb4&amp;logout\">
      <i class=\"material-icons\">power_settings_new</i> Abmelden
    </a>
  </div>
</div>
</div>
        <div class=\"component pull-md-right\">  <div class=\"shop-list\">
    <a class=\"link\" href=\"http://warenkorb.pixabit.de/\" target= \"_blank\">HPM Warenkorbsystem</a>
  </div>
</div>
              <div class=\"component pull-right\">
        <div class=\"shop-state\" id=\"debug-mode\">
          <i class=\"material-icons\">bug_report</i>
          <span class=\"label-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" data-html=\"true\"
            title=\"<p class='text-left text-nowrap'><strong>Ihr Shop befindet sich im Debug-Modus.</strong></p><p class='text-left'>Alle PHP-Fehler und -Nachrichten werden angezeigt. Wenn Sie dies nicht mehr benötigen, wählen Sie die Option <strong>Abschalten</strong>.</p>\">Debug-Modus</span>
        </div>
      </div>
        

    

    
    
  </nav>
</header>

<nav class=\"nav-bar\">
  <ul class=\"main-menu\">

          
                                  
                
        
          <li class=\"category-title -active\" data-submenu=\"2\">
              <span class=\"title\">Sell</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"3\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=a46cf6d99e083739ea7752e56a6f5780\" class=\"link\">
                    <i class=\"material-icons\">shopping_basket</i> <span>Bestellungen</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=a46cf6d99e083739ea7752e56a6f5780\" class=\"link\"> Bestellungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminInvoices&amp;token=ab5320e2e2699310fbbeb30081dcb70d\" class=\"link\"> Rechnungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminSlip&amp;token=fa88bd6ac9490f08ae51f7321db115ef\" class=\"link\"> Gutschriften
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarts&amp;token=8a2c2bbe21289d373b5dd8e04580451e\" class=\"link\"> Warenkörbe
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone -active\" data-submenu=\"9\">
                  <a href=\"/admin975acnmvl/index.php/product/catalog?_token=lK7TVmYvKVUUasG8rZT_9DJ6pGCBDmCUF9J0NJAW_AE\" class=\"link\">
                    <i class=\"material-icons\">store</i> <span>Katalog</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"10\">
                              <a href=\"/admin975acnmvl/index.php/product/catalog?_token=lK7TVmYvKVUUasG8rZT_9DJ6pGCBDmCUF9J0NJAW_AE\" class=\"link\"> Produkte
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCategories&amp;token=433b7fdedc7ce5908e41e74d80bc5dba\" class=\"link\"> Kategorien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminManufacturers&amp;token=2ee8cae449cb7edda26379551f5bb17c\" class=\"link\"> Brands &amp; Suppliers
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"23\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAddresses&amp;token=b720f17732bacdd8f24945b7f6aa6800\" class=\"link\">
                    <i class=\"material-icons\">account_circle</i> <span>Kunden</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAddresses&amp;token=b720f17732bacdd8f24945b7f6aa6800\" class=\"link\"> Adressen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"27\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrderMessage&amp;token=83e2c709c238a0bd8f8e6eebbf24e263\" class=\"link\">
                    <i class=\"material-icons\">chat</i> <span>Kundenservice</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrderMessage&amp;token=83e2c709c238a0bd8f8e6eebbf24e263\" class=\"link\"> Bestellnachrichten
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"72\">
              <span class=\"title\">Configure</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"73\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminReferrers&amp;token=b89db223daf4627e307e36814180a214\" class=\"link\">
                    <i class=\"material-icons\">settings</i> <span>Shop Parameters</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"88\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminReferrers&amp;token=b89db223daf4627e307e36814180a214\" class=\"link\"> Traffic
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"92\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminSearchConf&amp;token=de3c9f4c16d27a71971bedcd26011b47\" class=\"link\"> Suche
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"95\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminWebservice&amp;token=cb8151eb5f2b5ed6de40f44242a55c8a\" class=\"link\">
                    <i class=\"material-icons\">settings_applications</i> <span>Erweiterte Einstellungen</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminWebservice&amp;token=cb8151eb5f2b5ed6de40f44242a55c8a\" class=\"link\"> Webdienste
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
            </ul>

  <span class=\"menu-collapse\">
    <i class=\"material-icons\">&#xE8EE;</i>
  </span>

  

</nav>


<div id=\"main-div\">

  
    
<div class=\"header-toolbar\">

  
    <ol class=\"breadcrumb\">

              <li>
                      <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCatalog&amp;token=4272d0f3722cc78e3ec8e88d9bd54503\">Katalog</a>
                  </li>
      
              <li>
                      <a href=\"/admin975acnmvl/index.php/product/catalog?_token=lK7TVmYvKVUUasG8rZT_9DJ6pGCBDmCUF9J0NJAW_AE\">Produkte</a>
                  </li>
      
    </ol>
  

  
    <h2 class=\"title\">
      Produkte    </h2>
  

  
    <div class=\"toolbar-icons\">
                                      
          <a
            class=\"m-b-2 m-r-1 btn btn-primary  pointer\"            id=\"page-header-desc-configuration-add\"
            href=\"/admin975acnmvl/index.php/product/new?_token=lK7TVmYvKVUUasG8rZT_9DJ6pGCBDmCUF9J0NJAW_AE\"            title=\"Speichern und nächsten Artikel anlegen: STRG + P\"            data-toggle=\"tooltip\"
            data-placement=\"bottom\"          >
            <i class=\"material-icons\">add_circle_outline</i>
            <span class=\"title\">Neuer Artikel</span>
          </a>
                            
        <a
          class=\"toolbar-button toolbar_btn\"
          id=\"page-header-desc-configuration-modules-list\"
          href=\"/admin975acnmvl/index.php/module/catalog?_token=lK7TVmYvKVUUasG8rZT_9DJ6pGCBDmCUF9J0NJAW_AE\"          title=\"Empfohlene Module und Dienste\"
                  >
                      <i class=\"material-icons\">extension</i>
                    <span class=\"title\">Empfohlene Module und Dienste</span>
        </a>
            
                  <a class=\"toolbar-button btn-help btn-sidebar\" href=\"#\"
             title=\"Hilfe\"
             data-toggle=\"sidebar\"
             data-target=\"#right-sidebar\"
             data-url=\"/admin975acnmvl/index.php/common/sidebar/http%253A%252F%252Fhelp.prestashop.com%252Fde%252Fdoc%252FAdminProducts%253Fversion%253D1.7.0.4%2526country%253Dde/Hilfe?_token=lK7TVmYvKVUUasG8rZT_9DJ6pGCBDmCUF9J0NJAW_AE\"
             id=\"product_form_open_help\"
          >
            <i class=\"material-icons\">help</i>
            <span class=\"title\">Hilfe</span>
          </a>
                  </div>
  
    
</div>
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"http://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-DE&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t
<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/de/login?email=ugur.%C3%B6zen%40pixabit.de&amp;firstname=Ugur&amp;lastname=%C3%96zen&amp;website=http%3A%2F%2Fwarenkorb.pixabit.de%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/admin975acnmvl/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Verbinden Sie Ihren Shop mit Prestashops Marktplatz, um automatisch alle Kaufmodule zu importieren.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Sie haben noch keinen Account?</h4>
\t\t\t\t\t\t<p class='text-justify'>Entdecken Sie die Vielfalt der PrestaShop Addons! Stöbern Sie im offiziellen PrestaShop Martkplatz mit aktuell über 3 500 innovativen Templates und modularen Erweiterungen - ob es sich nun um Optimierung der Wechselkurse, Erhöhung der Zugriffsrate, Maßnahmen zur Kundenbindung oder Rentabilitätssteigerung handelt.</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Wechseln Sie zu PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link pull-right _blank\" href=\"//addons.prestashop.com/de/forgot-your-password\">Ich habe mein Passwort vergessen</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/de/login?email=ugur.%C3%B6zen%40pixabit.de&amp;firstname=Ugur&amp;lastname=%C3%96zen&amp;website=http%3A%2F%2Fwarenkorb.pixabit.de%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tErstellen Sie ein Konto
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Anmelden
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>
    <div class=\"content-div \">

      

      

      

      
      
      
      

      <div class=\"row \">
        <div class=\"col-xs-12\">
          <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>




  {% block content_header %}{% endblock %}
                 {% block content %}{% endblock %}
                 {% block content_footer %}{% endblock %}
                 {% block sidebar_right %}{% endblock %}

        </div>
      </div>

    </div>

  
</div>


  <div id=\"footer\" class=\"bootstrap hide\">
<!--
  <div class=\"col-sm-2 hidden-xs\">
    <a href=\"http://www.prestashop.com/\" class=\"_blank\">PrestaShop&trade;</a>
    -
    <span id=\"footer-load-time\"><i class=\"icon-time\" title=\"Ladezeit \"></i> 2.638s</span>
  </div>

  <div class=\"col-sm-2 hidden-xs\">
    <div class=\"social-networks\">
      <a class=\"link-social link-twitter _blank\" href=\"https://twitter.com/PrestaShop\" title=\"Twitter\">
        <i class=\"icon-twitter\"></i>
      </a>
      <a class=\"link-social link-facebook _blank\" href=\"https://www.facebook.com/prestashop\" title=\"Facebook\">
        <i class=\"icon-facebook\"></i>
      </a>
      <a class=\"link-social link-github _blank\" href=\"https://www.prestashop.com/github\" title=\"Github\">
        <i class=\"icon-github\"></i>
      </a>
      <a class=\"link-social link-google _blank\" href=\"https://plus.google.com/+prestashop/\" title=\"Google\">
        <i class=\"icon-google-plus\"></i>
      </a>
    </div>
  </div>
  <div class=\"col-sm-5\">
    <div class=\"footer-contact\">
      <a href=\"http://www.prestashop.com/en/contact_us?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-envelope\"></i>
        Kontakt
      </a>
      /&nbsp;
      <a href=\"http://forge.prestashop.com/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-bug\"></i>
        Bug-Tracker
      </a>
      /&nbsp;
      <a href=\"http://www.prestashop.com/forums/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-comments\"></i>
        Forum
      </a>
      /&nbsp;
      <a href=\"http://addons.prestashop.com/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-puzzle-piece\"></i>
        Addons
      </a>
      /&nbsp;
      <a href=\"http://www.prestashop.com/en/training-prestashop?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-book\"></i>
        Training
      </a>
                </div>
  </div>

  <div class=\"col-sm-3\">
    
  </div>

  <div id=\"go-top\" class=\"hide\"><i class=\"icon-arrow-up\"></i></div>
  -->
</div>



  <div class=\"bootstrap\">
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"http://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-DE&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t
<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/de/login?email=ugur.%C3%B6zen%40pixabit.de&amp;firstname=Ugur&amp;lastname=%C3%96zen&amp;website=http%3A%2F%2Fwarenkorb.pixabit.de%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/admin975acnmvl/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Verbinden Sie Ihren Shop mit Prestashops Marktplatz, um automatisch alle Kaufmodule zu importieren.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Sie haben noch keinen Account?</h4>
\t\t\t\t\t\t<p class='text-justify'>Entdecken Sie die Vielfalt der PrestaShop Addons! Stöbern Sie im offiziellen PrestaShop Martkplatz mit aktuell über 3 500 innovativen Templates und modularen Erweiterungen - ob es sich nun um Optimierung der Wechselkurse, Erhöhung der Zugriffsrate, Maßnahmen zur Kundenbindung oder Rentabilitätssteigerung handelt.</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Wechseln Sie zu PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link pull-right _blank\" href=\"//addons.prestashop.com/de/forgot-your-password\">Ich habe mein Passwort vergessen</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/de/login?email=ugur.%C3%B6zen%40pixabit.de&amp;firstname=Ugur&amp;lastname=%C3%96zen&amp;website=http%3A%2F%2Fwarenkorb.pixabit.de%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tErstellen Sie ein Konto
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Anmelden
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>
  </div>

{% block javascripts %}{% endblock %}{% block extra_javascripts %}{% endblock %}{% block translate_javascripts %}{% endblock %}</body>
</html>";
    }
}
