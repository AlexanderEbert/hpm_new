<?php

/* PrestaShopBundle:Admin/Module/Includes:menu_top.html.twig */
class __TwigTemplate_cb17f151274b3de4a6f76670065554cd2e254b0f9eba018f45b7cc99ff8ac0df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_41ea532f17ff4350aa09073711d2e1befc2e4da6aaf8b9cb415064a8ceb2fac1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_41ea532f17ff4350aa09073711d2e1befc2e4da6aaf8b9cb415064a8ceb2fac1->enter($__internal_41ea532f17ff4350aa09073711d2e1befc2e4da6aaf8b9cb415064a8ceb2fac1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PrestaShopBundle:Admin/Module/Includes:menu_top.html.twig"));

        // line 25
        echo "<div class=\"module-top-menu\">
    <div class=\"row\">
        <div class=\"col-md-8 module-menu-item\">
            ";
        // line 28
        if (array_key_exists("topMenuData", $context)) {
            // line 29
            echo "                ";
            $this->loadTemplate("PrestaShopBundle:Admin/Module/Includes:dropdown_categories.html.twig", "PrestaShopBundle:Admin/Module/Includes:menu_top.html.twig", 29)->display(array_merge($context, array("topMenuData" => (isset($context["topMenuData"]) ? $context["topMenuData"] : $this->getContext($context, "topMenuData")))));
            // line 30
            echo "            ";
        }
        // line 31
        echo "            ";
        if ((array_key_exists("requireFilterStatus", $context) && ((isset($context["requireFilterStatus"]) ? $context["requireFilterStatus"] : $this->getContext($context, "requireFilterStatus")) == true))) {
            // line 32
            echo "                ";
            $this->loadTemplate("PrestaShopBundle:Admin/Module/Includes:dropdown_status.html.twig", "PrestaShopBundle:Admin/Module/Includes:menu_top.html.twig", 32)->display($context);
            // line 33
            echo "            ";
        }
        // line 34
        echo "        </div>

        <div class=\"col-md-4\">
            <div class=\"input-group module-search-block\">
                <span class=\"input-group-addon module-search-icon\">
                    <i class=\"material-icons\">search</i>
                </span>
                <input id=\"module-search-bar\" class=\"module-search-bar form-control\" type=\"text\">
            </div>
        </div>

    </div>
</div>

<hr class=\"top-menu-separator\"/>

";
        // line 50
        $context["js_translatable"] = twig_array_merge(array("Search - placeholder" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Search modules: keyword, name, author...", array(), "Admin.Modules.Help")),         // line 52
(isset($context["js_translatable"]) ? $context["js_translatable"] : $this->getContext($context, "js_translatable")));
        
        $__internal_41ea532f17ff4350aa09073711d2e1befc2e4da6aaf8b9cb415064a8ceb2fac1->leave($__internal_41ea532f17ff4350aa09073711d2e1befc2e4da6aaf8b9cb415064a8ceb2fac1_prof);

    }

    public function getTemplateName()
    {
        return "PrestaShopBundle:Admin/Module/Includes:menu_top.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 52,  62 => 50,  44 => 34,  41 => 33,  38 => 32,  35 => 31,  32 => 30,  29 => 29,  27 => 28,  22 => 25,);
    }

    public function getSource()
    {
        return "{#**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *#}
<div class=\"module-top-menu\">
    <div class=\"row\">
        <div class=\"col-md-8 module-menu-item\">
            {% if topMenuData is defined %}
                {% include 'PrestaShopBundle:Admin/Module/Includes:dropdown_categories.html.twig' with { 'topMenuData': topMenuData } %}
            {% endif %}
            {% if requireFilterStatus is defined and requireFilterStatus == true %}
                {% include 'PrestaShopBundle:Admin/Module/Includes:dropdown_status.html.twig' %}
            {% endif %}
        </div>

        <div class=\"col-md-4\">
            <div class=\"input-group module-search-block\">
                <span class=\"input-group-addon module-search-icon\">
                    <i class=\"material-icons\">search</i>
                </span>
                <input id=\"module-search-bar\" class=\"module-search-bar form-control\" type=\"text\">
            </div>
        </div>

    </div>
</div>

<hr class=\"top-menu-separator\"/>

{% set js_translatable = {
\"Search - placeholder\": \"Search modules: keyword, name, author...\"|trans({}, 'Admin.Modules.Help'),
}|merge(js_translatable) %}";
    }
}
