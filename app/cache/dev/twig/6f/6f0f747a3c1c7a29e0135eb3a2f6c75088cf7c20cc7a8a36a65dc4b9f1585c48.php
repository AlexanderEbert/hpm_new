<?php

/* PrestaShopBundle:Admin/Module/Includes:modal_import.html.twig */
class __TwigTemplate_55beb52cc10a6036df59858c3e19c1abd890ad945e46180efb65286f920da7cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b1ca3b9c5624d0d9f183947a64f5934da199932b6e016a9dfdc81aaf78154857 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b1ca3b9c5624d0d9f183947a64f5934da199932b6e016a9dfdc81aaf78154857->enter($__internal_b1ca3b9c5624d0d9f183947a64f5934da199932b6e016a9dfdc81aaf78154857_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PrestaShopBundle:Admin/Module/Includes:modal_import.html.twig"));

        // line 25
        echo "<div id=\"module-modal-import\" class=\"modal modal-vcenter fade\" role=\"dialog\" data-backdrop=\"static\" data-keyboard=\"false\">
    <div class=\"modal-dialog\">
        <!-- Modal content-->
        <div class=\"modal-content\">
            <div class=\"modal-header\">
              <button id=\"module-modal-import-closing-cross\" type=\"button\" class=\"close\">&times;</button>
              <h4 class=\"modal-title module-modal-title\">";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Upload a module", array(), "Admin.Modules.Feature"), "html", null, true);
        echo "</h4>
            </div>
            <div class=\"modal-body\">
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <form action=\"#\" class=\"dropzone\" id=\"importDropzone\">
                            <div class=\"module-import-start\">
                                <i class=\"module-import-start-icon material-icons\">cloud_upload</i><br/>
                                <p class=module-import-start-main-text>
                                    ";
        // line 40
        echo twig_replace_filter($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Drop your module archive here or [1]select file[/1]", array(), "Admin.Modules.Feature"), array("[1]" => "<a href=\"#\" class=\"module-import-start-select-manual\">", "[/1]" => "</a>"));
        echo "
                                </p>
                                <p class=module-import-start-footer-text>
                                    ";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Please upload one file at a time, .zip or tarball format (.tar, .tar.gz or .tgz).", array(), "Admin.Modules.Help"), "html", null, true);
        echo "
                                    ";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Your module will be installed right after that.", array(), "Admin.Modules.Help"), "html", null, true);
        echo "
                                </p>
                            </div>
                            <div class='module-import-processing'>
                                <!-- Loader -->
                                <button class=\"btn btn-primary-reverse btn-lg onclick\" ></button>
                                <p class=module-import-processing-main-text>";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Installing module...", array(), "Admin.Modules.Notification"), "html", null, true);
        echo "</p>
                                <p class=module-import-processing-footer-text>
                                    ";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("It will close as soon as the module is installed. It won't be long!", array(), "Admin.Modules.Notification"), "html", null, true);
        echo "
                                </p>
                            </div>
                            <div class='module-import-success'>
                                <i class=\"module-import-success-icon material-icons\">done</i><br/>
                                <p class='module-import-success-msg'>";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Module installed!", array(), "Admin.Modules.Notification"), "html", null, true);
        echo "</p>
                                <p class=\"module-import-success-details\"></p>
                                <a class=\"module-import-success-configure btn btn-primary btn-sm light-button\" href='#'>";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Configure", array(), "Admin.Actions"), "html", null, true);
        echo "</a>
                            </div>
                            <div class='module-import-failure'>
                                <i class=\"module-import-failure-icon material-icons\">error</i><br/>
                                <p class='module-import-failure-msg'>";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Oops... Upload failed.", array(), "Admin.Modules.Notification"), "html", null, true);
        echo "</p>
                                <a href=\"#\" class=\"module-import-failure-details-action\">";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("What happened?", array(), "Admin.Modules.Help"), "html", null, true);
        echo "</a>
                                <div class='module-import-failure-details'></div>
                                <a class=\"module-import-failure-retry btn btn-tertiary\" href='#'>";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Try again", array(), "Admin.Actions"), "html", null, true);
        echo "</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class=\"modal-footer\">
            </div>
        </div>
    </div>
</div>
";
        
        $__internal_b1ca3b9c5624d0d9f183947a64f5934da199932b6e016a9dfdc81aaf78154857->leave($__internal_b1ca3b9c5624d0d9f183947a64f5934da199932b6e016a9dfdc81aaf78154857_prof);

    }

    public function getTemplateName()
    {
        return "PrestaShopBundle:Admin/Module/Includes:modal_import.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 66,  90 => 64,  86 => 63,  79 => 59,  74 => 57,  66 => 52,  61 => 50,  52 => 44,  48 => 43,  42 => 40,  30 => 31,  22 => 25,);
    }

    public function getSource()
    {
        return "{#**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *#}
<div id=\"module-modal-import\" class=\"modal modal-vcenter fade\" role=\"dialog\" data-backdrop=\"static\" data-keyboard=\"false\">
    <div class=\"modal-dialog\">
        <!-- Modal content-->
        <div class=\"modal-content\">
            <div class=\"modal-header\">
              <button id=\"module-modal-import-closing-cross\" type=\"button\" class=\"close\">&times;</button>
              <h4 class=\"modal-title module-modal-title\">{{ 'Upload a module'|trans({}, 'Admin.Modules.Feature') }}</h4>
            </div>
            <div class=\"modal-body\">
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <form action=\"#\" class=\"dropzone\" id=\"importDropzone\">
                            <div class=\"module-import-start\">
                                <i class=\"module-import-start-icon material-icons\">cloud_upload</i><br/>
                                <p class=module-import-start-main-text>
                                    {{ 'Drop your module archive here or [1]select file[/1]'|trans({}, 'Admin.Modules.Feature')|replace({'[1]' : '<a href=\"#\" class=\"module-import-start-select-manual\">', '[/1]' : '</a>'})|raw }}
                                </p>
                                <p class=module-import-start-footer-text>
                                    {{ 'Please upload one file at a time, .zip or tarball format (.tar, .tar.gz or .tgz).'|trans({}, 'Admin.Modules.Help') }}
                                    {{ 'Your module will be installed right after that.'|trans({}, 'Admin.Modules.Help') }}
                                </p>
                            </div>
                            <div class='module-import-processing'>
                                <!-- Loader -->
                                <button class=\"btn btn-primary-reverse btn-lg onclick\" ></button>
                                <p class=module-import-processing-main-text>{{ 'Installing module...'|trans({}, 'Admin.Modules.Notification') }}</p>
                                <p class=module-import-processing-footer-text>
                                    {{ \"It will close as soon as the module is installed. It won't be long!\"|trans({}, 'Admin.Modules.Notification') }}
                                </p>
                            </div>
                            <div class='module-import-success'>
                                <i class=\"module-import-success-icon material-icons\">done</i><br/>
                                <p class='module-import-success-msg'>{{ 'Module installed!'|trans({}, 'Admin.Modules.Notification') }}</p>
                                <p class=\"module-import-success-details\"></p>
                                <a class=\"module-import-success-configure btn btn-primary btn-sm light-button\" href='#'>{{ 'Configure'|trans({}, 'Admin.Actions') }}</a>
                            </div>
                            <div class='module-import-failure'>
                                <i class=\"module-import-failure-icon material-icons\">error</i><br/>
                                <p class='module-import-failure-msg'>{{ 'Oops... Upload failed.'|trans({}, 'Admin.Modules.Notification') }}</p>
                                <a href=\"#\" class=\"module-import-failure-details-action\">{{ 'What happened?'|trans({}, 'Admin.Modules.Help') }}</a>
                                <div class='module-import-failure-details'></div>
                                <a class=\"module-import-failure-retry btn btn-tertiary\" href='#'>{{ 'Try again'|trans({}, 'Admin.Actions') }}</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class=\"modal-footer\">
            </div>
        </div>
    </div>
</div>
";
    }
}
