<?php

/* ::base.html.twig */
class __TwigTemplate_c9b3077717b3c860c435b3561431e7e41905e35e2f85c74f556972b78484e749 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_57ab6bb61dac6b3e53333b5c6eac7ec3d2ad1a896b4ee54c58bdec5fd022d6e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_57ab6bb61dac6b3e53333b5c6eac7ec3d2ad1a896b4ee54c58bdec5fd022d6e6->enter($__internal_57ab6bb61dac6b3e53333b5c6eac7ec3d2ad1a896b4ee54c58bdec5fd022d6e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 25
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 29
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 30
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 31
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 34
        $this->displayBlock('body', $context, $blocks);
        // line 35
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 36
        echo "    </body>
</html>
";
        
        $__internal_57ab6bb61dac6b3e53333b5c6eac7ec3d2ad1a896b4ee54c58bdec5fd022d6e6->leave($__internal_57ab6bb61dac6b3e53333b5c6eac7ec3d2ad1a896b4ee54c58bdec5fd022d6e6_prof);

    }

    // line 29
    public function block_title($context, array $blocks = array())
    {
        $__internal_2bc718139a1ce4090f210fba1fe71543bea3f5745497f09b74c6124ada15aeda = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2bc718139a1ce4090f210fba1fe71543bea3f5745497f09b74c6124ada15aeda->enter($__internal_2bc718139a1ce4090f210fba1fe71543bea3f5745497f09b74c6124ada15aeda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_2bc718139a1ce4090f210fba1fe71543bea3f5745497f09b74c6124ada15aeda->leave($__internal_2bc718139a1ce4090f210fba1fe71543bea3f5745497f09b74c6124ada15aeda_prof);

    }

    // line 30
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_885f4683a239510aa54d2c13bcf36111cc4b3428fb89e279e7a11827f8b5b80c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_885f4683a239510aa54d2c13bcf36111cc4b3428fb89e279e7a11827f8b5b80c->enter($__internal_885f4683a239510aa54d2c13bcf36111cc4b3428fb89e279e7a11827f8b5b80c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_885f4683a239510aa54d2c13bcf36111cc4b3428fb89e279e7a11827f8b5b80c->leave($__internal_885f4683a239510aa54d2c13bcf36111cc4b3428fb89e279e7a11827f8b5b80c_prof);

    }

    // line 34
    public function block_body($context, array $blocks = array())
    {
        $__internal_5d6092b12d6080311c37f4aae13a7a949355067e41f0b969e5b4452cb7bf2a6e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5d6092b12d6080311c37f4aae13a7a949355067e41f0b969e5b4452cb7bf2a6e->enter($__internal_5d6092b12d6080311c37f4aae13a7a949355067e41f0b969e5b4452cb7bf2a6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_5d6092b12d6080311c37f4aae13a7a949355067e41f0b969e5b4452cb7bf2a6e->leave($__internal_5d6092b12d6080311c37f4aae13a7a949355067e41f0b969e5b4452cb7bf2a6e_prof);

    }

    // line 35
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_ad8356dd5bcddb8d516f8861575ff41ea1ba8bf4b507650c4864cb83165e5b41 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ad8356dd5bcddb8d516f8861575ff41ea1ba8bf4b507650c4864cb83165e5b41->enter($__internal_ad8356dd5bcddb8d516f8861575ff41ea1ba8bf4b507650c4864cb83165e5b41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_ad8356dd5bcddb8d516f8861575ff41ea1ba8bf4b507650c4864cb83165e5b41->leave($__internal_ad8356dd5bcddb8d516f8861575ff41ea1ba8bf4b507650c4864cb83165e5b41_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 35,  82 => 34,  71 => 30,  59 => 29,  50 => 36,  47 => 35,  45 => 34,  38 => 31,  36 => 30,  32 => 29,  26 => 25,);
    }

    public function getSource()
    {
        return "{#**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *#}
<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
";
    }
}
