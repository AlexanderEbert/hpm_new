<?php

/* PrestaShopBundle:Admin:Translations/include/translations-form-start.html.twig */
class __TwigTemplate_12edcf46ea930399b0f911bcb3bee0303dc129137ba043a4dc6c77d83afbddbb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4a6987c0055ec29af63e4be8616f34f30f7fff1f0f9282064c1b3f58051b3239 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a6987c0055ec29af63e4be8616f34f30f7fff1f0f9282064c1b3f58051b3239->enter($__internal_4a6987c0055ec29af63e4be8616f34f30f7fff1f0f9282064c1b3f58051b3239_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PrestaShopBundle:Admin:Translations/include/translations-form-start.html.twig"));

        // line 25
        echo (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title"));
        echo "
<div";
        // line 26
        echo (isset($context["parent"]) ? $context["parent"] : $this->getContext($context, "parent"));
        echo ">
  <div class=\"translation-forms col-offset\"";
        // line 27
        echo (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"));
        // line 28
        echo (isset($context["domain"]) ? $context["domain"] : $this->getContext($context, "domain"));
        // line 29
        echo (isset($context["missing_translations"]) ? $context["missing_translations"] : $this->getContext($context, "missing_translations"));
        echo ">
";
        
        $__internal_4a6987c0055ec29af63e4be8616f34f30f7fff1f0f9282064c1b3f58051b3239->leave($__internal_4a6987c0055ec29af63e4be8616f34f30f7fff1f0f9282064c1b3f58051b3239_prof);

    }

    public function getTemplateName()
    {
        return "PrestaShopBundle:Admin:Translations/include/translations-form-start.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 29,  32 => 28,  30 => 27,  26 => 26,  22 => 25,);
    }

    public function getSource()
    {
        return "{#**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *#}
{{ title|raw }}
<div{{ parent|raw }}>
  <div class=\"translation-forms col-offset\"{{- id|raw -}}
       {{- domain|raw -}}
       {{- missing_translations|raw -}}>
";
    }
}
