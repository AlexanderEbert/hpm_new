<?php

/* PrestaShopBundle:Admin:Translations/include/form-edit-message.html.twig */
class __TwigTemplate_557838ee48ffaa79f177dff13a29f9bf871ffb8ccef0501174400e03d541703b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_afc082d7381439bc6b65dd31094459df5612ab8633541a2c2b852201e3ed845a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_afc082d7381439bc6b65dd31094459df5612ab8633541a2c2b852201e3ed845a->enter($__internal_afc082d7381439bc6b65dd31094459df5612ab8633541a2c2b852201e3ed845a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PrestaShopBundle:Admin:Translations/include/form-edit-message.html.twig"));

        // line 25
        echo "<form method=\"post\" class=\"hide\" data-hash=\"";
        echo twig_escape_filter($this->env, (isset($context["hash"]) ? $context["hash"] : $this->getContext($context, "hash")), "html", null, true);
        echo "\" action=\"";
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : $this->getContext($context, "action")), "html", null, true);
        echo "\">
  <div class=\"alerts\">
    <div class=\"hide alert alert-info\">";
        // line 27
        echo twig_escape_filter($this->env, (isset($context["notification_success"]) ? $context["notification_success"] : $this->getContext($context, "notification_success")), "html", null, true);
        echo "</div>
    <div class=\"hide alert alert-danger\">";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["notification_error"]) ? $context["notification_error"] : $this->getContext($context, "notification_error")), "html", null, true);
        echo "</div>
  </div>
  <p><verbatim>";
        // line 30
        echo twig_escape_filter($this->env, (isset($context["translation_key"]) ? $context["translation_key"] : $this->getContext($context, "translation_key")), "html", null, true);
        echo "</verbatim></p>
  <div class=\"form-group row";
        // line 31
        if ( !(isset($context["is_translated"]) ? $context["is_translated"] : $this->getContext($context, "is_translated"))) {
            echo " has-warning";
        }
        echo "\">
    <div class=\"col-lg-12\">
      <textarea class=\"form-control";
        // line 33
        if ( !(isset($context["is_translated"]) ? $context["is_translated"] : $this->getContext($context, "is_translated"))) {
            echo " form-control-warning";
        }
        echo "\"
                rows=\"3\"
                name=\"translation_value\">";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["edited_translation_value"]) ? $context["edited_translation_value"] : $this->getContext($context, "edited_translation_value")), "html", null, true);
        // line 37
        echo "</textarea>
    </div>
    <input type=\"hidden\" name=\"domain\" value=\"";
        // line 39
        echo twig_escape_filter($this->env, (isset($context["domain"]) ? $context["domain"] : $this->getContext($context, "domain")), "html", null, true);
        echo "\"/>
    <input type=\"hidden\" name=\"locale\" value=\"";
        // line 40
        echo twig_escape_filter($this->env, (isset($context["locale"]) ? $context["locale"] : $this->getContext($context, "locale")), "html", null, true);
        echo "\"/>
    <input type=\"hidden\" name=\"default\" value=\"";
        // line 41
        echo twig_escape_filter($this->env, (isset($context["default_translation_value"]) ? $context["default_translation_value"] : $this->getContext($context, "default_translation_value")), "html", null, true);
        echo "\"/>
    <input type=\"hidden\" name=\"theme\" value=\"";
        // line 42
        echo twig_escape_filter($this->env, (isset($context["theme"]) ? $context["theme"] : $this->getContext($context, "theme")), "html", null, true);
        echo "\"/>
    <input type=\"hidden\" name=\"translation_key\" value=\"";
        // line 43
        echo twig_escape_filter($this->env, (isset($context["translation_key"]) ? $context["translation_key"] : $this->getContext($context, "translation_key")), "html", null, true);
        echo "\"/>
  </div>
  <div class=\"col-md-offset-1 buttons\">
    <input class=\"btn btn-primary btn-sm pull-right\" type=\"submit\" value=\"";
        // line 46
        echo twig_escape_filter($this->env, (isset($context["label_edit"]) ? $context["label_edit"] : $this->getContext($context, "label_edit")), "html", null, true);
        echo "\"/>
    <input class=\"btn btn-tertiary-outline btn-sm pull-right reset-translation-value\" type=\"button\" value=\"";
        // line 47
        echo twig_escape_filter($this->env, (isset($context["label_reset"]) ? $context["label_reset"] : $this->getContext($context, "label_reset")), "html", null, true);
        echo "\"/>
  </div>
</form>
";
        
        $__internal_afc082d7381439bc6b65dd31094459df5612ab8633541a2c2b852201e3ed845a->leave($__internal_afc082d7381439bc6b65dd31094459df5612ab8633541a2c2b852201e3ed845a_prof);

    }

    public function getTemplateName()
    {
        return "PrestaShopBundle:Admin:Translations/include/form-edit-message.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 47,  85 => 46,  79 => 43,  75 => 42,  71 => 41,  67 => 40,  63 => 39,  59 => 37,  57 => 36,  50 => 33,  43 => 31,  39 => 30,  34 => 28,  30 => 27,  22 => 25,);
    }

    public function getSource()
    {
        return "{#**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *#}
<form method=\"post\" class=\"hide\" data-hash=\"{{ hash }}\" action=\"{{ action }}\">
  <div class=\"alerts\">
    <div class=\"hide alert alert-info\">{{ notification_success }}</div>
    <div class=\"hide alert alert-danger\">{{ notification_error }}</div>
  </div>
  <p><verbatim>{{ translation_key }}</verbatim></p>
  <div class=\"form-group row{% if not is_translated %} has-warning{% endif %}\">
    <div class=\"col-lg-12\">
      <textarea class=\"form-control{% if not is_translated %} form-control-warning{% endif %}\"
                rows=\"3\"
                name=\"translation_value\">
          {{- edited_translation_value -}}
      </textarea>
    </div>
    <input type=\"hidden\" name=\"domain\" value=\"{{ domain }}\"/>
    <input type=\"hidden\" name=\"locale\" value=\"{{ locale }}\"/>
    <input type=\"hidden\" name=\"default\" value=\"{{ default_translation_value }}\"/>
    <input type=\"hidden\" name=\"theme\" value=\"{{ theme }}\"/>
    <input type=\"hidden\" name=\"translation_key\" value=\"{{ translation_key }}\"/>
  </div>
  <div class=\"col-md-offset-1 buttons\">
    <input class=\"btn btn-primary btn-sm pull-right\" type=\"submit\" value=\"{{ label_edit }}\"/>
    <input class=\"btn btn-tertiary-outline btn-sm pull-right reset-translation-value\" type=\"button\" value=\"{{ label_reset }}\"/>
  </div>
</form>
";
    }
}
