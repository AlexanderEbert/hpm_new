<?php

/* PrestaShopBundle:Admin\Translations:list.html.twig */
class __TwigTemplate_d39b742bf5940f1103badfe63a6032c941ea6b2a3731bd15742cdb9577f83ee9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 25
        $this->parent = $this->loadTemplate("PrestaShopBundle:Admin:layout.html.twig", "PrestaShopBundle:Admin\\Translations:list.html.twig", 25);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PrestaShopBundle:Admin:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b40d1551fd4b50a762ad18f9ba40564328e03f4aff5a3f9927937c880e2a66fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b40d1551fd4b50a762ad18f9ba40564328e03f4aff5a3f9927937c880e2a66fe->enter($__internal_b40d1551fd4b50a762ad18f9ba40564328e03f4aff5a3f9927937c880e2a66fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PrestaShopBundle:Admin\\Translations:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b40d1551fd4b50a762ad18f9ba40564328e03f4aff5a3f9927937c880e2a66fe->leave($__internal_b40d1551fd4b50a762ad18f9ba40564328e03f4aff5a3f9927937c880e2a66fe_prof);

    }

    // line 27
    public function block_content($context, array $blocks = array())
    {
        $__internal_0caf24bb2629c1872b6d6502caacd3b4fa95f5376029f6a2102c34003c029f49 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0caf24bb2629c1872b6d6502caacd3b4fa95f5376029f6a2102c34003c029f49->enter($__internal_0caf24bb2629c1872b6d6502caacd3b4fa95f5376029f6a2102c34003c029f49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 28
        echo "<div class=\"search-translation\">
  <h2>";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Search translations", array(), "Admin.International.Feature"), "html", null, true);
        echo "</h2>
  <div class=\"summary pull-right\">
    <span data-template=\"";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["total_translations"]) ? $context["total_translations"] : $this->getContext($context, "total_translations")), "html", null, true);
        echo "\" class=\"total-translations\"></span>
    <span class=\"hide separator\"> - </span>
    <span data-template=\"";
        // line 33
        echo twig_escape_filter($this->env, (isset($context["total_remaining_translations"]) ? $context["total_remaining_translations"] : $this->getContext($context, "total_remaining_translations")), "html", null, true);
        echo "\" class=\"total-remaining-translations\"></span>
  </div>
  <form>
    <label>";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("(2 characters at least)", array(), "Admin.International.Feature"), "html", null, true);
        echo "</label>
    <div class=\"form-inline\">
      <input class=\"form-control search\"
             pattern=\".{2,}\"
             placeholder=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Search a word or expression, e.g.: \"Order confirmation\"", array(), "Admin.International.Help"), "html", null, true);
        echo "\"
             type=\"search\"
             id=\"jetsSearch\"
      />
      <input class=\"btn btn-primary search-button\" type=\"submit\" value=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Search", array(), "Admin.Actions"), "html", null, true);
        echo "\" />
      <input class=\"btn btn-tertiary-outline\" type=\"reset\" value=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Reset", array(), "Admin.Actions"), "html", null, true);
        echo "\" />
    </div>
  </form>
  <div class=\"hide alert alert-warning\">
    <i class=\"material-icons\">info_outline</i>
    <span>";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("No results found", array(), "Admin.Notifications.Error"), "html", null, true);
        echo "</span>
  </div>
  ";
        // line 52
        $this->loadTemplate("PrestaShopBundle:Admin/Translations/include:translations-forms.html.twig", "PrestaShopBundle:Admin\\Translations:list.html.twig", 52)->display($context);
        // line 53
        echo "</div>

<form method=\"post\" id=\"messages-fragments\" action=\"";
        // line 55
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_international_translations_messages_fragments");
        echo "\">
  <input type=\"hidden\" name=\"lang\" value=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["requestParams"]) ? $context["requestParams"] : $this->getContext($context, "requestParams")), "lang", array()), "html", null, true);
        echo "\">
  <input type=\"hidden\" name=\"type\" value=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["requestParams"]) ? $context["requestParams"] : $this->getContext($context, "requestParams")), "type", array()), "html", null, true);
        echo "\">
  <input type=\"hidden\" name=\"theme\" value=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["requestParams"]) ? $context["requestParams"] : $this->getContext($context, "requestParams")), "theme", array()), "html", null, true);
        echo "\">
</form>

<div class=\"container-fluid\">
  <div class=\"row\">
    ";
        // line 63
        $this->loadTemplate("PrestaShopBundle:Admin/Translations/include:translations-tree.html.twig", "PrestaShopBundle:Admin\\Translations:list.html.twig", 63)->display($context);
        // line 64
        echo "    <div class=\"translation-domains col-xs-6\">
      <h1 id=\"domain\">
        <span class=\"name\"></span>
        <span class=\"separator hide\">-</span>
        <span class=\"missing-translations\"></span>
      </h1>
      <p class=\"missing-translations-paragraph\"></p>
      <div class=\"navbar-container pull-right\"></div>
      <div class=\"forms-container\">
      </div>
    </div>
  </div>
</div>
";
        
        $__internal_0caf24bb2629c1872b6d6502caacd3b4fa95f5376029f6a2102c34003c029f49->leave($__internal_0caf24bb2629c1872b6d6502caacd3b4fa95f5376029f6a2102c34003c029f49_prof);

    }

    public function getTemplateName()
    {
        return "PrestaShopBundle:Admin\\Translations:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 64,  116 => 63,  108 => 58,  104 => 57,  100 => 56,  96 => 55,  92 => 53,  90 => 52,  85 => 50,  77 => 45,  73 => 44,  66 => 40,  59 => 36,  53 => 33,  48 => 31,  43 => 29,  40 => 28,  34 => 27,  11 => 25,);
    }

    public function getSource()
    {
        return "{#**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *#}
{% extends 'PrestaShopBundle:Admin:layout.html.twig' %}

{% block content %}
<div class=\"search-translation\">
  <h2>{{ 'Search translations'|trans({}, 'Admin.International.Feature') }}</h2>
  <div class=\"summary pull-right\">
    <span data-template=\"{{ total_translations }}\" class=\"total-translations\"></span>
    <span class=\"hide separator\"> - </span>
    <span data-template=\"{{ total_remaining_translations }}\" class=\"total-remaining-translations\"></span>
  </div>
  <form>
    <label>{{ '(2 characters at least)'|trans({}, 'Admin.International.Feature') }}</label>
    <div class=\"form-inline\">
      <input class=\"form-control search\"
             pattern=\".{2,}\"
             placeholder=\"{{ 'Search a word or expression, e.g.: \\\"Order confirmation\\\"'|trans({}, 'Admin.International.Help') }}\"
             type=\"search\"
             id=\"jetsSearch\"
      />
      <input class=\"btn btn-primary search-button\" type=\"submit\" value=\"{{ 'Search'|trans({}, 'Admin.Actions') }}\" />
      <input class=\"btn btn-tertiary-outline\" type=\"reset\" value=\"{{ 'Reset'|trans({}, 'Admin.Actions') }}\" />
    </div>
  </form>
  <div class=\"hide alert alert-warning\">
    <i class=\"material-icons\">info_outline</i>
    <span>{{ 'No results found'|trans({}, 'Admin.Notifications.Error') }}</span>
  </div>
  {% include 'PrestaShopBundle:Admin/Translations/include:translations-forms.html.twig' %}
</div>

<form method=\"post\" id=\"messages-fragments\" action=\"{{ path('admin_international_translations_messages_fragments') }}\">
  <input type=\"hidden\" name=\"lang\" value=\"{{ requestParams.lang }}\">
  <input type=\"hidden\" name=\"type\" value=\"{{ requestParams.type }}\">
  <input type=\"hidden\" name=\"theme\" value=\"{{ requestParams.theme }}\">
</form>

<div class=\"container-fluid\">
  <div class=\"row\">
    {% include 'PrestaShopBundle:Admin/Translations/include:translations-tree.html.twig' %}
    <div class=\"translation-domains col-xs-6\">
      <h1 id=\"domain\">
        <span class=\"name\"></span>
        <span class=\"separator hide\">-</span>
        <span class=\"missing-translations\"></span>
      </h1>
      <p class=\"missing-translations-paragraph\"></p>
      <div class=\"navbar-container pull-right\"></div>
      <div class=\"forms-container\">
      </div>
    </div>
  </div>
</div>
{% endblock %}
";
    }
}
