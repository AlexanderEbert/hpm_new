<?php

/* PrestaShopBundle:Admin/TwigTemplateForm:form_div_layout.html.twig */
class __TwigTemplate_39ac17c995f0e4bedca0a422e78d5a545e38d2a4ca16a2d9d95faadd7b0a7534 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'form_widget' => array($this, 'block_form_widget'),
            'form_widget_simple' => array($this, 'block_form_widget_simple'),
            'form_widget_compound' => array($this, 'block_form_widget_compound'),
            'collection_widget' => array($this, 'block_collection_widget'),
            'textarea_widget' => array($this, 'block_textarea_widget'),
            'choice_widget' => array($this, 'block_choice_widget'),
            'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
            'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
            'choice_widget_options' => array($this, 'block_choice_widget_options'),
            'checkbox_widget' => array($this, 'block_checkbox_widget'),
            'radio_widget' => array($this, 'block_radio_widget'),
            'datetime_widget' => array($this, 'block_datetime_widget'),
            'date_widget' => array($this, 'block_date_widget'),
            'time_widget' => array($this, 'block_time_widget'),
            'number_widget' => array($this, 'block_number_widget'),
            'integer_widget' => array($this, 'block_integer_widget'),
            'money_widget' => array($this, 'block_money_widget'),
            'url_widget' => array($this, 'block_url_widget'),
            'search_widget' => array($this, 'block_search_widget'),
            'percent_widget' => array($this, 'block_percent_widget'),
            'password_widget' => array($this, 'block_password_widget'),
            'hidden_widget' => array($this, 'block_hidden_widget'),
            'email_widget' => array($this, 'block_email_widget'),
            'button_widget' => array($this, 'block_button_widget'),
            'submit_widget' => array($this, 'block_submit_widget'),
            'reset_widget' => array($this, 'block_reset_widget'),
            'form_label' => array($this, 'block_form_label'),
            'button_label' => array($this, 'block_button_label'),
            'repeated_row' => array($this, 'block_repeated_row'),
            'form_row' => array($this, 'block_form_row'),
            'button_row' => array($this, 'block_button_row'),
            'hidden_row' => array($this, 'block_hidden_row'),
            'form' => array($this, 'block_form'),
            'form_start' => array($this, 'block_form_start'),
            'form_end' => array($this, 'block_form_end'),
            'form_enctype' => array($this, 'block_form_enctype'),
            'form_errors' => array($this, 'block_form_errors'),
            'form_rest' => array($this, 'block_form_rest'),
            'form_rows' => array($this, 'block_form_rows'),
            'widget_attributes' => array($this, 'block_widget_attributes'),
            'widget_container_attributes' => array($this, 'block_widget_container_attributes'),
            'button_attributes' => array($this, 'block_button_attributes'),
            'attributes' => array($this, 'block_attributes'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c7847b07779c4c7c6b6701ba1752c72da868651f5fe4f8bb6ede97d18ba9fb32 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c7847b07779c4c7c6b6701ba1752c72da868651f5fe4f8bb6ede97d18ba9fb32->enter($__internal_c7847b07779c4c7c6b6701ba1752c72da868651f5fe4f8bb6ede97d18ba9fb32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PrestaShopBundle:Admin/TwigTemplateForm:form_div_layout.html.twig"));

        // line 27
        $this->displayBlock('form_widget', $context, $blocks);
        // line 35
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 40
        $this->displayBlock('form_widget_compound', $context, $blocks);
        // line 50
        $this->displayBlock('collection_widget', $context, $blocks);
        // line 57
        $this->displayBlock('textarea_widget', $context, $blocks);
        // line 61
        $this->displayBlock('choice_widget', $context, $blocks);
        // line 69
        $this->displayBlock('choice_widget_expanded', $context, $blocks);
        // line 78
        $this->displayBlock('choice_widget_collapsed', $context, $blocks);
        // line 98
        $this->displayBlock('choice_widget_options', $context, $blocks);
        // line 112
        $this->displayBlock('checkbox_widget', $context, $blocks);
        // line 117
        $this->displayBlock('radio_widget', $context, $blocks);
        // line 121
        $this->displayBlock('datetime_widget', $context, $blocks);
        // line 134
        $this->displayBlock('date_widget', $context, $blocks);
        // line 148
        $this->displayBlock('time_widget', $context, $blocks);
        // line 159
        $this->displayBlock('number_widget', $context, $blocks);
        // line 165
        $this->displayBlock('integer_widget', $context, $blocks);
        // line 170
        $this->displayBlock('money_widget', $context, $blocks);
        // line 174
        $this->displayBlock('url_widget', $context, $blocks);
        // line 179
        $this->displayBlock('search_widget', $context, $blocks);
        // line 184
        $this->displayBlock('percent_widget', $context, $blocks);
        // line 189
        $this->displayBlock('password_widget', $context, $blocks);
        // line 194
        $this->displayBlock('hidden_widget', $context, $blocks);
        // line 199
        $this->displayBlock('email_widget', $context, $blocks);
        // line 204
        $this->displayBlock('button_widget', $context, $blocks);
        // line 218
        $this->displayBlock('submit_widget', $context, $blocks);
        // line 223
        $this->displayBlock('reset_widget', $context, $blocks);
        // line 230
        $this->displayBlock('form_label', $context, $blocks);
        // line 256
        $this->displayBlock('button_label', $context, $blocks);
        // line 260
        $this->displayBlock('repeated_row', $context, $blocks);
        // line 268
        $this->displayBlock('form_row', $context, $blocks);
        // line 276
        $this->displayBlock('button_row', $context, $blocks);
        // line 282
        $this->displayBlock('hidden_row', $context, $blocks);
        // line 288
        $this->displayBlock('form', $context, $blocks);
        // line 294
        $this->displayBlock('form_start', $context, $blocks);
        // line 307
        $this->displayBlock('form_end', $context, $blocks);
        // line 314
        $this->displayBlock('form_enctype', $context, $blocks);
        // line 318
        $this->displayBlock('form_errors', $context, $blocks);
        // line 328
        $this->displayBlock('form_rest', $context, $blocks);
        // line 335
        echo "
";
        // line 338
        $this->displayBlock('form_rows', $context, $blocks);
        // line 344
        $this->displayBlock('widget_attributes', $context, $blocks);
        // line 361
        $this->displayBlock('widget_container_attributes', $context, $blocks);
        // line 375
        $this->displayBlock('button_attributes', $context, $blocks);
        // line 389
        $this->displayBlock('attributes', $context, $blocks);
        
        $__internal_c7847b07779c4c7c6b6701ba1752c72da868651f5fe4f8bb6ede97d18ba9fb32->leave($__internal_c7847b07779c4c7c6b6701ba1752c72da868651f5fe4f8bb6ede97d18ba9fb32_prof);

    }

    // line 27
    public function block_form_widget($context, array $blocks = array())
    {
        $__internal_4fcff867795268063a94452a385755fed47e8659a9ad6ad64821cc42ea98aadc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4fcff867795268063a94452a385755fed47e8659a9ad6ad64821cc42ea98aadc->enter($__internal_4fcff867795268063a94452a385755fed47e8659a9ad6ad64821cc42ea98aadc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget"));

        // line 28
        if ((isset($context["compound"]) ? $context["compound"] : $this->getContext($context, "compound"))) {
            // line 29
            $this->displayBlock("form_widget_compound", $context, $blocks);
        } else {
            // line 31
            $this->displayBlock("form_widget_simple", $context, $blocks);
        }
        
        $__internal_4fcff867795268063a94452a385755fed47e8659a9ad6ad64821cc42ea98aadc->leave($__internal_4fcff867795268063a94452a385755fed47e8659a9ad6ad64821cc42ea98aadc_prof);

    }

    // line 35
    public function block_form_widget_simple($context, array $blocks = array())
    {
        $__internal_5730aea992af643ab1c071a133198e7b10c80bf30e6e6837f90bbd7c21bce400 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5730aea992af643ab1c071a133198e7b10c80bf30e6e6837f90bbd7c21bce400->enter($__internal_5730aea992af643ab1c071a133198e7b10c80bf30e6e6837f90bbd7c21bce400_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        // line 36
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "text")) : ("text"));
        // line 37
        echo "<input type=\"";
        echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "html", null, true);
        echo "\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo " ";
        if ( !twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
            echo "value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "\" ";
        }
        echo "/>";
        
        $__internal_5730aea992af643ab1c071a133198e7b10c80bf30e6e6837f90bbd7c21bce400->leave($__internal_5730aea992af643ab1c071a133198e7b10c80bf30e6e6837f90bbd7c21bce400_prof);

    }

    // line 40
    public function block_form_widget_compound($context, array $blocks = array())
    {
        $__internal_de4dceaa181c62cd7758cc077a8931d5b3f3afeb6a7082058c7e1f87a128d6df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de4dceaa181c62cd7758cc077a8931d5b3f3afeb6a7082058c7e1f87a128d6df->enter($__internal_de4dceaa181c62cd7758cc077a8931d5b3f3afeb6a7082058c7e1f87a128d6df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_compound"));

        // line 41
        echo "<div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">";
        // line 42
        if (twig_test_empty($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array()))) {
            // line 43
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        }
        // line 45
        $this->displayBlock("form_rows", $context, $blocks);
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        // line 47
        echo "</div>";
        
        $__internal_de4dceaa181c62cd7758cc077a8931d5b3f3afeb6a7082058c7e1f87a128d6df->leave($__internal_de4dceaa181c62cd7758cc077a8931d5b3f3afeb6a7082058c7e1f87a128d6df_prof);

    }

    // line 50
    public function block_collection_widget($context, array $blocks = array())
    {
        $__internal_6e18341a1cf27612e7f7a228649bc3553e002a7da570ed49063e30abf1443879 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e18341a1cf27612e7f7a228649bc3553e002a7da570ed49063e30abf1443879->enter($__internal_6e18341a1cf27612e7f7a228649bc3553e002a7da570ed49063e30abf1443879_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "collection_widget"));

        // line 51
        if (array_key_exists("prototype", $context)) {
            // line 52
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("data-prototype" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["prototype"]) ? $context["prototype"] : $this->getContext($context, "prototype")), 'row')));
        }
        // line 54
        $this->displayBlock("form_widget", $context, $blocks);
        
        $__internal_6e18341a1cf27612e7f7a228649bc3553e002a7da570ed49063e30abf1443879->leave($__internal_6e18341a1cf27612e7f7a228649bc3553e002a7da570ed49063e30abf1443879_prof);

    }

    // line 57
    public function block_textarea_widget($context, array $blocks = array())
    {
        $__internal_7c3789e0bcaac230b86af5fa2e9283ad271367fbd8cef2fd73e07e9dd01d0758 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7c3789e0bcaac230b86af5fa2e9283ad271367fbd8cef2fd73e07e9dd01d0758->enter($__internal_7c3789e0bcaac230b86af5fa2e9283ad271367fbd8cef2fd73e07e9dd01d0758_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        // line 58
        echo "<textarea ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo ">";
        echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
        echo "</textarea>";
        
        $__internal_7c3789e0bcaac230b86af5fa2e9283ad271367fbd8cef2fd73e07e9dd01d0758->leave($__internal_7c3789e0bcaac230b86af5fa2e9283ad271367fbd8cef2fd73e07e9dd01d0758_prof);

    }

    // line 61
    public function block_choice_widget($context, array $blocks = array())
    {
        $__internal_c36af1cdab3771b24059c715b0cdee4b7d8f9ff7e6e9da56df790d8d0ff94e96 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c36af1cdab3771b24059c715b0cdee4b7d8f9ff7e6e9da56df790d8d0ff94e96->enter($__internal_c36af1cdab3771b24059c715b0cdee4b7d8f9ff7e6e9da56df790d8d0ff94e96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget"));

        // line 62
        if ((isset($context["expanded"]) ? $context["expanded"] : $this->getContext($context, "expanded"))) {
            // line 63
            $this->displayBlock("choice_widget_expanded", $context, $blocks);
        } else {
            // line 65
            $this->displayBlock("choice_widget_collapsed", $context, $blocks);
        }
        
        $__internal_c36af1cdab3771b24059c715b0cdee4b7d8f9ff7e6e9da56df790d8d0ff94e96->leave($__internal_c36af1cdab3771b24059c715b0cdee4b7d8f9ff7e6e9da56df790d8d0ff94e96_prof);

    }

    // line 69
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        $__internal_f4bea5aa756fa5a8da96e5d7c509d281bf62e04ce6a48bf778c49f3ec6040825 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f4bea5aa756fa5a8da96e5d7c509d281bf62e04ce6a48bf778c49f3ec6040825->enter($__internal_f4bea5aa756fa5a8da96e5d7c509d281bf62e04ce6a48bf778c49f3ec6040825_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        // line 70
        echo "<div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">";
        // line 71
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 72
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($context["child"], 'widget');
            // line 73
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($context["child"], 'label', array("translation_domain" => (isset($context["choice_translation_domain"]) ? $context["choice_translation_domain"] : $this->getContext($context, "choice_translation_domain"))));
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "</div>";
        
        $__internal_f4bea5aa756fa5a8da96e5d7c509d281bf62e04ce6a48bf778c49f3ec6040825->leave($__internal_f4bea5aa756fa5a8da96e5d7c509d281bf62e04ce6a48bf778c49f3ec6040825_prof);

    }

    // line 78
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        $__internal_ae6833701beeb130818d4f3a0b35fc273b3736113f25302200179afd262db6be = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae6833701beeb130818d4f3a0b35fc273b3736113f25302200179afd262db6be->enter($__internal_ae6833701beeb130818d4f3a0b35fc273b3736113f25302200179afd262db6be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        // line 79
        if (((((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required")) && (null === (isset($context["placeholder"]) ? $context["placeholder"] : $this->getContext($context, "placeholder")))) &&  !(isset($context["placeholder_in_choices"]) ? $context["placeholder_in_choices"] : $this->getContext($context, "placeholder_in_choices"))) &&  !(isset($context["multiple"]) ? $context["multiple"] : $this->getContext($context, "multiple")))) {
            // line 80
            $context["required"] = false;
        }
        // line 82
        echo "<select ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if ((isset($context["multiple"]) ? $context["multiple"] : $this->getContext($context, "multiple"))) {
            echo " multiple=\"multiple\"";
        }
        echo " data-toggle=\"select2\">";
        // line 83
        if ( !(null === (isset($context["placeholder"]) ? $context["placeholder"] : $this->getContext($context, "placeholder")))) {
            // line 84
            echo "<option value=\"\"";
            if (((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required")) && twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))))) {
                echo " selected=\"selected\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, ((((isset($context["placeholder"]) ? $context["placeholder"] : $this->getContext($context, "placeholder")) != "")) ? ((isset($context["placeholder"]) ? $context["placeholder"] : $this->getContext($context, "placeholder"))) : ("")), "html", null, true);
            echo "</option>";
        }
        // line 86
        if ((twig_length_filter($this->env, (isset($context["preferred_choices"]) ? $context["preferred_choices"] : $this->getContext($context, "preferred_choices"))) > 0)) {
            // line 87
            $context["options"] = (isset($context["preferred_choices"]) ? $context["preferred_choices"] : $this->getContext($context, "preferred_choices"));
            // line 88
            $this->displayBlock("choice_widget_options", $context, $blocks);
            // line 89
            if (((twig_length_filter($this->env, (isset($context["choices"]) ? $context["choices"] : $this->getContext($context, "choices"))) > 0) &&  !(null === (isset($context["separator"]) ? $context["separator"] : $this->getContext($context, "separator"))))) {
                // line 90
                echo "<option disabled=\"disabled\">";
                echo twig_escape_filter($this->env, (isset($context["separator"]) ? $context["separator"] : $this->getContext($context, "separator")), "html", null, true);
                echo "</option>";
            }
        }
        // line 93
        $context["options"] = (isset($context["choices"]) ? $context["choices"] : $this->getContext($context, "choices"));
        // line 94
        $this->displayBlock("choice_widget_options", $context, $blocks);
        // line 95
        echo "</select>";
        
        $__internal_ae6833701beeb130818d4f3a0b35fc273b3736113f25302200179afd262db6be->leave($__internal_ae6833701beeb130818d4f3a0b35fc273b3736113f25302200179afd262db6be_prof);

    }

    // line 98
    public function block_choice_widget_options($context, array $blocks = array())
    {
        $__internal_bef36dbb3181cf6d2c2b65b125d7f8760c84fcd9f646423b9689243a4ee4d683 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bef36dbb3181cf6d2c2b65b125d7f8760c84fcd9f646423b9689243a4ee4d683->enter($__internal_bef36dbb3181cf6d2c2b65b125d7f8760c84fcd9f646423b9689243a4ee4d683_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_options"));

        // line 99
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["group_label"] => $context["choice"]) {
            // line 100
            if (twig_test_iterable($context["choice"])) {
                // line 101
                echo "<optgroup label=\"";
                echo twig_escape_filter($this->env, ((((isset($context["choice_translation_domain"]) ? $context["choice_translation_domain"] : $this->getContext($context, "choice_translation_domain")) === false)) ? ($context["group_label"]) : ($context["group_label"])), "html", null, true);
                echo "\">
                ";
                // line 102
                $context["options"] = $context["choice"];
                // line 103
                $this->displayBlock("choice_widget_options", $context, $blocks);
                // line 104
                echo "</optgroup>";
            } else {
                // line 106
                $context["attr"] = $this->getAttribute($context["choice"], "attr", array());
                // line 107
                echo "            <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["choice"], "value", array()), "html", null, true);
                echo "\" ";
                $this->displayBlock("attributes", $context, $blocks);
                if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->isSelectedChoice($context["choice"], (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
                    echo " selected=\"selected\"";
                }
                echo ">";
                echo twig_escape_filter($this->env, ((((isset($context["choice_translation_domain"]) ? $context["choice_translation_domain"] : $this->getContext($context, "choice_translation_domain")) === false)) ? ($this->getAttribute($context["choice"], "label", array())) : ($this->getAttribute($context["choice"], "label", array()))), "html", null, true);
                echo "</option>";
            }
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['group_label'], $context['choice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_bef36dbb3181cf6d2c2b65b125d7f8760c84fcd9f646423b9689243a4ee4d683->leave($__internal_bef36dbb3181cf6d2c2b65b125d7f8760c84fcd9f646423b9689243a4ee4d683_prof);

    }

    // line 112
    public function block_checkbox_widget($context, array $blocks = array())
    {
        $__internal_8ad8dec397999641875a6e612fe1cd63a67069302bc0da9aacc44168e6403675 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8ad8dec397999641875a6e612fe1cd63a67069302bc0da9aacc44168e6403675->enter($__internal_8ad8dec397999641875a6e612fe1cd63a67069302bc0da9aacc44168e6403675_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        // line 113
        $context["switch"] = ((array_key_exists("switch", $context)) ? (_twig_default_filter((isset($context["switch"]) ? $context["switch"] : $this->getContext($context, "switch")), "")) : (""));
        // line 114
        echo "<input type=\"checkbox\" ";
        if ((isset($context["switch"]) ? $context["switch"] : $this->getContext($context, "switch"))) {
            echo "data-toggle=\"switch\"";
        }
        echo " ";
        if ((isset($context["switch"]) ? $context["switch"] : $this->getContext($context, "switch"))) {
            echo "class=\"";
            echo twig_escape_filter($this->env, (isset($context["switch"]) ? $context["switch"] : $this->getContext($context, "switch")), "html", null, true);
            echo "\"";
        }
        echo " ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (array_key_exists("value", $context)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "\"";
        }
        if ((isset($context["checked"]) ? $context["checked"] : $this->getContext($context, "checked"))) {
            echo " checked=\"checked\"";
        }
        echo " />
";
        
        $__internal_8ad8dec397999641875a6e612fe1cd63a67069302bc0da9aacc44168e6403675->leave($__internal_8ad8dec397999641875a6e612fe1cd63a67069302bc0da9aacc44168e6403675_prof);

    }

    // line 117
    public function block_radio_widget($context, array $blocks = array())
    {
        $__internal_aba4b813595cd37dc7bde7f233721bdb69e81131020b0054671d7e66cad4a7ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aba4b813595cd37dc7bde7f233721bdb69e81131020b0054671d7e66cad4a7ab->enter($__internal_aba4b813595cd37dc7bde7f233721bdb69e81131020b0054671d7e66cad4a7ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        // line 118
        echo "<input type=\"radio\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (array_key_exists("value", $context)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "\"";
        }
        if ((isset($context["checked"]) ? $context["checked"] : $this->getContext($context, "checked"))) {
            echo " checked=\"checked\"";
        }
        echo " />
";
        
        $__internal_aba4b813595cd37dc7bde7f233721bdb69e81131020b0054671d7e66cad4a7ab->leave($__internal_aba4b813595cd37dc7bde7f233721bdb69e81131020b0054671d7e66cad4a7ab_prof);

    }

    // line 121
    public function block_datetime_widget($context, array $blocks = array())
    {
        $__internal_5f71267f49ddd09769ec26fd6d10a7d3ebfe81009c6d8f2d8cdd69cebeb362de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5f71267f49ddd09769ec26fd6d10a7d3ebfe81009c6d8f2d8cdd69cebeb362de->enter($__internal_5f71267f49ddd09769ec26fd6d10a7d3ebfe81009c6d8f2d8cdd69cebeb362de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        // line 122
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 123
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 125
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 126
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date", array()), 'errors');
            // line 127
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "time", array()), 'errors');
            // line 128
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date", array()), 'widget');
            // line 129
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "time", array()), 'widget');
            // line 130
            echo "</div>";
        }
        
        $__internal_5f71267f49ddd09769ec26fd6d10a7d3ebfe81009c6d8f2d8cdd69cebeb362de->leave($__internal_5f71267f49ddd09769ec26fd6d10a7d3ebfe81009c6d8f2d8cdd69cebeb362de_prof);

    }

    // line 134
    public function block_date_widget($context, array $blocks = array())
    {
        $__internal_19a9d7b73eb95d5eda22369625820a760aa9d0c01147fe41194c67aa89aa2775 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_19a9d7b73eb95d5eda22369625820a760aa9d0c01147fe41194c67aa89aa2775->enter($__internal_19a9d7b73eb95d5eda22369625820a760aa9d0c01147fe41194c67aa89aa2775_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        // line 135
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 136
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 138
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 139
            echo twig_replace_filter((isset($context["date_pattern"]) ? $context["date_pattern"] : $this->getContext($context, "date_pattern")), array("{{ year }}" =>             // line 140
$this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "year", array()), 'widget'), "{{ month }}" =>             // line 141
$this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "month", array()), 'widget'), "{{ day }}" =>             // line 142
$this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "day", array()), 'widget')));
            // line 144
            echo "</div>";
        }
        
        $__internal_19a9d7b73eb95d5eda22369625820a760aa9d0c01147fe41194c67aa89aa2775->leave($__internal_19a9d7b73eb95d5eda22369625820a760aa9d0c01147fe41194c67aa89aa2775_prof);

    }

    // line 148
    public function block_time_widget($context, array $blocks = array())
    {
        $__internal_f46666db81283b63613e461826fee8e6e0bbea932627ca0152f118d4c2291f7f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f46666db81283b63613e461826fee8e6e0bbea932627ca0152f118d4c2291f7f->enter($__internal_f46666db81283b63613e461826fee8e6e0bbea932627ca0152f118d4c2291f7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        // line 149
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 150
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 152
            $context["vars"] = ((((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "text")) ? (array("attr" => array("size" => 1))) : (array()));
            // line 153
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 154
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "hour", array()), 'widget', (isset($context["vars"]) ? $context["vars"] : $this->getContext($context, "vars")));
            if ((isset($context["with_minutes"]) ? $context["with_minutes"] : $this->getContext($context, "with_minutes"))) {
                echo ":";
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "minute", array()), 'widget', (isset($context["vars"]) ? $context["vars"] : $this->getContext($context, "vars")));
            }
            if ((isset($context["with_seconds"]) ? $context["with_seconds"] : $this->getContext($context, "with_seconds"))) {
                echo ":";
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "second", array()), 'widget', (isset($context["vars"]) ? $context["vars"] : $this->getContext($context, "vars")));
            }
            // line 155
            echo "        </div>";
        }
        
        $__internal_f46666db81283b63613e461826fee8e6e0bbea932627ca0152f118d4c2291f7f->leave($__internal_f46666db81283b63613e461826fee8e6e0bbea932627ca0152f118d4c2291f7f_prof);

    }

    // line 159
    public function block_number_widget($context, array $blocks = array())
    {
        $__internal_1e93c3f016f9b2cba278ab5a0c6f131305e481ab9acee9b046b5fd5794e3e7f5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1e93c3f016f9b2cba278ab5a0c6f131305e481ab9acee9b046b5fd5794e3e7f5->enter($__internal_1e93c3f016f9b2cba278ab5a0c6f131305e481ab9acee9b046b5fd5794e3e7f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "number_widget"));

        // line 161
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "text")) : ("text"));
        // line 162
        $this->displayBlock("form_widget_simple", $context, $blocks);
        
        $__internal_1e93c3f016f9b2cba278ab5a0c6f131305e481ab9acee9b046b5fd5794e3e7f5->leave($__internal_1e93c3f016f9b2cba278ab5a0c6f131305e481ab9acee9b046b5fd5794e3e7f5_prof);

    }

    // line 165
    public function block_integer_widget($context, array $blocks = array())
    {
        $__internal_1f022cf5c3dadbbb47826c1117394cf61a19b88f643c5877228618b47cda4aae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f022cf5c3dadbbb47826c1117394cf61a19b88f643c5877228618b47cda4aae->enter($__internal_1f022cf5c3dadbbb47826c1117394cf61a19b88f643c5877228618b47cda4aae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "integer_widget"));

        // line 166
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "number")) : ("number"));
        // line 167
        $this->displayBlock("form_widget_simple", $context, $blocks);
        
        $__internal_1f022cf5c3dadbbb47826c1117394cf61a19b88f643c5877228618b47cda4aae->leave($__internal_1f022cf5c3dadbbb47826c1117394cf61a19b88f643c5877228618b47cda4aae_prof);

    }

    // line 170
    public function block_money_widget($context, array $blocks = array())
    {
        $__internal_f78b299b0586daeab2ca22bd9fbb92b17b6f496f47f5a125bda7caf3da553520 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f78b299b0586daeab2ca22bd9fbb92b17b6f496f47f5a125bda7caf3da553520->enter($__internal_f78b299b0586daeab2ca22bd9fbb92b17b6f496f47f5a125bda7caf3da553520_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        // line 171
        echo twig_replace_filter((isset($context["money_pattern"]) ? $context["money_pattern"] : $this->getContext($context, "money_pattern")), array("{{ widget }}" => $this->renderBlock("form_widget_simple", $context, $blocks)));
        
        $__internal_f78b299b0586daeab2ca22bd9fbb92b17b6f496f47f5a125bda7caf3da553520->leave($__internal_f78b299b0586daeab2ca22bd9fbb92b17b6f496f47f5a125bda7caf3da553520_prof);

    }

    // line 174
    public function block_url_widget($context, array $blocks = array())
    {
        $__internal_812248728539ddf0a131f684ab6ac010a721c8bfec8b43b7350b928b58770f37 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_812248728539ddf0a131f684ab6ac010a721c8bfec8b43b7350b928b58770f37->enter($__internal_812248728539ddf0a131f684ab6ac010a721c8bfec8b43b7350b928b58770f37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "url_widget"));

        // line 175
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "url")) : ("url"));
        // line 176
        $this->displayBlock("form_widget_simple", $context, $blocks);
        
        $__internal_812248728539ddf0a131f684ab6ac010a721c8bfec8b43b7350b928b58770f37->leave($__internal_812248728539ddf0a131f684ab6ac010a721c8bfec8b43b7350b928b58770f37_prof);

    }

    // line 179
    public function block_search_widget($context, array $blocks = array())
    {
        $__internal_4d6a3b13d11d7729ab5d0f4e534d5500c732c87ff60e0abf45bd92657ec4e5a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d6a3b13d11d7729ab5d0f4e534d5500c732c87ff60e0abf45bd92657ec4e5a7->enter($__internal_4d6a3b13d11d7729ab5d0f4e534d5500c732c87ff60e0abf45bd92657ec4e5a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "search_widget"));

        // line 180
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "search")) : ("search"));
        // line 181
        $this->displayBlock("form_widget_simple", $context, $blocks);
        
        $__internal_4d6a3b13d11d7729ab5d0f4e534d5500c732c87ff60e0abf45bd92657ec4e5a7->leave($__internal_4d6a3b13d11d7729ab5d0f4e534d5500c732c87ff60e0abf45bd92657ec4e5a7_prof);

    }

    // line 184
    public function block_percent_widget($context, array $blocks = array())
    {
        $__internal_c6751b3e0f9228905246b69d1ef7e00db3aa3020dde18b0725808ee28da270c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c6751b3e0f9228905246b69d1ef7e00db3aa3020dde18b0725808ee28da270c7->enter($__internal_c6751b3e0f9228905246b69d1ef7e00db3aa3020dde18b0725808ee28da270c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        // line 185
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "text")) : ("text"));
        // line 186
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo " %";
        
        $__internal_c6751b3e0f9228905246b69d1ef7e00db3aa3020dde18b0725808ee28da270c7->leave($__internal_c6751b3e0f9228905246b69d1ef7e00db3aa3020dde18b0725808ee28da270c7_prof);

    }

    // line 189
    public function block_password_widget($context, array $blocks = array())
    {
        $__internal_1a6eabe1018d8fd8353841dd75e42545235b0a87505ec1f762fe8c7c9319b3d1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a6eabe1018d8fd8353841dd75e42545235b0a87505ec1f762fe8c7c9319b3d1->enter($__internal_1a6eabe1018d8fd8353841dd75e42545235b0a87505ec1f762fe8c7c9319b3d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "password_widget"));

        // line 190
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "password")) : ("password"));
        // line 191
        $this->displayBlock("form_widget_simple", $context, $blocks);
        
        $__internal_1a6eabe1018d8fd8353841dd75e42545235b0a87505ec1f762fe8c7c9319b3d1->leave($__internal_1a6eabe1018d8fd8353841dd75e42545235b0a87505ec1f762fe8c7c9319b3d1_prof);

    }

    // line 194
    public function block_hidden_widget($context, array $blocks = array())
    {
        $__internal_5b82a097b43a938e9836f0d687550783ca3ddc433c8f00656e464a0deba305f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b82a097b43a938e9836f0d687550783ca3ddc433c8f00656e464a0deba305f7->enter($__internal_5b82a097b43a938e9836f0d687550783ca3ddc433c8f00656e464a0deba305f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hidden_widget"));

        // line 195
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "hidden")) : ("hidden"));
        // line 196
        $this->displayBlock("form_widget_simple", $context, $blocks);
        
        $__internal_5b82a097b43a938e9836f0d687550783ca3ddc433c8f00656e464a0deba305f7->leave($__internal_5b82a097b43a938e9836f0d687550783ca3ddc433c8f00656e464a0deba305f7_prof);

    }

    // line 199
    public function block_email_widget($context, array $blocks = array())
    {
        $__internal_50b3284b3ba2b0ca6b1f6fe938c46ecd2ea09223d63227dbffd6d1eac5004fed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_50b3284b3ba2b0ca6b1f6fe938c46ecd2ea09223d63227dbffd6d1eac5004fed->enter($__internal_50b3284b3ba2b0ca6b1f6fe938c46ecd2ea09223d63227dbffd6d1eac5004fed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "email_widget"));

        // line 200
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "email")) : ("email"));
        // line 201
        $this->displayBlock("form_widget_simple", $context, $blocks);
        
        $__internal_50b3284b3ba2b0ca6b1f6fe938c46ecd2ea09223d63227dbffd6d1eac5004fed->leave($__internal_50b3284b3ba2b0ca6b1f6fe938c46ecd2ea09223d63227dbffd6d1eac5004fed_prof);

    }

    // line 204
    public function block_button_widget($context, array $blocks = array())
    {
        $__internal_e157db6cacb4ab9add0a0b2f93acc77eff0b8c61873cc0ce66309b2a3a11a1b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e157db6cacb4ab9add0a0b2f93acc77eff0b8c61873cc0ce66309b2a3a11a1b8->enter($__internal_e157db6cacb4ab9add0a0b2f93acc77eff0b8c61873cc0ce66309b2a3a11a1b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        // line 205
        if (twig_test_empty((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")))) {
            // line 206
            if ( !twig_test_empty((isset($context["label_format"]) ? $context["label_format"] : $this->getContext($context, "label_format")))) {
                // line 207
                $context["label"] = twig_replace_filter((isset($context["label_format"]) ? $context["label_format"] : $this->getContext($context, "label_format")), array("%name%" =>                 // line 208
(isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "%id%" =>                 // line 209
(isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))));
            } else {
                // line 212
                $context["label"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->humanize((isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")));
            }
        }
        // line 215
        echo "<button type=\"";
        echo twig_escape_filter($this->env, ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "button")) : ("button")), "html", null, true);
        echo "\" ";
        $this->displayBlock("button_attributes", $context, $blocks);
        echo ">";
        echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), "html", null, true);
        echo "</button>";
        
        $__internal_e157db6cacb4ab9add0a0b2f93acc77eff0b8c61873cc0ce66309b2a3a11a1b8->leave($__internal_e157db6cacb4ab9add0a0b2f93acc77eff0b8c61873cc0ce66309b2a3a11a1b8_prof);

    }

    // line 218
    public function block_submit_widget($context, array $blocks = array())
    {
        $__internal_6cc3157c544e276d5cb5a92a4c5274d5b7188d1f55e980029172008d0d742101 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6cc3157c544e276d5cb5a92a4c5274d5b7188d1f55e980029172008d0d742101->enter($__internal_6cc3157c544e276d5cb5a92a4c5274d5b7188d1f55e980029172008d0d742101_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_widget"));

        // line 219
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "submit")) : ("submit"));
        // line 220
        $this->displayBlock("button_widget", $context, $blocks);
        
        $__internal_6cc3157c544e276d5cb5a92a4c5274d5b7188d1f55e980029172008d0d742101->leave($__internal_6cc3157c544e276d5cb5a92a4c5274d5b7188d1f55e980029172008d0d742101_prof);

    }

    // line 223
    public function block_reset_widget($context, array $blocks = array())
    {
        $__internal_4d91f1c649e9300a70ea29cb907be21f85109386ee506660375bfb747656c417 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d91f1c649e9300a70ea29cb907be21f85109386ee506660375bfb747656c417->enter($__internal_4d91f1c649e9300a70ea29cb907be21f85109386ee506660375bfb747656c417_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_widget"));

        // line 224
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "reset")) : ("reset"));
        // line 225
        $this->displayBlock("button_widget", $context, $blocks);
        
        $__internal_4d91f1c649e9300a70ea29cb907be21f85109386ee506660375bfb747656c417->leave($__internal_4d91f1c649e9300a70ea29cb907be21f85109386ee506660375bfb747656c417_prof);

    }

    // line 230
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_a20790b8a69d8b5c2d4ea7f2f2c44986e28fdd8171ca6038ce5ec7c98a8a0c35 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a20790b8a69d8b5c2d4ea7f2f2c44986e28fdd8171ca6038ce5ec7c98a8a0c35->enter($__internal_a20790b8a69d8b5c2d4ea7f2f2c44986e28fdd8171ca6038ce5ec7c98a8a0c35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 231
        if ( !((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false)) {
            // line 232
            if ( !(isset($context["compound"]) ? $context["compound"] : $this->getContext($context, "compound"))) {
                // line 233
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("for" => (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))));
            }
            // line 235
            echo "        ";
            if ((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required"))) {
                // line 236
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => trim(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " required"))));
            }
            // line 238
            echo "        ";
            if (twig_test_empty((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")))) {
                // line 239
                if ( !twig_test_empty((isset($context["label_format"]) ? $context["label_format"] : $this->getContext($context, "label_format")))) {
                    // line 240
                    $context["label"] = twig_replace_filter((isset($context["label_format"]) ? $context["label_format"] : $this->getContext($context, "label_format")), array("%name%" =>                     // line 241
(isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "%id%" =>                     // line 242
(isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))));
                } else {
                    // line 245
                    $context["label"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->humanize((isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")));
                }
            }
            // line 248
            echo "<label";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            echo ((((isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")) === false)) ? ((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"))) : ((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"))));
            echo "</label>
        ";
            // line 249
            if ($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "tooltip", array(), "array", true, true)) {
                // line 250
                echo "            ";
                $context["placement"] = (($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "tooltip_placement", array(), "array", true, true)) ? ($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), "tooltip_placement", array(), "array")) : ("top"));
                // line 251
                echo "            <i class=\"icon-question\" data-toggle=\"tooltip\" data-placement=\"";
                echo twig_escape_filter($this->env, (isset($context["placement"]) ? $context["placement"] : $this->getContext($context, "placement")), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), "tooltip", array(), "array"), "html", null, true);
                echo "\"></i>
        ";
            }
        }
        
        $__internal_a20790b8a69d8b5c2d4ea7f2f2c44986e28fdd8171ca6038ce5ec7c98a8a0c35->leave($__internal_a20790b8a69d8b5c2d4ea7f2f2c44986e28fdd8171ca6038ce5ec7c98a8a0c35_prof);

    }

    // line 256
    public function block_button_label($context, array $blocks = array())
    {
        $__internal_c8c71725cf056f928a245e626b453825cb2660bf6d851d4b1fa60fd366aec904 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8c71725cf056f928a245e626b453825cb2660bf6d851d4b1fa60fd366aec904->enter($__internal_c8c71725cf056f928a245e626b453825cb2660bf6d851d4b1fa60fd366aec904_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_label"));

        
        $__internal_c8c71725cf056f928a245e626b453825cb2660bf6d851d4b1fa60fd366aec904->leave($__internal_c8c71725cf056f928a245e626b453825cb2660bf6d851d4b1fa60fd366aec904_prof);

    }

    // line 260
    public function block_repeated_row($context, array $blocks = array())
    {
        $__internal_0cb17c21ca8454d92562009b6d2e6f2fa3ab584794d197ce14a87c9e745c1b44 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0cb17c21ca8454d92562009b6d2e6f2fa3ab584794d197ce14a87c9e745c1b44->enter($__internal_0cb17c21ca8454d92562009b6d2e6f2fa3ab584794d197ce14a87c9e745c1b44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "repeated_row"));

        // line 265
        $this->displayBlock("form_rows", $context, $blocks);
        
        $__internal_0cb17c21ca8454d92562009b6d2e6f2fa3ab584794d197ce14a87c9e745c1b44->leave($__internal_0cb17c21ca8454d92562009b6d2e6f2fa3ab584794d197ce14a87c9e745c1b44_prof);

    }

    // line 268
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_ccf32c1661bef981368c0f6de63ede48d248020acb9f78deea1c97cea2ba1328 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ccf32c1661bef981368c0f6de63ede48d248020acb9f78deea1c97cea2ba1328->enter($__internal_ccf32c1661bef981368c0f6de63ede48d248020acb9f78deea1c97cea2ba1328_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 269
        echo "<div>";
        // line 270
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label');
        // line 271
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        // line 272
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 273
        echo "</div>";
        
        $__internal_ccf32c1661bef981368c0f6de63ede48d248020acb9f78deea1c97cea2ba1328->leave($__internal_ccf32c1661bef981368c0f6de63ede48d248020acb9f78deea1c97cea2ba1328_prof);

    }

    // line 276
    public function block_button_row($context, array $blocks = array())
    {
        $__internal_cb1dad0fd5030d47ef33b2db300090b84286f3b7173514a1e691a0b997feed89 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb1dad0fd5030d47ef33b2db300090b84286f3b7173514a1e691a0b997feed89->enter($__internal_cb1dad0fd5030d47ef33b2db300090b84286f3b7173514a1e691a0b997feed89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        // line 277
        echo "<div>";
        // line 278
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 279
        echo "</div>";
        
        $__internal_cb1dad0fd5030d47ef33b2db300090b84286f3b7173514a1e691a0b997feed89->leave($__internal_cb1dad0fd5030d47ef33b2db300090b84286f3b7173514a1e691a0b997feed89_prof);

    }

    // line 282
    public function block_hidden_row($context, array $blocks = array())
    {
        $__internal_9f5a08f5ce45db7d99ab1a72b75acdc5ff17572e0d2a8b42b09b91a5c26e461d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f5a08f5ce45db7d99ab1a72b75acdc5ff17572e0d2a8b42b09b91a5c26e461d->enter($__internal_9f5a08f5ce45db7d99ab1a72b75acdc5ff17572e0d2a8b42b09b91a5c26e461d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hidden_row"));

        // line 283
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        
        $__internal_9f5a08f5ce45db7d99ab1a72b75acdc5ff17572e0d2a8b42b09b91a5c26e461d->leave($__internal_9f5a08f5ce45db7d99ab1a72b75acdc5ff17572e0d2a8b42b09b91a5c26e461d_prof);

    }

    // line 288
    public function block_form($context, array $blocks = array())
    {
        $__internal_d3520b409f455245158a9c3aec1d55f969e0590eca95a50b9aa95a50539de36f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d3520b409f455245158a9c3aec1d55f969e0590eca95a50b9aa95a50539de36f->enter($__internal_d3520b409f455245158a9c3aec1d55f969e0590eca95a50b9aa95a50539de36f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        // line 289
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        // line 290
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 291
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        
        $__internal_d3520b409f455245158a9c3aec1d55f969e0590eca95a50b9aa95a50539de36f->leave($__internal_d3520b409f455245158a9c3aec1d55f969e0590eca95a50b9aa95a50539de36f_prof);

    }

    // line 294
    public function block_form_start($context, array $blocks = array())
    {
        $__internal_accf9a898c6429bfc9b36b58f08523c7affda48883741f2cd7a937098e12e218 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_accf9a898c6429bfc9b36b58f08523c7affda48883741f2cd7a937098e12e218->enter($__internal_accf9a898c6429bfc9b36b58f08523c7affda48883741f2cd7a937098e12e218_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_start"));

        // line 295
        $context["method"] = twig_upper_filter($this->env, (isset($context["method"]) ? $context["method"] : $this->getContext($context, "method")));
        // line 296
        if (twig_in_filter((isset($context["method"]) ? $context["method"] : $this->getContext($context, "method")), array(0 => "GET", 1 => "POST"))) {
            // line 297
            $context["form_method"] = (isset($context["method"]) ? $context["method"] : $this->getContext($context, "method"));
        } else {
            // line 299
            $context["form_method"] = "POST";
        }
        // line 301
        echo "<form name=\"";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "html", null, true);
        echo "\" method=\"";
        echo twig_escape_filter($this->env, twig_lower_filter($this->env, (isset($context["form_method"]) ? $context["form_method"] : $this->getContext($context, "form_method"))), "html", null, true);
        echo "\" action=\"";
        echo twig_escape_filter($this->env, (isset($context["action"]) ? $context["action"] : $this->getContext($context, "action")), "html", null, true);
        echo "\"";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        if ((isset($context["multipart"]) ? $context["multipart"] : $this->getContext($context, "multipart"))) {
            echo " enctype=\"multipart/form-data\"";
        }
        echo ">";
        // line 302
        if (((isset($context["form_method"]) ? $context["form_method"] : $this->getContext($context, "form_method")) != (isset($context["method"]) ? $context["method"] : $this->getContext($context, "method")))) {
            // line 303
            echo "<input type=\"hidden\" name=\"_method\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["method"]) ? $context["method"] : $this->getContext($context, "method")), "html", null, true);
            echo "\" />";
        }
        
        $__internal_accf9a898c6429bfc9b36b58f08523c7affda48883741f2cd7a937098e12e218->leave($__internal_accf9a898c6429bfc9b36b58f08523c7affda48883741f2cd7a937098e12e218_prof);

    }

    // line 307
    public function block_form_end($context, array $blocks = array())
    {
        $__internal_c1878491c0fd5b6f62aa26dffcc0b71a7701f5163ba3a576491539d5c55e92cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1878491c0fd5b6f62aa26dffcc0b71a7701f5163ba3a576491539d5c55e92cd->enter($__internal_c1878491c0fd5b6f62aa26dffcc0b71a7701f5163ba3a576491539d5c55e92cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_end"));

        // line 308
        if (( !array_key_exists("render_rest", $context) || (isset($context["render_rest"]) ? $context["render_rest"] : $this->getContext($context, "render_rest")))) {
            // line 309
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'rest');
        }
        // line 311
        echo "</form>";
        
        $__internal_c1878491c0fd5b6f62aa26dffcc0b71a7701f5163ba3a576491539d5c55e92cd->leave($__internal_c1878491c0fd5b6f62aa26dffcc0b71a7701f5163ba3a576491539d5c55e92cd_prof);

    }

    // line 314
    public function block_form_enctype($context, array $blocks = array())
    {
        $__internal_fccc5d033094c59b6859ff13aaeccf340669b0956d54744dfcdc8c3be45ef308 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fccc5d033094c59b6859ff13aaeccf340669b0956d54744dfcdc8c3be45ef308->enter($__internal_fccc5d033094c59b6859ff13aaeccf340669b0956d54744dfcdc8c3be45ef308_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_enctype"));

        // line 315
        if ((isset($context["multipart"]) ? $context["multipart"] : $this->getContext($context, "multipart"))) {
            echo "enctype=\"multipart/form-data\"";
        }
        
        $__internal_fccc5d033094c59b6859ff13aaeccf340669b0956d54744dfcdc8c3be45ef308->leave($__internal_fccc5d033094c59b6859ff13aaeccf340669b0956d54744dfcdc8c3be45ef308_prof);

    }

    // line 318
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_3e99f069afecad9e060a1430ed261f7fd44172a51bdad8841e2783bf3c9ed4e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e99f069afecad9e060a1430ed261f7fd44172a51bdad8841e2783bf3c9ed4e1->enter($__internal_3e99f069afecad9e060a1430ed261f7fd44172a51bdad8841e2783bf3c9ed4e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 319
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 320
            echo "<ul>";
            // line 321
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 322
                echo "<li>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 324
            echo "</ul>";
        }
        
        $__internal_3e99f069afecad9e060a1430ed261f7fd44172a51bdad8841e2783bf3c9ed4e1->leave($__internal_3e99f069afecad9e060a1430ed261f7fd44172a51bdad8841e2783bf3c9ed4e1_prof);

    }

    // line 328
    public function block_form_rest($context, array $blocks = array())
    {
        $__internal_a3ff2b9355eaf5d3680d14d75136f493b2e73a464a754181fef6b678e5633721 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a3ff2b9355eaf5d3680d14d75136f493b2e73a464a754181fef6b678e5633721->enter($__internal_a3ff2b9355eaf5d3680d14d75136f493b2e73a464a754181fef6b678e5633721_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_rest"));

        // line 329
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 330
            if ( !$this->getAttribute($context["child"], "rendered", array())) {
                // line 331
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($context["child"], 'row');
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_a3ff2b9355eaf5d3680d14d75136f493b2e73a464a754181fef6b678e5633721->leave($__internal_a3ff2b9355eaf5d3680d14d75136f493b2e73a464a754181fef6b678e5633721_prof);

    }

    // line 338
    public function block_form_rows($context, array $blocks = array())
    {
        $__internal_2285f3c591212ae94a6000085e59f255f975411fd414384fff3fb27b65d624fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2285f3c591212ae94a6000085e59f255f975411fd414384fff3fb27b65d624fb->enter($__internal_2285f3c591212ae94a6000085e59f255f975411fd414384fff3fb27b65d624fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_rows"));

        // line 339
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 340
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($context["child"], 'row');
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_2285f3c591212ae94a6000085e59f255f975411fd414384fff3fb27b65d624fb->leave($__internal_2285f3c591212ae94a6000085e59f255f975411fd414384fff3fb27b65d624fb_prof);

    }

    // line 344
    public function block_widget_attributes($context, array $blocks = array())
    {
        $__internal_a7f8a65a452a83d18bf3c003f7a9c8619a9b8f3c71d43f07d2ea3b72119f7d2c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a7f8a65a452a83d18bf3c003f7a9c8619a9b8f3c71d43f07d2ea3b72119f7d2c->enter($__internal_a7f8a65a452a83d18bf3c003f7a9c8619a9b8f3c71d43f07d2ea3b72119f7d2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "widget_attributes"));

        // line 345
        echo "id=\"";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "\" name=\"";
        echo twig_escape_filter($this->env, (isset($context["full_name"]) ? $context["full_name"] : $this->getContext($context, "full_name")), "html", null, true);
        echo "\"";
        // line 346
        if (((isset($context["read_only"]) ? $context["read_only"] : $this->getContext($context, "read_only")) &&  !$this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "readonly", array(), "any", true, true))) {
            echo " readonly=\"readonly\"";
        }
        // line 347
        if ((isset($context["disabled"]) ? $context["disabled"] : $this->getContext($context, "disabled"))) {
            echo " disabled=\"disabled\"";
        }
        // line 348
        if ((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required"))) {
            echo " required=\"required\"";
        }
        // line 349
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            // line 350
            echo " ";
            // line 351
            if (twig_in_filter($context["attrname"], array(0 => "placeholder", 1 => "title"))) {
                // line 352
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            } elseif ((            // line 353
$context["attrvalue"] === true)) {
                // line 354
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "\"";
            } elseif ( !(            // line 355
$context["attrvalue"] === false)) {
                // line 356
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_a7f8a65a452a83d18bf3c003f7a9c8619a9b8f3c71d43f07d2ea3b72119f7d2c->leave($__internal_a7f8a65a452a83d18bf3c003f7a9c8619a9b8f3c71d43f07d2ea3b72119f7d2c_prof);

    }

    // line 361
    public function block_widget_container_attributes($context, array $blocks = array())
    {
        $__internal_139eb33519a216ee351477a21e973996f5861dcacead379376a337e5af10f1f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_139eb33519a216ee351477a21e973996f5861dcacead379376a337e5af10f1f6->enter($__internal_139eb33519a216ee351477a21e973996f5861dcacead379376a337e5af10f1f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "widget_container_attributes"));

        // line 362
        if ( !twig_test_empty((isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")))) {
            echo "id=\"";
            echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
            echo "\"";
        }
        // line 363
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            // line 364
            echo " ";
            // line 365
            if (twig_in_filter($context["attrname"], array(0 => "placeholder", 1 => "title"))) {
                // line 366
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            } elseif ((            // line 367
$context["attrvalue"] === true)) {
                // line 368
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "\"";
            } elseif ( !(            // line 369
$context["attrvalue"] === false)) {
                // line 370
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_139eb33519a216ee351477a21e973996f5861dcacead379376a337e5af10f1f6->leave($__internal_139eb33519a216ee351477a21e973996f5861dcacead379376a337e5af10f1f6_prof);

    }

    // line 375
    public function block_button_attributes($context, array $blocks = array())
    {
        $__internal_4c029c1bb97ed8086f057dbedd04932e3885b9d65abe83c694f8e56c391ccfaf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c029c1bb97ed8086f057dbedd04932e3885b9d65abe83c694f8e56c391ccfaf->enter($__internal_4c029c1bb97ed8086f057dbedd04932e3885b9d65abe83c694f8e56c391ccfaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_attributes"));

        // line 376
        echo "id=\"";
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id")), "html", null, true);
        echo "\" name=\"";
        echo twig_escape_filter($this->env, (isset($context["full_name"]) ? $context["full_name"] : $this->getContext($context, "full_name")), "html", null, true);
        echo "\"";
        if ((isset($context["disabled"]) ? $context["disabled"] : $this->getContext($context, "disabled"))) {
            echo " disabled=\"disabled\"";
        }
        // line 377
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            // line 378
            echo " ";
            // line 379
            if (twig_in_filter($context["attrname"], array(0 => "placeholder", 1 => "title"))) {
                // line 380
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            } elseif ((            // line 381
$context["attrvalue"] === true)) {
                // line 382
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "\"";
            } elseif ( !(            // line 383
$context["attrvalue"] === false)) {
                // line 384
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_4c029c1bb97ed8086f057dbedd04932e3885b9d65abe83c694f8e56c391ccfaf->leave($__internal_4c029c1bb97ed8086f057dbedd04932e3885b9d65abe83c694f8e56c391ccfaf_prof);

    }

    // line 389
    public function block_attributes($context, array $blocks = array())
    {
        $__internal_4adf9b99beb55f417bb35c2543fb9a4a8d40006a694438db21f2a39359ed25e8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4adf9b99beb55f417bb35c2543fb9a4a8d40006a694438db21f2a39359ed25e8->enter($__internal_4adf9b99beb55f417bb35c2543fb9a4a8d40006a694438db21f2a39359ed25e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "attributes"));

        // line 390
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            // line 391
            echo " ";
            // line 392
            if (twig_in_filter($context["attrname"], array(0 => "placeholder", 1 => "title"))) {
                // line 393
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            } elseif ((            // line 394
$context["attrvalue"] === true)) {
                // line 395
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "\"";
            } elseif ( !(            // line 396
$context["attrvalue"] === false)) {
                // line 397
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_4adf9b99beb55f417bb35c2543fb9a4a8d40006a694438db21f2a39359ed25e8->leave($__internal_4adf9b99beb55f417bb35c2543fb9a4a8d40006a694438db21f2a39359ed25e8_prof);

    }

    public function getTemplateName()
    {
        return "PrestaShopBundle:Admin/TwigTemplateForm:form_div_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  1297 => 397,  1295 => 396,  1290 => 395,  1288 => 394,  1283 => 393,  1281 => 392,  1279 => 391,  1275 => 390,  1269 => 389,  1254 => 384,  1252 => 383,  1247 => 382,  1245 => 381,  1240 => 380,  1238 => 379,  1236 => 378,  1232 => 377,  1223 => 376,  1217 => 375,  1202 => 370,  1200 => 369,  1195 => 368,  1193 => 367,  1188 => 366,  1186 => 365,  1184 => 364,  1180 => 363,  1174 => 362,  1168 => 361,  1153 => 356,  1151 => 355,  1146 => 354,  1144 => 353,  1139 => 352,  1137 => 351,  1135 => 350,  1131 => 349,  1127 => 348,  1123 => 347,  1119 => 346,  1113 => 345,  1107 => 344,  1096 => 340,  1092 => 339,  1086 => 338,  1074 => 331,  1072 => 330,  1068 => 329,  1062 => 328,  1054 => 324,  1046 => 322,  1042 => 321,  1040 => 320,  1038 => 319,  1032 => 318,  1023 => 315,  1017 => 314,  1010 => 311,  1007 => 309,  1005 => 308,  999 => 307,  989 => 303,  987 => 302,  963 => 301,  960 => 299,  957 => 297,  955 => 296,  953 => 295,  947 => 294,  940 => 291,  938 => 290,  936 => 289,  930 => 288,  923 => 283,  917 => 282,  910 => 279,  908 => 278,  906 => 277,  900 => 276,  893 => 273,  891 => 272,  889 => 271,  887 => 270,  885 => 269,  879 => 268,  872 => 265,  866 => 260,  855 => 256,  841 => 251,  838 => 250,  836 => 249,  818 => 248,  814 => 245,  811 => 242,  810 => 241,  809 => 240,  807 => 239,  804 => 238,  801 => 236,  798 => 235,  795 => 233,  793 => 232,  791 => 231,  785 => 230,  778 => 225,  776 => 224,  770 => 223,  763 => 220,  761 => 219,  755 => 218,  742 => 215,  738 => 212,  735 => 209,  734 => 208,  733 => 207,  731 => 206,  729 => 205,  723 => 204,  716 => 201,  714 => 200,  708 => 199,  701 => 196,  699 => 195,  693 => 194,  686 => 191,  684 => 190,  678 => 189,  670 => 186,  668 => 185,  662 => 184,  655 => 181,  653 => 180,  647 => 179,  640 => 176,  638 => 175,  632 => 174,  625 => 171,  619 => 170,  612 => 167,  610 => 166,  604 => 165,  597 => 162,  595 => 161,  589 => 159,  581 => 155,  571 => 154,  566 => 153,  564 => 152,  561 => 150,  559 => 149,  553 => 148,  545 => 144,  543 => 142,  542 => 141,  541 => 140,  540 => 139,  536 => 138,  533 => 136,  531 => 135,  525 => 134,  517 => 130,  515 => 129,  513 => 128,  511 => 127,  509 => 126,  505 => 125,  502 => 123,  500 => 122,  494 => 121,  476 => 118,  470 => 117,  442 => 114,  440 => 113,  434 => 112,  405 => 107,  403 => 106,  400 => 104,  398 => 103,  396 => 102,  391 => 101,  389 => 100,  372 => 99,  366 => 98,  359 => 95,  357 => 94,  355 => 93,  349 => 90,  347 => 89,  345 => 88,  343 => 87,  341 => 86,  332 => 84,  330 => 83,  323 => 82,  320 => 80,  318 => 79,  312 => 78,  305 => 75,  299 => 73,  297 => 72,  293 => 71,  289 => 70,  283 => 69,  275 => 65,  272 => 63,  270 => 62,  264 => 61,  253 => 58,  247 => 57,  240 => 54,  237 => 52,  235 => 51,  229 => 50,  222 => 47,  220 => 46,  218 => 45,  215 => 43,  213 => 42,  209 => 41,  203 => 40,  186 => 37,  184 => 36,  178 => 35,  170 => 31,  167 => 29,  165 => 28,  159 => 27,  152 => 389,  150 => 375,  148 => 361,  146 => 344,  144 => 338,  141 => 335,  139 => 328,  137 => 318,  135 => 314,  133 => 307,  131 => 294,  129 => 288,  127 => 282,  125 => 276,  123 => 268,  121 => 260,  119 => 256,  117 => 230,  115 => 223,  113 => 218,  111 => 204,  109 => 199,  107 => 194,  105 => 189,  103 => 184,  101 => 179,  99 => 174,  97 => 170,  95 => 165,  93 => 159,  91 => 148,  89 => 134,  87 => 121,  85 => 117,  83 => 112,  81 => 98,  79 => 78,  77 => 69,  75 => 61,  73 => 57,  71 => 50,  69 => 40,  67 => 35,  65 => 27,);
    }

    public function getSource()
    {
        return "{#**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *#}
{# Widgets #}

{%- block form_widget -%}
    {% if compound %}
        {{- block('form_widget_compound') -}}
    {% else %}
        {{- block('form_widget_simple') -}}
    {% endif %}
{%- endblock form_widget -%}

{%- block form_widget_simple -%}
    {%- set type = type|default('text') -%}
    <input type=\"{{ type }}\" {{ block('widget_attributes') }} {% if value is not empty %}value=\"{{ value }}\" {% endif %}/>
{%- endblock form_widget_simple -%}

{%- block form_widget_compound -%}
    <div {{ block('widget_container_attributes') }}>
        {%- if form.parent is empty -%}
            {{ form_errors(form) }}
        {%- endif -%}
        {{- block('form_rows') -}}
        {{- form_rest(form) -}}
    </div>
{%- endblock form_widget_compound -%}

{%- block collection_widget -%}
    {% if prototype is defined %}
        {%- set attr = attr|merge({'data-prototype': form_row(prototype) }) -%}
    {% endif %}
    {{- block('form_widget') -}}
{%- endblock collection_widget -%}

{%- block textarea_widget -%}
    <textarea {{ block('widget_attributes') }}>{{ value }}</textarea>
{%- endblock textarea_widget -%}

{%- block choice_widget -%}
    {% if expanded %}
        {{- block('choice_widget_expanded') -}}
    {% else %}
        {{- block('choice_widget_collapsed') -}}
    {% endif %}
{%- endblock choice_widget -%}

{%- block choice_widget_expanded -%}
    <div {{ block('widget_container_attributes') }}>
        {%- for child in form %}
            {{- form_widget(child) -}}
            {{- form_label(child, null, {translation_domain: choice_translation_domain}) -}}
        {% endfor -%}
    </div>
{%- endblock choice_widget_expanded -%}

{%- block choice_widget_collapsed -%}
    {%- if required and placeholder is none and not placeholder_in_choices and not multiple -%}
        {% set required = false %}
    {%- endif -%}
    <select {{ block('widget_attributes') }}{% if multiple %} multiple=\"multiple\"{% endif %} data-toggle=\"select2\">
        {%- if placeholder is not none -%}
            <option value=\"\"{% if required and value is empty %} selected=\"selected\"{% endif %}>{{ placeholder != '' ? placeholder }}</option>
        {%- endif -%}
        {%- if preferred_choices|length > 0 -%}
            {% set options = preferred_choices %}
            {{- block('choice_widget_options') -}}
            {%- if choices|length > 0 and separator is not none -%}
                <option disabled=\"disabled\">{{ separator }}</option>
            {%- endif -%}
        {%- endif -%}
        {%- set options = choices -%}
        {{- block('choice_widget_options') -}}
    </select>
{%- endblock choice_widget_collapsed -%}

{%- block choice_widget_options -%}
    {% for group_label, choice in options %}
        {%- if choice is iterable -%}
            <optgroup label=\"{{ choice_translation_domain is same as(false) ? group_label : group_label }}\">
                {% set options = choice %}
                {{- block('choice_widget_options') -}}
            </optgroup>
        {%- else -%}
            {% set attr = choice.attr %}
            <option value=\"{{ choice.value }}\" {{ block('attributes') }}{% if choice is selectedchoice(value) %} selected=\"selected\"{% endif %}>{{ choice_translation_domain is same as(false) ? choice.label : choice.label }}</option>
        {%- endif -%}
    {% endfor %}
{%- endblock choice_widget_options -%}

{%- block checkbox_widget -%}
    {% set switch = switch|default('') -%}
    <input type=\"checkbox\" {% if switch %}data-toggle=\"switch\"{% endif %} {% if switch %}class=\"{{ switch }}\"{% endif %} {{ block('widget_attributes') }}{% if value is defined %} value=\"{{ value }}\"{% endif %}{% if checked %} checked=\"checked\"{% endif %} />
{% endblock checkbox_widget -%}

{%- block radio_widget -%}
    <input type=\"radio\" {{ block('widget_attributes') }}{% if value is defined %} value=\"{{ value }}\"{% endif %}{% if checked %} checked=\"checked\"{% endif %} />
{% endblock radio_widget -%}

{%- block datetime_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {%- else -%}
        <div {{ block('widget_container_attributes') }}>
            {{- form_errors(form.date) -}}
            {{- form_errors(form.time) -}}
            {{- form_widget(form.date) -}}
            {{- form_widget(form.time) -}}
        </div>
    {%- endif -%}
{%- endblock datetime_widget -%}

{%- block date_widget -%}
    {%- if widget == 'single_text' -%}
        {{ block('form_widget_simple') }}
    {%- else -%}
        <div {{ block('widget_container_attributes') }}>
            {{- date_pattern|replace({
                '{{ year }}':  form_widget(form.year),
                '{{ month }}': form_widget(form.month),
                '{{ day }}':   form_widget(form.day),
            })|raw -}}
        </div>
    {%- endif -%}
{%- endblock date_widget -%}

{%- block time_widget -%}
    {%- if widget == 'single_text' -%}
        {{ block('form_widget_simple') }}
    {%- else -%}
        {%- set vars = widget == 'text' ? { 'attr': { 'size': 1 }} : {} -%}
        <div {{ block('widget_container_attributes') }}>
            {{ form_widget(form.hour, vars) }}{% if with_minutes %}:{{ form_widget(form.minute, vars) }}{% endif %}{% if with_seconds %}:{{ form_widget(form.second, vars) }}{% endif %}
        </div>
    {%- endif -%}
{%- endblock time_widget -%}

{%- block number_widget -%}
    {# type=\"number\" doesn't work with floats #}
    {%- set type = type|default('text') -%}
    {{ block('form_widget_simple') }}
{%- endblock number_widget -%}

{%- block integer_widget -%}
    {%- set type = type|default('number') -%}
    {{ block('form_widget_simple') }}
{%- endblock integer_widget -%}

{%- block money_widget -%}
    {{ money_pattern|replace({ '{{ widget }}': block('form_widget_simple') })|raw }}
{%- endblock money_widget -%}

{%- block url_widget -%}
    {%- set type = type|default('url') -%}
    {{ block('form_widget_simple') }}
{%- endblock url_widget -%}

{%- block search_widget -%}
    {%- set type = type|default('search') -%}
    {{ block('form_widget_simple') }}
{%- endblock search_widget -%}

{%- block percent_widget -%}
    {%- set type = type|default('text') -%}
    {{ block('form_widget_simple') }} %
{%- endblock percent_widget -%}

{%- block password_widget -%}
    {%- set type = type|default('password') -%}
    {{ block('form_widget_simple') }}
{%- endblock password_widget -%}

{%- block hidden_widget -%}
    {%- set type = type|default('hidden') -%}
    {{ block('form_widget_simple') }}
{%- endblock hidden_widget -%}

{%- block email_widget -%}
    {%- set type = type|default('email') -%}
    {{ block('form_widget_simple') }}
{%- endblock email_widget -%}

{%- block button_widget -%}
    {%- if label is empty -%}
        {%- if label_format is not empty -%}
            {% set label = label_format|replace({
            '%name%': name,
            '%id%': id,
            }) %}
        {%- else -%}
            {% set label = name|humanize %}
        {%- endif -%}
    {%- endif -%}
    <button type=\"{{ type|default('button') }}\" {{ block('button_attributes') }}>{{ label }}</button>
{%- endblock button_widget -%}

{%- block submit_widget -%}
    {%- set type = type|default('submit') -%}
    {{ block('button_widget') }}
{%- endblock submit_widget -%}

{%- block reset_widget -%}
    {%- set type = type|default('reset') -%}
    {{ block('button_widget') }}
{%- endblock reset_widget -%}

{# Labels #}

{%- block form_label -%}
    {% if label is not same as(false) -%}
        {% if not compound -%}
            {% set label_attr = label_attr|merge({'for': id}) %}
        {%- endif %}
        {% if required -%}
            {% set label_attr = label_attr|merge({'class': (label_attr.class|default('') ~ ' required')|trim}) %}
        {%- endif %}
        {% if label is empty -%}
            {%- if label_format is not empty -%}
                {% set label = label_format|replace({
                    '%name%': name,
                    '%id%': id,
                }) %}
            {%- else -%}
                {% set label = name|humanize %}
            {%- endif -%}
        {%- endif -%}
        <label{% for attrname, attrvalue in label_attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>{{ translation_domain is same as(false) ? label|raw : label|raw }}</label>
        {% if label_attr['tooltip'] is defined %}
            {% set placement = label_attr['tooltip_placement'] is defined ? label_attr['tooltip_placement'] : 'top' %}
            <i class=\"icon-question\" data-toggle=\"tooltip\" data-placement=\"{{ placement }}\" title=\"{{ label_attr['tooltip'] }}\"></i>
        {% endif %}
    {%- endif -%}
{%- endblock form_label -%}

{%- block button_label -%}{%- endblock -%}

{# Rows #}

{%- block repeated_row -%}
    {#
    No need to render the errors here, as all errors are mapped
    to the first child (see RepeatedTypeValidatorExtension).
    #}
    {{- block('form_rows') -}}
{%- endblock repeated_row -%}

{%- block form_row -%}
    <div>
        {{- form_label(form) -}}
        {{- form_errors(form) -}}
        {{- form_widget(form) -}}
    </div>
{%- endblock form_row -%}

{%- block button_row -%}
    <div>
        {{- form_widget(form) -}}
    </div>
{%- endblock button_row -%}

{%- block hidden_row -%}
    {{ form_widget(form) }}
{%- endblock hidden_row -%}

{# Misc #}

{%- block form -%}
    {{ form_start(form) }}
    {{- form_widget(form) -}}
    {{ form_end(form) }}
{%- endblock form -%}

{%- block form_start -%}
    {% set method = method|upper %}
    {%- if method in [\"GET\", \"POST\"] -%}
    {% set form_method = method %}
{%- else -%}
    {% set form_method = \"POST\" %}
{%- endif -%}
    <form name=\"{{ name }}\" method=\"{{ form_method|lower }}\" action=\"{{ action }}\"{% for attrname, attrvalue in attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}{% if multipart %} enctype=\"multipart/form-data\"{% endif %}>
    {%- if form_method != method -%}
    <input type=\"hidden\" name=\"_method\" value=\"{{ method }}\" />
{%- endif -%}
{%- endblock form_start -%}

{%- block form_end -%}
    {%- if not render_rest is defined or render_rest -%}
    {{ form_rest(form) }}
{%- endif -%}
    </form>
{%- endblock form_end -%}

{%- block form_enctype -%}
    {% if multipart %}enctype=\"multipart/form-data\"{% endif %}
{%- endblock form_enctype -%}

{%- block form_errors -%}
    {%- if errors|length > 0 -%}
        <ul>
            {%- for error in errors -%}
                <li>{{ error.message }}</li>
            {%- endfor -%}
        </ul>
    {%- endif -%}
{%- endblock form_errors -%}

{%- block form_rest -%}
    {% for child in form -%}
        {% if not child.rendered %}
            {{- form_row(child) -}}
        {% endif %}
    {%- endfor %}
{% endblock form_rest %}

{# Support #}

{%- block form_rows -%}
    {% for child in form %}
        {{- form_row(child) -}}
    {% endfor %}
{%- endblock form_rows -%}

{%- block widget_attributes -%}
    id=\"{{ id }}\" name=\"{{ full_name }}\"
    {%- if read_only and attr.readonly is not defined %} readonly=\"readonly\"{% endif -%}
    {%- if disabled %} disabled=\"disabled\"{% endif -%}
    {%- if required %} required=\"required\"{% endif -%}
    {%- for attrname, attrvalue in attr -%}
        {{- \" \" -}}
        {%- if attrname in ['placeholder', 'title'] -%}
            {{- attrname }}=\"{{ attrvalue }}\"
        {%- elseif attrvalue is same as(true) -%}
            {{- attrname }}=\"{{ attrname }}\"
        {%- elseif attrvalue is not same as(false) -%}
            {{- attrname }}=\"{{ attrvalue }}\"
        {%- endif -%}
    {%- endfor -%}
{%- endblock widget_attributes -%}

{%- block widget_container_attributes -%}
    {%- if id is not empty %}id=\"{{ id }}\"{% endif -%}
    {%- for attrname, attrvalue in attr -%}
        {{- \" \" -}}
        {%- if attrname in ['placeholder', 'title'] -%}
            {{- attrname }}=\"{{ attrvalue }}\"
        {%- elseif attrvalue is same as(true) -%}
            {{- attrname }}=\"{{ attrname }}\"
        {%- elseif attrvalue is not same as(false) -%}
            {{- attrname }}=\"{{ attrvalue }}\"
        {%- endif -%}
    {%- endfor -%}
{%- endblock widget_container_attributes -%}

{%- block button_attributes -%}
    id=\"{{ id }}\" name=\"{{ full_name }}\"{% if disabled %} disabled=\"disabled\"{% endif -%}
    {%- for attrname, attrvalue in attr -%}
        {{- \" \" -}}
        {%- if attrname in ['placeholder', 'title'] -%}
            {{- attrname }}=\"{{ attrvalue }}\"
        {%- elseif attrvalue is same as(true) -%}
            {{- attrname }}=\"{{ attrname }}\"
        {%- elseif attrvalue is not same as(false) -%}
            {{- attrname }}=\"{{ attrvalue }}\"
        {%- endif -%}
    {%- endfor -%}
{%- endblock button_attributes -%}

{% block attributes -%}
    {%- for attrname, attrvalue in attr -%}
        {{- \" \" -}}
        {%- if attrname in ['placeholder', 'title'] -%}
            {{- attrname }}=\"{{ attrvalue }}\"
        {%- elseif attrvalue is same as(true) -%}
            {{- attrname }}=\"{{ attrname }}\"
        {%- elseif attrvalue is not same as(false) -%}
            {{- attrname }}=\"{{ attrvalue }}\"
        {%- endif -%}
    {%- endfor -%}
{%- endblock attributes -%}
";
    }
}
