<?php

/* __string_template__e500148a711e40bd374e88827b1dc1dd273adc7a7a4f9ab82da4effab167ffa7 */
class __TwigTemplate_8bea18b05fe1688bbd2fff1b1381fa1e8478d023771fbe013dac8b1885c8ccb1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'extra_stylesheets' => array($this, 'block_extra_stylesheets'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
            'content_footer' => array($this, 'block_content_footer'),
            'sidebar_right' => array($this, 'block_sidebar_right'),
            'javascripts' => array($this, 'block_javascripts'),
            'extra_javascripts' => array($this, 'block_extra_javascripts'),
            'translate_javascripts' => array($this, 'block_translate_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bf4f992d8eb9dc0c7938484c7bb7efada46776c626dc129e588990740c225202 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf4f992d8eb9dc0c7938484c7bb7efada46776c626dc129e588990740c225202->enter($__internal_bf4f992d8eb9dc0c7938484c7bb7efada46776c626dc129e588990740c225202_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "__string_template__e500148a711e40bd374e88827b1dc1dd273adc7a7a4f9ab82da4effab167ffa7"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"de\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>Produkte • HPM Warenkorbsystem</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminProducts';
    var iso_user = 'de';
    var full_language_code = 'de';
    var full_cldr_language_code = 'de-DE';
    var country_iso_code = 'DE';
    var _PS_VERSION_ = '1.7.0.4';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Eine neue Bestellung ist in Ihrem Shop eingegangen.';
    var order_number_msg = 'Bestell-Nr. ';
    var total_msg = 'Gesamt: ';
    var from_msg = 'von ';
    var see_order_msg = 'Diese Bestellung anzeigen';
    var new_customer_msg = 'Neue Kundenregistrierung im Shop.';
    var customer_name_msg = 'Kunde ';
    var new_msg = 'Ihr Shop hat eine neue Nachricht erhalten.';
    var see_msg = 'Nachricht lesen';
    var token = 'bc3e3afca146b3cf8a23f6bffa1e1ef9';
    var token_admin_orders = '99b81f32f77b6a580f0bbf7013cc709a';
    var token_admin_customers = '9e0bcce8ef8f3277db83b133caa1398f';
    var token_admin_customer_threads = 'ab8f7b03edb50870fdb1b1601014239d';
    var currentIndex = 'index.php?controller=AdminProducts';
    var employee_token = '8648c74ea631e24fad9461dae16d91bc';
    var choose_language_translate = 'Wählen Sie eine Sprache';
    var default_language = '1';
    var admin_modules_link = '/admin975acnmvl/index.php/module/catalog/recommended?_token=04uE5NwelWAVLMsy-LJsBxl6q8q7fxPX5gMPF59ld6A';
    var tab_modules_list = 'prestagiftvouchers,dmuassocprodcat,etranslation,apiway,prestashoptoquickbooks';
    var update_success_msg = 'Aktualisierung durchgeführt!';
    var errorLogin = 'PrestaShop konnte sich nicht bei Addons anmelden. Überprüfen Sie bitte Ihre Zugangsdaten und Ihre Internetverbindung.';
    var search_product_msg = 'Artikel suchen';
  </script>

      <link href=\"/admin975acnmvl/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/admin975acnmvl/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/admin975acnmvl\\/\";
var baseDir = \"\\/\";
var currency = {\"iso_code\":\"EUR\",\"sign\":\"\\u20ac\",\"name\":\"Euro\",\"format\":\"#,##0.00\\u00a0\\u00a4\"};
var host_mode = false;
</script>
<script type=\"text/javascript\" src=\"/admin975acnmvl/themes/new-theme/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=1.7.0.4\"></script>
<script type=\"text/javascript\" src=\"/js/cldr.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=1.7.0.4\"></script>
<script type=\"text/javascript\" src=\"/admin975acnmvl/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/admin975acnmvl/themes/default/js/vendor/nv.d3.min.js\"></script>


  

";
        // line 69
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>
<body class=\"adminproducts\">



<header>
  <nav class=\"main-header\">

    
    

    
    <a class=\"logo pull-left\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminDashboard&amp;token=9b512ec9ae2eca4fea31a19eaefd3093\"></a>

    <div class=\"component pull-left\"><div class=\"ps-dropdown dropdown\">
  <span type=\"button\" id=\"quick-access\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
    <span class=\"selected-item\">Schnellzugriff</span> <i class=\"material-icons arrow-down\">keyboard_arrow_down</i>
  </span>
  <div class=\"ps-dropdown-menu dropdown-menu\" aria-labelledby=\"quick-access\">
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCategories&amp;addcategory&amp;token=6894d3e321b0452e75b68f9d8381d0a2\"
                 data-item=\"Neue Kategorie\"
      >Neue Kategorie</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php/productnew?token=a6b89c646ef91542bc1816b800faab41\"
                 data-item=\"Neuer Artikel\"
      >Neuer Artikel</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=efc5a2931bb59a184b77e33bb20eaf9d\"
                 data-item=\"Neuer Ermäßigungsgutschein\"
      >Neuer Ermäßigungsgutschein</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=99b81f32f77b6a580f0bbf7013cc709a\"
                 data-item=\"Orders\"
      >Orders</a>
        <hr>
        <a
      class=\"dropdown-item js-quick-link\"
      data-rand=\"176\"
      data-icon=\"icon-AdminCatalog\"
      data-method=\"add\"
      data-url=\"index.php/productform55?-LJsBxl6q8q7fxPX5gMPF59ld6A\"
      data-post-link=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminQuickAccesses&token=3493f79864a5aed3808c21d62f484a3a\"
      data-prompt-text=\"Bitte dieses Kürzel angeben:\"
      data-link=\"Produkte - Liste\"
    >
      <i class=\"material-icons\">add_circle_outline</i>
      Zu Favoriten hinzufügen
    </a>
    <a class=\"dropdown-item\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminQuickAccesses&token=3493f79864a5aed3808c21d62f484a3a\">
      <i class=\"material-icons\">settings</i>
      Schnellzugänge verwalten
    </a>
  </div>
</div>
</div>
    <div class=\"component\">

<form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form\"
      method=\"post\"
      action=\"/admin975acnmvl/index.php?controller=AdminSearch&amp;token=6c409b5a0328abc5bd1998ed7bf16e46\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input id=\"bo_query\" name=\"bo_query\" type=\"search\" class=\"form-control dropdown-form-search js-form-search\" value=\"\" placeholder=\"Suche (z.B. Bestell-Nr., Kundenname ...)\" />
    <div class=\"input-group-addon\">
      <div class=\"dropdown\">
        <span class=\"dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
          Überall
        </span>
        <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu\">
          <ul class=\"items-list js-items-list\">
            <li class=\"search-all search-option active\">
              <a class=\"dropdown-item\" data-item=\"Überall\" href=\"#\" data-value=\"0\" data-placeholder=\"Wonach suchen Sie?\" data-icon=\"icon-search\">
              <i class=\"material-icons\">search</i> Überall</a>
            </li>
            <hr>
            <li class=\"search-book search-option\">
              <a class=\"dropdown-item\" data-item=\"Katalog\" href=\"#\" data-value=\"1\" data-placeholder=\"Artikelname, Bestandseinheit, Artikel-Nr. ...\" data-icon=\"icon-book\">
                <i class=\"material-icons\">library_books</i> Katalog
              </a>
            </li>
            <li class=\"search-customers-name search-option\">
              <a class=\"dropdown-item\" data-item=\"Kunden nach Name\" href=\"#\" data-value=\"2\" data-placeholder=\"E-Mail , Name...\" data-icon=\"icon-group\">
                <i class=\"material-icons\">group</i> Kunden nach Name
              </a>
            </li>
            <li class=\"search-customers-addresses search-option\">
              <a class=\"dropdown-item\" data-item=\"Kunden nach IP-Adresse\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\">
                <i class=\"material-icons\">desktop_windows</i>Kunden nach IP-Adresse</a>
            </li>
            <li class=\"search-orders search-option\">
              <a class=\"dropdown-item\" data-item=\"Bestellungen\" href=\"#\" data-value=\"3\" data-placeholder=\"Bestell-Nr.\" data-icon=\"icon-credit-card\">
                <i class=\"material-icons\">credit_card</i> Bestellungen
              </a>
            </li>
            <li class=\"search-invoices search-option\">
              <a class=\"dropdown-item\" data-item=\"Rechnungen\" href=\"#\" data-value=\"4\" data-placeholder=\"Rechnungsnummer\" data-icon=\"icon-book\">
                <i class=\"material-icons\">book</i></i> Rechnungen
              </a>
            </li>
            <li class=\"search-carts search-option\">
              <a class=\"dropdown-item\" data-item=\"Warenkörbe\" href=\"#\" data-value=\"5\" data-placeholder=\"Warenkorb-ID\" data-icon=\"icon-shopping-cart\">
                <i class=\"material-icons\">shopping_cart</i> Warenkörbe
              </a>
            </li>
            <li class=\"search-modules search-option\">
              <a class=\"dropdown-item\" data-item=\"Module\" href=\"#\" data-value=\"7\" data-placeholder=\"Modul-Name\" data-icon=\"icon-puzzle-piece\">
                <i class=\"material-icons\">view_module</i> Module
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class=\"input-group-addon search-bar\">
      <button type=\"submit\">SUCHE<i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
  });
</script>
</div>


    <div class=\"component pull-md-right -norightmargin\"><div class=\"employee-dropdown dropdown\">
      <div class=\"img-circle person\" data-toggle=\"dropdown\">
      <i class=\"material-icons\">person</i>
    </div>
    <div class=\"dropdown-menu dropdown-menu-right p-a-1 m-r-2\">
    <div class=\"text-xs-center\">
      <img class=\"avatar img-circle\" src=\"https://profile.prestashop.com/martin.schmidt%40pixabit.de.jpg\" /><br>
      Martin Schmidt
    </div>
    <hr>
    <a class=\"employee-link\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminEmployees&amp;token=8648c74ea631e24fad9461dae16d91bc&amp;id_employee=9&amp;updateemployee\" target=\"_blank\">
      <i class=\"material-icons\">settings_applications</i> Ihr Profil
    </a>
    <a class=\"employee-link m-t-1\" id=\"header_logout\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLogin&amp;token=a7b352bae907cf3df04e641e72c91de8&amp;logout\">
      <i class=\"material-icons\">power_settings_new</i> Abmelden
    </a>
  </div>
</div>
</div>
          <div class=\"component pull-md-right\"><div class=\"notification-center dropdown\">
  <div class=\"notification dropdown-toggle\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count\">0</span>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Bestellungen<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Kunden<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Nachrichten<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell kein neuer Kunde!<br>
              Haben Sie die <strong><a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarts&token=a36c134379836369eede72faf392a18c&action=filterOnlyAbandonedCarts\">verwaisten Warenkörbe</a></strong> überprüft?<br>
              Haben Sie in der letzten Zeit Ihre Konversionsrate überprüft?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell kein neuer Kunde!<br>
              Waren Sie in letzter Zeit in den Sozialen Medien aktiv?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell keine neuen Nachrichten <br>
              Ihre Kunden sind offenbar alle zufrieden.
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      von <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"pull-xs-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - Anmelden <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
</div>
        <div class=\"component pull-md-right\">  <div class=\"shop-list\">
    <a class=\"link\" href=\"http://warenkorb.pixabit.de/\" target= \"_blank\">HPM Warenkorbsystem</a>
  </div>
</div>
              <div class=\"component pull-right\">
        <div class=\"shop-state\" id=\"debug-mode\">
          <i class=\"material-icons\">bug_report</i>
          <span class=\"label-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" data-html=\"true\"
            title=\"<p class='text-left text-nowrap'><strong>Ihr Shop befindet sich im Debug-Modus.</strong></p><p class='text-left'>Alle PHP-Fehler und -Nachrichten werden angezeigt. Wenn Sie dies nicht mehr benötigen, wählen Sie die Option <strong>Abschalten</strong>.</p>\">Debug-Modus</span>
        </div>
      </div>
        

    

    
    
  </nav>
</header>

<nav class=\"nav-bar\">
  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\">
            <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminDashboard&amp;token=9b512ec9ae2eca4fea31a19eaefd3093\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Dashboard</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title -active\" data-submenu=\"2\">
              <span class=\"title\">Sell</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"3\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=99b81f32f77b6a580f0bbf7013cc709a\" class=\"link\">
                    <i class=\"material-icons\">shopping_basket</i> <span>Bestellungen</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=99b81f32f77b6a580f0bbf7013cc709a\" class=\"link\"> Bestellungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminInvoices&amp;token=220f0754a44cedc66ecf886234827dd5\" class=\"link\"> Rechnungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminSlip&amp;token=7632567382444d3eec14053ebfb4ff9d\" class=\"link\"> Gutschriften
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminDeliverySlip&amp;token=24648e8a9836c04fb9437fd36d39c789\" class=\"link\"> Lieferscheine
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarts&amp;token=a36c134379836369eede72faf392a18c\" class=\"link\"> Warenkörbe
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone -active\" data-submenu=\"9\">
                  <a href=\"/admin975acnmvl/index.php/product/catalog?_token=04uE5NwelWAVLMsy-LJsBxl6q8q7fxPX5gMPF59ld6A\" class=\"link\">
                    <i class=\"material-icons\">store</i> <span>Katalog</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"10\">
                              <a href=\"/admin975acnmvl/index.php/product/catalog?_token=04uE5NwelWAVLMsy-LJsBxl6q8q7fxPX5gMPF59ld6A\" class=\"link\"> Produkte
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCategories&amp;token=6894d3e321b0452e75b68f9d8381d0a2\" class=\"link\"> Kategorien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminTracking&amp;token=17ed06520e31864bd375829cd4f22816\" class=\"link\"> Kontrollübersicht
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminTabPacklink&amp;token=2988eb6db1ece0cc18b5b32c357a693f\" class=\"link\"> Attributes &amp; Features
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminManufacturers&amp;token=c92e3386aed0b00fa97d01545a1cf16f\" class=\"link\"> Brands &amp; Suppliers
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAttachments&amp;token=937139ad1f208667d1267a43c37b6278\" class=\"link\"> Anhänge
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCartRules&amp;token=efc5a2931bb59a184b77e33bb20eaf9d\" class=\"link\"> Discounts
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"23\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomers&amp;token=9e0bcce8ef8f3277db83b133caa1398f\" class=\"link\">
                    <i class=\"material-icons\">account_circle</i> <span>Kunden</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"24\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomers&amp;token=9e0bcce8ef8f3277db83b133caa1398f\" class=\"link\"> Kunden
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAddresses&amp;token=bc80205a9a0afc76134b5b8d7b269710\" class=\"link\"> Adressen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"26\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOutstanding&amp;token=80a0794b80e24a7c439d9705a6a6cf1b\" class=\"link\"> Offene Forderungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"27\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomerThreads&amp;token=ab8f7b03edb50870fdb1b1601014239d\" class=\"link\">
                    <i class=\"material-icons\">chat</i> <span>Kundenservice</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"28\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomerThreads&amp;token=ab8f7b03edb50870fdb1b1601014239d\" class=\"link\"> Customer Service
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrderMessage&amp;token=e98d027a0cb5a1e9804468dbd71f1465\" class=\"link\"> Bestellnachrichten
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminReturn&amp;token=17eebf0f33863fa262e7e5e0c0c1678e\" class=\"link\"> Warenrücksendungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"31\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminStats&amp;token=e64e4c3d8daa2b3e631766b673018c45\" class=\"link\">
                    <i class=\"material-icons\">assessment</i> <span>Statistiken</span>
                  </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"41\">
              <span class=\"title\">Improve</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"42\">
                  <a href=\"/admin975acnmvl/index.php/module/catalog?_token=04uE5NwelWAVLMsy-LJsBxl6q8q7fxPX5gMPF59ld6A\" class=\"link\">
                    <i class=\"material-icons\">extension</i> <span>Modules</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"43\">
                              <a href=\"/admin975acnmvl/index.php/module/catalog?_token=04uE5NwelWAVLMsy-LJsBxl6q8q7fxPX5gMPF59ld6A\" class=\"link\"> Modules &amp; Services
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"45\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAddonsCatalog&amp;token=3f8c1158ed0f25f7ad352a6cd63d6d49\" class=\"link\"> Modules Catalog
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"46\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminThemes&amp;token=6c2d71e651b3842e6a1b7d740aea2c14\" class=\"link\">
                    <i class=\"material-icons\">desktop_mac</i> <span>Design</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"47\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminThemes&amp;token=6c2d71e651b3842e6a1b7d740aea2c14\" class=\"link\"> Templates &amp; Vorlagen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"48\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminThemesCatalog&amp;token=cdc57566edf2add834d46bebcb1e8d60\" class=\"link\"> Theme Catalog
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"49\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCmsContent&amp;token=a6ebc6417b63883733d4ac89d1abd329\" class=\"link\"> Pages
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"50\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminModulesPositions&amp;token=b57b70af8910da217a0885dfd062a460\" class=\"link\"> Positionen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"51\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminImages&amp;token=0f2879927012c500db42d3f318df4ba3\" class=\"link\"> Image Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"116\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLinkWidget&amp;token=d92b199879128680bf3be20caeb11273\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"52\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarriers&amp;token=1c984b4e0471d88bc1676b65e6acd941\" class=\"link\">
                    <i class=\"material-icons\">local_shipping</i> <span>Versand</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"53\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarriers&amp;token=1c984b4e0471d88bc1676b65e6acd941\" class=\"link\"> Versanddienste
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"54\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminShipping&amp;token=ed0cfa083fdd763aaed2db42c7dea1ab\" class=\"link\"> Voreinstellungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"55\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPayment&amp;token=e13a00dfd9a3d79bf8139bc26afc082a\" class=\"link\">
                    <i class=\"material-icons\">payment</i> <span>Zahlung</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"56\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPayment&amp;token=e13a00dfd9a3d79bf8139bc26afc082a\" class=\"link\"> Payment Methods
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPaymentPreferences&amp;token=7180cfa4d8cf117c253cc9886cb4c7f3\" class=\"link\"> Preferences
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"58\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLocalization&amp;token=6dfae2e824d7266282c86a2aa3d07d65\" class=\"link\">
                    <i class=\"material-icons\">language</i> <span>International</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"59\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLocalization&amp;token=6dfae2e824d7266282c86a2aa3d07d65\" class=\"link\"> Lokalisierung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"64\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCountries&amp;token=f009d54f2f7711092af112b9a2b8b01f\" class=\"link\"> Locations
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"68\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminTaxes&amp;token=d901c48d97be3a81c749ab8220cbaf89\" class=\"link\"> Taxes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"71\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminTranslations&amp;token=0a15cf1689b8154f416d7c093eb9dae5\" class=\"link\"> Übersetzungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"72\">
              <span class=\"title\">Configure</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"73\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPreferences&amp;token=45aa9fac8ded9d3d8601a44aac04fc5d\" class=\"link\">
                    <i class=\"material-icons\">settings</i> <span>Shop Parameters</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"74\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPreferences&amp;token=45aa9fac8ded9d3d8601a44aac04fc5d\" class=\"link\"> Allgemein
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"77\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrderPreferences&amp;token=181ef2c04929c87698f85c40f3c74397\" class=\"link\"> Order Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"80\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPPreferences&amp;token=eb8a1b6f52019e506b0823446b5b6b55\" class=\"link\"> Produkte
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"81\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomerPreferences&amp;token=d25af3d349fc3a2a542c5c335e7b7008\" class=\"link\"> Customer Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"85\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminContacts&amp;token=f8ae4098a78b2bccbc19bb8f4f367430\" class=\"link\"> Contact
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"88\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminMeta&amp;token=48670327b5f1c04247da39cedacf7eca\" class=\"link\"> Traffic
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"92\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminSearchConf&amp;token=e8474b15811c37d308ac6ef099ecbc65\" class=\"link\"> Suche
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"95\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminInformation&amp;token=47756da51418e12ba2921161887f19a9\" class=\"link\">
                    <i class=\"material-icons\">settings_applications</i> <span>Erweiterte Einstellungen</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"96\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminInformation&amp;token=47756da51418e12ba2921161887f19a9\" class=\"link\"> Information
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"97\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPerformance&amp;token=5e929a58121277ffbd3ffc124abd3e54\" class=\"link\"> Leistung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"98\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAdminPreferences&amp;token=67b7b30e87c7b85878f7b7cf5150708c\" class=\"link\"> Verwaltung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"99\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminEmails&amp;token=edc075337984876852ead7be6cbfaba5\" class=\"link\"> E-Mail
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"100\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminImport&amp;token=505093d51ce615fa042242aac95e29b4\" class=\"link\"> Import
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"101\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminEmployees&amp;token=8648c74ea631e24fad9461dae16d91bc\" class=\"link\"> Mitarbeiter
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"105\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminRequestSql&amp;token=d12201d0c59affd14e9a0914886c0704\" class=\"link\"> Database
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"108\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLogs&amp;token=6a4c6d43ebd42ea92769f1efb3e466a2\" class=\"link\"> Log-Dateien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminWebservice&amp;token=ae862098968d7d27f43f6b8f73cb548d\" class=\"link\"> Webdienste
                              </a>
                            </li>

                                                                                                                                                                            </ul>
                                    </li>
                          
        
            </ul>

  <span class=\"menu-collapse\">
    <i class=\"material-icons\">&#xE8EE;</i>
  </span>

  

</nav>


<div id=\"main-div\">

  
        
    <div class=\"content-div -notoolbar\">

      

      

      

      
      
      
      

      <div class=\"row \">
        <div class=\"col-xs-12\">
          <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>




  ";
        // line 932
        $this->displayBlock('content_header', $context, $blocks);
        // line 933
        echo "                 ";
        $this->displayBlock('content', $context, $blocks);
        // line 934
        echo "                 ";
        $this->displayBlock('content_footer', $context, $blocks);
        // line 935
        echo "                 ";
        $this->displayBlock('sidebar_right', $context, $blocks);
        // line 936
        echo "
        </div>
      </div>

    </div>

  
</div>


  <div id=\"footer\" class=\"bootstrap hide\">
<!--
  <div class=\"col-sm-2 hidden-xs\">
    <a href=\"http://www.prestashop.com/\" class=\"_blank\">PrestaShop&trade;</a>
    -
    <span id=\"footer-load-time\"><i class=\"icon-time\" title=\"Ladezeit \"></i> 1.088s</span>
  </div>

  <div class=\"col-sm-2 hidden-xs\">
    <div class=\"social-networks\">
      <a class=\"link-social link-twitter _blank\" href=\"https://twitter.com/PrestaShop\" title=\"Twitter\">
        <i class=\"icon-twitter\"></i>
      </a>
      <a class=\"link-social link-facebook _blank\" href=\"https://www.facebook.com/prestashop\" title=\"Facebook\">
        <i class=\"icon-facebook\"></i>
      </a>
      <a class=\"link-social link-github _blank\" href=\"https://www.prestashop.com/github\" title=\"Github\">
        <i class=\"icon-github\"></i>
      </a>
      <a class=\"link-social link-google _blank\" href=\"https://plus.google.com/+prestashop/\" title=\"Google\">
        <i class=\"icon-google-plus\"></i>
      </a>
    </div>
  </div>
  <div class=\"col-sm-5\">
    <div class=\"footer-contact\">
      <a href=\"http://www.prestashop.com/en/contact_us?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-envelope\"></i>
        Kontakt
      </a>
      /&nbsp;
      <a href=\"http://forge.prestashop.com/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-bug\"></i>
        Bug-Tracker
      </a>
      /&nbsp;
      <a href=\"http://www.prestashop.com/forums/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-comments\"></i>
        Forum
      </a>
      /&nbsp;
      <a href=\"http://addons.prestashop.com/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-puzzle-piece\"></i>
        Addons
      </a>
      /&nbsp;
      <a href=\"http://www.prestashop.com/en/training-prestashop?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-book\"></i>
        Training
      </a>
                </div>
  </div>

  <div class=\"col-sm-3\">
    
  </div>

  <div id=\"go-top\" class=\"hide\"><i class=\"icon-arrow-up\"></i></div>
  -->
</div>



  <div class=\"bootstrap\">
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"http://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-DE&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t
<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/de/login?email=martin.schmidt%40pixabit.de&amp;firstname=Martin&amp;lastname=Schmidt&amp;website=http%3A%2F%2Fwarenkorb.pixabit.de%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/admin975acnmvl/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Verbinden Sie Ihren Shop mit Prestashops Marktplatz, um automatisch alle Kaufmodule zu importieren.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Sie haben noch keinen Account?</h4>
\t\t\t\t\t\t<p class='text-justify'>Entdecken Sie die Vielfalt der PrestaShop Addons! Stöbern Sie im offiziellen PrestaShop Martkplatz mit aktuell über 3 500 innovativen Templates und modularen Erweiterungen - ob es sich nun um Optimierung der Wechselkurse, Erhöhung der Zugriffsrate, Maßnahmen zur Kundenbindung oder Rentabilitätssteigerung handelt.</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Wechseln Sie zu PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link pull-right _blank\" href=\"//addons.prestashop.com/de/forgot-your-password\">Ich habe mein Passwort vergessen</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/de/login?email=martin.schmidt%40pixabit.de&amp;firstname=Martin&amp;lastname=Schmidt&amp;website=http%3A%2F%2Fwarenkorb.pixabit.de%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tErstellen Sie ein Konto
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Anmelden
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>
  </div>

";
        // line 1081
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>
</html>";
        
        $__internal_bf4f992d8eb9dc0c7938484c7bb7efada46776c626dc129e588990740c225202->leave($__internal_bf4f992d8eb9dc0c7938484c7bb7efada46776c626dc129e588990740c225202_prof);

    }

    // line 69
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_f56f62dc558c94afb2f1cf96d1ad6de6f760e84760d7c01358c1d89341df1163 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f56f62dc558c94afb2f1cf96d1ad6de6f760e84760d7c01358c1d89341df1163->enter($__internal_f56f62dc558c94afb2f1cf96d1ad6de6f760e84760d7c01358c1d89341df1163_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_f56f62dc558c94afb2f1cf96d1ad6de6f760e84760d7c01358c1d89341df1163->leave($__internal_f56f62dc558c94afb2f1cf96d1ad6de6f760e84760d7c01358c1d89341df1163_prof);

    }

    public function block_extra_stylesheets($context, array $blocks = array())
    {
        $__internal_a4bb87083d8e94f1c5a2df5c4ccc070146e6f3f8f07be6f07be2263d1283e8e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a4bb87083d8e94f1c5a2df5c4ccc070146e6f3f8f07be6f07be2263d1283e8e2->enter($__internal_a4bb87083d8e94f1c5a2df5c4ccc070146e6f3f8f07be6f07be2263d1283e8e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "extra_stylesheets"));

        
        $__internal_a4bb87083d8e94f1c5a2df5c4ccc070146e6f3f8f07be6f07be2263d1283e8e2->leave($__internal_a4bb87083d8e94f1c5a2df5c4ccc070146e6f3f8f07be6f07be2263d1283e8e2_prof);

    }

    // line 932
    public function block_content_header($context, array $blocks = array())
    {
        $__internal_cdcf84da69903219fe7bd8bdf5e6f73e58c2d836740378f6b2172b19dcd56c5f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cdcf84da69903219fe7bd8bdf5e6f73e58c2d836740378f6b2172b19dcd56c5f->enter($__internal_cdcf84da69903219fe7bd8bdf5e6f73e58c2d836740378f6b2172b19dcd56c5f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        
        $__internal_cdcf84da69903219fe7bd8bdf5e6f73e58c2d836740378f6b2172b19dcd56c5f->leave($__internal_cdcf84da69903219fe7bd8bdf5e6f73e58c2d836740378f6b2172b19dcd56c5f_prof);

    }

    // line 933
    public function block_content($context, array $blocks = array())
    {
        $__internal_8e1c9e9851f9139174ad118f7de91ffe492b1503efc6e792fd0515b96c9998de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e1c9e9851f9139174ad118f7de91ffe492b1503efc6e792fd0515b96c9998de->enter($__internal_8e1c9e9851f9139174ad118f7de91ffe492b1503efc6e792fd0515b96c9998de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_8e1c9e9851f9139174ad118f7de91ffe492b1503efc6e792fd0515b96c9998de->leave($__internal_8e1c9e9851f9139174ad118f7de91ffe492b1503efc6e792fd0515b96c9998de_prof);

    }

    // line 934
    public function block_content_footer($context, array $blocks = array())
    {
        $__internal_a5a1524bbf3b212da0f23c1d4feb5b5007c3f9ceb36b108e74e0657f594063b3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a5a1524bbf3b212da0f23c1d4feb5b5007c3f9ceb36b108e74e0657f594063b3->enter($__internal_a5a1524bbf3b212da0f23c1d4feb5b5007c3f9ceb36b108e74e0657f594063b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_footer"));

        
        $__internal_a5a1524bbf3b212da0f23c1d4feb5b5007c3f9ceb36b108e74e0657f594063b3->leave($__internal_a5a1524bbf3b212da0f23c1d4feb5b5007c3f9ceb36b108e74e0657f594063b3_prof);

    }

    // line 935
    public function block_sidebar_right($context, array $blocks = array())
    {
        $__internal_beb509ee0611730ffba7eaed20fe221010cb2a1a77099a9ba4ea2df5409c8f52 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_beb509ee0611730ffba7eaed20fe221010cb2a1a77099a9ba4ea2df5409c8f52->enter($__internal_beb509ee0611730ffba7eaed20fe221010cb2a1a77099a9ba4ea2df5409c8f52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar_right"));

        
        $__internal_beb509ee0611730ffba7eaed20fe221010cb2a1a77099a9ba4ea2df5409c8f52->leave($__internal_beb509ee0611730ffba7eaed20fe221010cb2a1a77099a9ba4ea2df5409c8f52_prof);

    }

    // line 1081
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_e1993ba9d4388f8517efd83a52d39fe852a5c0e098ff775276dd0dfcf5be7677 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1993ba9d4388f8517efd83a52d39fe852a5c0e098ff775276dd0dfcf5be7677->enter($__internal_e1993ba9d4388f8517efd83a52d39fe852a5c0e098ff775276dd0dfcf5be7677_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_e1993ba9d4388f8517efd83a52d39fe852a5c0e098ff775276dd0dfcf5be7677->leave($__internal_e1993ba9d4388f8517efd83a52d39fe852a5c0e098ff775276dd0dfcf5be7677_prof);

    }

    public function block_extra_javascripts($context, array $blocks = array())
    {
        $__internal_6d7aab14a8cfd909f8123a8c2878d7964418712a2864c5fa01f76d98ef18b0aa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d7aab14a8cfd909f8123a8c2878d7964418712a2864c5fa01f76d98ef18b0aa->enter($__internal_6d7aab14a8cfd909f8123a8c2878d7964418712a2864c5fa01f76d98ef18b0aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "extra_javascripts"));

        
        $__internal_6d7aab14a8cfd909f8123a8c2878d7964418712a2864c5fa01f76d98ef18b0aa->leave($__internal_6d7aab14a8cfd909f8123a8c2878d7964418712a2864c5fa01f76d98ef18b0aa_prof);

    }

    public function block_translate_javascripts($context, array $blocks = array())
    {
        $__internal_a2d08375e1a86d9e0c89dccb382b6fe5269ea57b96123591111a9bffc963d1d8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2d08375e1a86d9e0c89dccb382b6fe5269ea57b96123591111a9bffc963d1d8->enter($__internal_a2d08375e1a86d9e0c89dccb382b6fe5269ea57b96123591111a9bffc963d1d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "translate_javascripts"));

        
        $__internal_a2d08375e1a86d9e0c89dccb382b6fe5269ea57b96123591111a9bffc963d1d8->leave($__internal_a2d08375e1a86d9e0c89dccb382b6fe5269ea57b96123591111a9bffc963d1d8_prof);

    }

    public function getTemplateName()
    {
        return "__string_template__e500148a711e40bd374e88827b1dc1dd273adc7a7a4f9ab82da4effab167ffa7";
    }

    public function getDebugInfo()
    {
        return array (  1202 => 1081,  1191 => 935,  1180 => 934,  1169 => 933,  1158 => 932,  1137 => 69,  1126 => 1081,  979 => 936,  976 => 935,  973 => 934,  970 => 933,  968 => 932,  101 => 69,  31 => 1,);
    }

    public function getSource()
    {
        return "<!DOCTYPE html>
<html lang=\"de\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>Produkte • HPM Warenkorbsystem</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminProducts';
    var iso_user = 'de';
    var full_language_code = 'de';
    var full_cldr_language_code = 'de-DE';
    var country_iso_code = 'DE';
    var _PS_VERSION_ = '1.7.0.4';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Eine neue Bestellung ist in Ihrem Shop eingegangen.';
    var order_number_msg = 'Bestell-Nr. ';
    var total_msg = 'Gesamt: ';
    var from_msg = 'von ';
    var see_order_msg = 'Diese Bestellung anzeigen';
    var new_customer_msg = 'Neue Kundenregistrierung im Shop.';
    var customer_name_msg = 'Kunde ';
    var new_msg = 'Ihr Shop hat eine neue Nachricht erhalten.';
    var see_msg = 'Nachricht lesen';
    var token = 'bc3e3afca146b3cf8a23f6bffa1e1ef9';
    var token_admin_orders = '99b81f32f77b6a580f0bbf7013cc709a';
    var token_admin_customers = '9e0bcce8ef8f3277db83b133caa1398f';
    var token_admin_customer_threads = 'ab8f7b03edb50870fdb1b1601014239d';
    var currentIndex = 'index.php?controller=AdminProducts';
    var employee_token = '8648c74ea631e24fad9461dae16d91bc';
    var choose_language_translate = 'Wählen Sie eine Sprache';
    var default_language = '1';
    var admin_modules_link = '/admin975acnmvl/index.php/module/catalog/recommended?_token=04uE5NwelWAVLMsy-LJsBxl6q8q7fxPX5gMPF59ld6A';
    var tab_modules_list = 'prestagiftvouchers,dmuassocprodcat,etranslation,apiway,prestashoptoquickbooks';
    var update_success_msg = 'Aktualisierung durchgeführt!';
    var errorLogin = 'PrestaShop konnte sich nicht bei Addons anmelden. Überprüfen Sie bitte Ihre Zugangsdaten und Ihre Internetverbindung.';
    var search_product_msg = 'Artikel suchen';
  </script>

      <link href=\"/admin975acnmvl/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/admin975acnmvl/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/admin975acnmvl\\/\";
var baseDir = \"\\/\";
var currency = {\"iso_code\":\"EUR\",\"sign\":\"\\u20ac\",\"name\":\"Euro\",\"format\":\"#,##0.00\\u00a0\\u00a4\"};
var host_mode = false;
</script>
<script type=\"text/javascript\" src=\"/admin975acnmvl/themes/new-theme/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=1.7.0.4\"></script>
<script type=\"text/javascript\" src=\"/js/cldr.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=1.7.0.4\"></script>
<script type=\"text/javascript\" src=\"/admin975acnmvl/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/admin975acnmvl/themes/default/js/vendor/nv.d3.min.js\"></script>


  

{% block stylesheets %}{% endblock %}{% block extra_stylesheets %}{% endblock %}</head>
<body class=\"adminproducts\">



<header>
  <nav class=\"main-header\">

    
    

    
    <a class=\"logo pull-left\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminDashboard&amp;token=9b512ec9ae2eca4fea31a19eaefd3093\"></a>

    <div class=\"component pull-left\"><div class=\"ps-dropdown dropdown\">
  <span type=\"button\" id=\"quick-access\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
    <span class=\"selected-item\">Schnellzugriff</span> <i class=\"material-icons arrow-down\">keyboard_arrow_down</i>
  </span>
  <div class=\"ps-dropdown-menu dropdown-menu\" aria-labelledby=\"quick-access\">
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCategories&amp;addcategory&amp;token=6894d3e321b0452e75b68f9d8381d0a2\"
                 data-item=\"Neue Kategorie\"
      >Neue Kategorie</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php/productnew?token=a6b89c646ef91542bc1816b800faab41\"
                 data-item=\"Neuer Artikel\"
      >Neuer Artikel</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=efc5a2931bb59a184b77e33bb20eaf9d\"
                 data-item=\"Neuer Ermäßigungsgutschein\"
      >Neuer Ermäßigungsgutschein</a>
          <a class=\"dropdown-item\"
         href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=99b81f32f77b6a580f0bbf7013cc709a\"
                 data-item=\"Orders\"
      >Orders</a>
        <hr>
        <a
      class=\"dropdown-item js-quick-link\"
      data-rand=\"176\"
      data-icon=\"icon-AdminCatalog\"
      data-method=\"add\"
      data-url=\"index.php/productform55?-LJsBxl6q8q7fxPX5gMPF59ld6A\"
      data-post-link=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminQuickAccesses&token=3493f79864a5aed3808c21d62f484a3a\"
      data-prompt-text=\"Bitte dieses Kürzel angeben:\"
      data-link=\"Produkte - Liste\"
    >
      <i class=\"material-icons\">add_circle_outline</i>
      Zu Favoriten hinzufügen
    </a>
    <a class=\"dropdown-item\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminQuickAccesses&token=3493f79864a5aed3808c21d62f484a3a\">
      <i class=\"material-icons\">settings</i>
      Schnellzugänge verwalten
    </a>
  </div>
</div>
</div>
    <div class=\"component\">

<form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form\"
      method=\"post\"
      action=\"/admin975acnmvl/index.php?controller=AdminSearch&amp;token=6c409b5a0328abc5bd1998ed7bf16e46\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input id=\"bo_query\" name=\"bo_query\" type=\"search\" class=\"form-control dropdown-form-search js-form-search\" value=\"\" placeholder=\"Suche (z.B. Bestell-Nr., Kundenname ...)\" />
    <div class=\"input-group-addon\">
      <div class=\"dropdown\">
        <span class=\"dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
          Überall
        </span>
        <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu\">
          <ul class=\"items-list js-items-list\">
            <li class=\"search-all search-option active\">
              <a class=\"dropdown-item\" data-item=\"Überall\" href=\"#\" data-value=\"0\" data-placeholder=\"Wonach suchen Sie?\" data-icon=\"icon-search\">
              <i class=\"material-icons\">search</i> Überall</a>
            </li>
            <hr>
            <li class=\"search-book search-option\">
              <a class=\"dropdown-item\" data-item=\"Katalog\" href=\"#\" data-value=\"1\" data-placeholder=\"Artikelname, Bestandseinheit, Artikel-Nr. ...\" data-icon=\"icon-book\">
                <i class=\"material-icons\">library_books</i> Katalog
              </a>
            </li>
            <li class=\"search-customers-name search-option\">
              <a class=\"dropdown-item\" data-item=\"Kunden nach Name\" href=\"#\" data-value=\"2\" data-placeholder=\"E-Mail , Name...\" data-icon=\"icon-group\">
                <i class=\"material-icons\">group</i> Kunden nach Name
              </a>
            </li>
            <li class=\"search-customers-addresses search-option\">
              <a class=\"dropdown-item\" data-item=\"Kunden nach IP-Adresse\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\">
                <i class=\"material-icons\">desktop_windows</i>Kunden nach IP-Adresse</a>
            </li>
            <li class=\"search-orders search-option\">
              <a class=\"dropdown-item\" data-item=\"Bestellungen\" href=\"#\" data-value=\"3\" data-placeholder=\"Bestell-Nr.\" data-icon=\"icon-credit-card\">
                <i class=\"material-icons\">credit_card</i> Bestellungen
              </a>
            </li>
            <li class=\"search-invoices search-option\">
              <a class=\"dropdown-item\" data-item=\"Rechnungen\" href=\"#\" data-value=\"4\" data-placeholder=\"Rechnungsnummer\" data-icon=\"icon-book\">
                <i class=\"material-icons\">book</i></i> Rechnungen
              </a>
            </li>
            <li class=\"search-carts search-option\">
              <a class=\"dropdown-item\" data-item=\"Warenkörbe\" href=\"#\" data-value=\"5\" data-placeholder=\"Warenkorb-ID\" data-icon=\"icon-shopping-cart\">
                <i class=\"material-icons\">shopping_cart</i> Warenkörbe
              </a>
            </li>
            <li class=\"search-modules search-option\">
              <a class=\"dropdown-item\" data-item=\"Module\" href=\"#\" data-value=\"7\" data-placeholder=\"Modul-Name\" data-icon=\"icon-puzzle-piece\">
                <i class=\"material-icons\">view_module</i> Module
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class=\"input-group-addon search-bar\">
      <button type=\"submit\">SUCHE<i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
  });
</script>
</div>


    <div class=\"component pull-md-right -norightmargin\"><div class=\"employee-dropdown dropdown\">
      <div class=\"img-circle person\" data-toggle=\"dropdown\">
      <i class=\"material-icons\">person</i>
    </div>
    <div class=\"dropdown-menu dropdown-menu-right p-a-1 m-r-2\">
    <div class=\"text-xs-center\">
      <img class=\"avatar img-circle\" src=\"https://profile.prestashop.com/martin.schmidt%40pixabit.de.jpg\" /><br>
      Martin Schmidt
    </div>
    <hr>
    <a class=\"employee-link\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminEmployees&amp;token=8648c74ea631e24fad9461dae16d91bc&amp;id_employee=9&amp;updateemployee\" target=\"_blank\">
      <i class=\"material-icons\">settings_applications</i> Ihr Profil
    </a>
    <a class=\"employee-link m-t-1\" id=\"header_logout\" href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLogin&amp;token=a7b352bae907cf3df04e641e72c91de8&amp;logout\">
      <i class=\"material-icons\">power_settings_new</i> Abmelden
    </a>
  </div>
</div>
</div>
          <div class=\"component pull-md-right\"><div class=\"notification-center dropdown\">
  <div class=\"notification dropdown-toggle\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count\">0</span>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Bestellungen<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Kunden<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Nachrichten<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell kein neuer Kunde!<br>
              Haben Sie die <strong><a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarts&token=a36c134379836369eede72faf392a18c&action=filterOnlyAbandonedCarts\">verwaisten Warenkörbe</a></strong> überprüft?<br>
              Haben Sie in der letzten Zeit Ihre Konversionsrate überprüft?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell kein neuer Kunde!<br>
              Waren Sie in letzter Zeit in den Sozialen Medien aktiv?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell keine neuen Nachrichten <br>
              Ihre Kunden sind offenbar alle zufrieden.
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      von <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"pull-xs-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - Anmelden <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
</div>
        <div class=\"component pull-md-right\">  <div class=\"shop-list\">
    <a class=\"link\" href=\"http://warenkorb.pixabit.de/\" target= \"_blank\">HPM Warenkorbsystem</a>
  </div>
</div>
              <div class=\"component pull-right\">
        <div class=\"shop-state\" id=\"debug-mode\">
          <i class=\"material-icons\">bug_report</i>
          <span class=\"label-tooltip\" data-toggle=\"tooltip\" data-placement=\"bottom\" data-html=\"true\"
            title=\"<p class='text-left text-nowrap'><strong>Ihr Shop befindet sich im Debug-Modus.</strong></p><p class='text-left'>Alle PHP-Fehler und -Nachrichten werden angezeigt. Wenn Sie dies nicht mehr benötigen, wählen Sie die Option <strong>Abschalten</strong>.</p>\">Debug-Modus</span>
        </div>
      </div>
        

    

    
    
  </nav>
</header>

<nav class=\"nav-bar\">
  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\">
            <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminDashboard&amp;token=9b512ec9ae2eca4fea31a19eaefd3093\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Dashboard</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title -active\" data-submenu=\"2\">
              <span class=\"title\">Sell</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"3\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=99b81f32f77b6a580f0bbf7013cc709a\" class=\"link\">
                    <i class=\"material-icons\">shopping_basket</i> <span>Bestellungen</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrders&amp;token=99b81f32f77b6a580f0bbf7013cc709a\" class=\"link\"> Bestellungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminInvoices&amp;token=220f0754a44cedc66ecf886234827dd5\" class=\"link\"> Rechnungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminSlip&amp;token=7632567382444d3eec14053ebfb4ff9d\" class=\"link\"> Gutschriften
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminDeliverySlip&amp;token=24648e8a9836c04fb9437fd36d39c789\" class=\"link\"> Lieferscheine
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarts&amp;token=a36c134379836369eede72faf392a18c\" class=\"link\"> Warenkörbe
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone -active\" data-submenu=\"9\">
                  <a href=\"/admin975acnmvl/index.php/product/catalog?_token=04uE5NwelWAVLMsy-LJsBxl6q8q7fxPX5gMPF59ld6A\" class=\"link\">
                    <i class=\"material-icons\">store</i> <span>Katalog</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"10\">
                              <a href=\"/admin975acnmvl/index.php/product/catalog?_token=04uE5NwelWAVLMsy-LJsBxl6q8q7fxPX5gMPF59ld6A\" class=\"link\"> Produkte
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCategories&amp;token=6894d3e321b0452e75b68f9d8381d0a2\" class=\"link\"> Kategorien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminTracking&amp;token=17ed06520e31864bd375829cd4f22816\" class=\"link\"> Kontrollübersicht
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminTabPacklink&amp;token=2988eb6db1ece0cc18b5b32c357a693f\" class=\"link\"> Attributes &amp; Features
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminManufacturers&amp;token=c92e3386aed0b00fa97d01545a1cf16f\" class=\"link\"> Brands &amp; Suppliers
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAttachments&amp;token=937139ad1f208667d1267a43c37b6278\" class=\"link\"> Anhänge
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCartRules&amp;token=efc5a2931bb59a184b77e33bb20eaf9d\" class=\"link\"> Discounts
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"23\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomers&amp;token=9e0bcce8ef8f3277db83b133caa1398f\" class=\"link\">
                    <i class=\"material-icons\">account_circle</i> <span>Kunden</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"24\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomers&amp;token=9e0bcce8ef8f3277db83b133caa1398f\" class=\"link\"> Kunden
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAddresses&amp;token=bc80205a9a0afc76134b5b8d7b269710\" class=\"link\"> Adressen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"26\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOutstanding&amp;token=80a0794b80e24a7c439d9705a6a6cf1b\" class=\"link\"> Offene Forderungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"27\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomerThreads&amp;token=ab8f7b03edb50870fdb1b1601014239d\" class=\"link\">
                    <i class=\"material-icons\">chat</i> <span>Kundenservice</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"28\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomerThreads&amp;token=ab8f7b03edb50870fdb1b1601014239d\" class=\"link\"> Customer Service
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrderMessage&amp;token=e98d027a0cb5a1e9804468dbd71f1465\" class=\"link\"> Bestellnachrichten
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminReturn&amp;token=17eebf0f33863fa262e7e5e0c0c1678e\" class=\"link\"> Warenrücksendungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"31\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminStats&amp;token=e64e4c3d8daa2b3e631766b673018c45\" class=\"link\">
                    <i class=\"material-icons\">assessment</i> <span>Statistiken</span>
                  </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"41\">
              <span class=\"title\">Improve</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"42\">
                  <a href=\"/admin975acnmvl/index.php/module/catalog?_token=04uE5NwelWAVLMsy-LJsBxl6q8q7fxPX5gMPF59ld6A\" class=\"link\">
                    <i class=\"material-icons\">extension</i> <span>Modules</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"43\">
                              <a href=\"/admin975acnmvl/index.php/module/catalog?_token=04uE5NwelWAVLMsy-LJsBxl6q8q7fxPX5gMPF59ld6A\" class=\"link\"> Modules &amp; Services
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"45\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAddonsCatalog&amp;token=3f8c1158ed0f25f7ad352a6cd63d6d49\" class=\"link\"> Modules Catalog
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"46\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminThemes&amp;token=6c2d71e651b3842e6a1b7d740aea2c14\" class=\"link\">
                    <i class=\"material-icons\">desktop_mac</i> <span>Design</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"47\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminThemes&amp;token=6c2d71e651b3842e6a1b7d740aea2c14\" class=\"link\"> Templates &amp; Vorlagen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"48\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminThemesCatalog&amp;token=cdc57566edf2add834d46bebcb1e8d60\" class=\"link\"> Theme Catalog
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"49\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCmsContent&amp;token=a6ebc6417b63883733d4ac89d1abd329\" class=\"link\"> Pages
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"50\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminModulesPositions&amp;token=b57b70af8910da217a0885dfd062a460\" class=\"link\"> Positionen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"51\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminImages&amp;token=0f2879927012c500db42d3f318df4ba3\" class=\"link\"> Image Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"116\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLinkWidget&amp;token=d92b199879128680bf3be20caeb11273\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"52\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarriers&amp;token=1c984b4e0471d88bc1676b65e6acd941\" class=\"link\">
                    <i class=\"material-icons\">local_shipping</i> <span>Versand</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"53\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCarriers&amp;token=1c984b4e0471d88bc1676b65e6acd941\" class=\"link\"> Versanddienste
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"54\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminShipping&amp;token=ed0cfa083fdd763aaed2db42c7dea1ab\" class=\"link\"> Voreinstellungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"55\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPayment&amp;token=e13a00dfd9a3d79bf8139bc26afc082a\" class=\"link\">
                    <i class=\"material-icons\">payment</i> <span>Zahlung</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"56\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPayment&amp;token=e13a00dfd9a3d79bf8139bc26afc082a\" class=\"link\"> Payment Methods
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPaymentPreferences&amp;token=7180cfa4d8cf117c253cc9886cb4c7f3\" class=\"link\"> Preferences
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"58\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLocalization&amp;token=6dfae2e824d7266282c86a2aa3d07d65\" class=\"link\">
                    <i class=\"material-icons\">language</i> <span>International</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"59\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLocalization&amp;token=6dfae2e824d7266282c86a2aa3d07d65\" class=\"link\"> Lokalisierung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"64\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCountries&amp;token=f009d54f2f7711092af112b9a2b8b01f\" class=\"link\"> Locations
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"68\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminTaxes&amp;token=d901c48d97be3a81c749ab8220cbaf89\" class=\"link\"> Taxes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"71\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminTranslations&amp;token=0a15cf1689b8154f416d7c093eb9dae5\" class=\"link\"> Übersetzungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"72\">
              <span class=\"title\">Configure</span>
          </li>

                          
                
                                
                <li class=\"link-levelone \" data-submenu=\"73\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPreferences&amp;token=45aa9fac8ded9d3d8601a44aac04fc5d\" class=\"link\">
                    <i class=\"material-icons\">settings</i> <span>Shop Parameters</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"74\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPreferences&amp;token=45aa9fac8ded9d3d8601a44aac04fc5d\" class=\"link\"> Allgemein
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"77\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminOrderPreferences&amp;token=181ef2c04929c87698f85c40f3c74397\" class=\"link\"> Order Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"80\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPPreferences&amp;token=eb8a1b6f52019e506b0823446b5b6b55\" class=\"link\"> Produkte
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"81\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminCustomerPreferences&amp;token=d25af3d349fc3a2a542c5c335e7b7008\" class=\"link\"> Customer Settings
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"85\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminContacts&amp;token=f8ae4098a78b2bccbc19bb8f4f367430\" class=\"link\"> Contact
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"88\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminMeta&amp;token=48670327b5f1c04247da39cedacf7eca\" class=\"link\"> Traffic
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"92\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminSearchConf&amp;token=e8474b15811c37d308ac6ef099ecbc65\" class=\"link\"> Suche
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                
                <li class=\"link-levelone \" data-submenu=\"95\">
                  <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminInformation&amp;token=47756da51418e12ba2921161887f19a9\" class=\"link\">
                    <i class=\"material-icons\">settings_applications</i> <span>Erweiterte Einstellungen</span>
                  </a>
                                          <ul class=\"submenu\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"96\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminInformation&amp;token=47756da51418e12ba2921161887f19a9\" class=\"link\"> Information
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"97\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminPerformance&amp;token=5e929a58121277ffbd3ffc124abd3e54\" class=\"link\"> Leistung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"98\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminAdminPreferences&amp;token=67b7b30e87c7b85878f7b7cf5150708c\" class=\"link\"> Verwaltung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"99\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminEmails&amp;token=edc075337984876852ead7be6cbfaba5\" class=\"link\"> E-Mail
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"100\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminImport&amp;token=505093d51ce615fa042242aac95e29b4\" class=\"link\"> Import
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"101\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminEmployees&amp;token=8648c74ea631e24fad9461dae16d91bc\" class=\"link\"> Mitarbeiter
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"105\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminRequestSql&amp;token=d12201d0c59affd14e9a0914886c0704\" class=\"link\"> Database
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"108\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminLogs&amp;token=6a4c6d43ebd42ea92769f1efb3e466a2\" class=\"link\"> Log-Dateien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\">
                              <a href=\"https://warenkorb.pixabit.de/admin975acnmvl/index.php?controller=AdminWebservice&amp;token=ae862098968d7d27f43f6b8f73cb548d\" class=\"link\"> Webdienste
                              </a>
                            </li>

                                                                                                                                                                            </ul>
                                    </li>
                          
        
            </ul>

  <span class=\"menu-collapse\">
    <i class=\"material-icons\">&#xE8EE;</i>
  </span>

  

</nav>


<div id=\"main-div\">

  
        
    <div class=\"content-div -notoolbar\">

      

      

      

      
      
      
      

      <div class=\"row \">
        <div class=\"col-xs-12\">
          <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>




  {% block content_header %}{% endblock %}
                 {% block content %}{% endblock %}
                 {% block content_footer %}{% endblock %}
                 {% block sidebar_right %}{% endblock %}

        </div>
      </div>

    </div>

  
</div>


  <div id=\"footer\" class=\"bootstrap hide\">
<!--
  <div class=\"col-sm-2 hidden-xs\">
    <a href=\"http://www.prestashop.com/\" class=\"_blank\">PrestaShop&trade;</a>
    -
    <span id=\"footer-load-time\"><i class=\"icon-time\" title=\"Ladezeit \"></i> 1.088s</span>
  </div>

  <div class=\"col-sm-2 hidden-xs\">
    <div class=\"social-networks\">
      <a class=\"link-social link-twitter _blank\" href=\"https://twitter.com/PrestaShop\" title=\"Twitter\">
        <i class=\"icon-twitter\"></i>
      </a>
      <a class=\"link-social link-facebook _blank\" href=\"https://www.facebook.com/prestashop\" title=\"Facebook\">
        <i class=\"icon-facebook\"></i>
      </a>
      <a class=\"link-social link-github _blank\" href=\"https://www.prestashop.com/github\" title=\"Github\">
        <i class=\"icon-github\"></i>
      </a>
      <a class=\"link-social link-google _blank\" href=\"https://plus.google.com/+prestashop/\" title=\"Google\">
        <i class=\"icon-google-plus\"></i>
      </a>
    </div>
  </div>
  <div class=\"col-sm-5\">
    <div class=\"footer-contact\">
      <a href=\"http://www.prestashop.com/en/contact_us?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-envelope\"></i>
        Kontakt
      </a>
      /&nbsp;
      <a href=\"http://forge.prestashop.com/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-bug\"></i>
        Bug-Tracker
      </a>
      /&nbsp;
      <a href=\"http://www.prestashop.com/forums/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-comments\"></i>
        Forum
      </a>
      /&nbsp;
      <a href=\"http://addons.prestashop.com/?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-puzzle-piece\"></i>
        Addons
      </a>
      /&nbsp;
      <a href=\"http://www.prestashop.com/en/training-prestashop?utm_source=back-office&amp;utm_medium=footer&amp;utm_campaign=back-office-DE&amp;utm_content=download\" class=\"footer_link _blank\">
        <i class=\"icon-book\"></i>
        Training
      </a>
                </div>
  </div>

  <div class=\"col-sm-3\">
    
  </div>

  <div id=\"go-top\" class=\"hide\"><i class=\"icon-arrow-up\"></i></div>
  -->
</div>



  <div class=\"bootstrap\">
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"http://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-DE&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t
<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/de/login?email=martin.schmidt%40pixabit.de&amp;firstname=Martin&amp;lastname=Schmidt&amp;website=http%3A%2F%2Fwarenkorb.pixabit.de%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/admin975acnmvl/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Verbinden Sie Ihren Shop mit Prestashops Marktplatz, um automatisch alle Kaufmodule zu importieren.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Sie haben noch keinen Account?</h4>
\t\t\t\t\t\t<p class='text-justify'>Entdecken Sie die Vielfalt der PrestaShop Addons! Stöbern Sie im offiziellen PrestaShop Martkplatz mit aktuell über 3 500 innovativen Templates und modularen Erweiterungen - ob es sich nun um Optimierung der Wechselkurse, Erhöhung der Zugriffsrate, Maßnahmen zur Kundenbindung oder Rentabilitätssteigerung handelt.</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Wechseln Sie zu PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link pull-right _blank\" href=\"//addons.prestashop.com/de/forgot-your-password\">Ich habe mein Passwort vergessen</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/de/login?email=martin.schmidt%40pixabit.de&amp;firstname=Martin&amp;lastname=Schmidt&amp;website=http%3A%2F%2Fwarenkorb.pixabit.de%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tErstellen Sie ein Konto
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Anmelden
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>
  </div>

{% block javascripts %}{% endblock %}{% block extra_javascripts %}{% endblock %}{% block translate_javascripts %}{% endblock %}</body>
</html>";
    }
}
