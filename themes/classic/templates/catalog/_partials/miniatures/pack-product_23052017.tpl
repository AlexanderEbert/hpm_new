{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<article>
  <div class="card">
    <div class="pack-product-container">
        
       
  <!-- Checkbox f�r sp�tere Auswahlfunktion -->
      <div class="pack-product-price">
        <span><input type="checkbox" id="produktpaket_{$product.id}" name="productpaketid[]" value="{$product.id}" checked="checked" onchange="changeSubtotal('{$product.price}','produktpaket_{$product.id}');"/></span>
      </div>
     
        <div class="pack-product-quantity">
      </div>

      <div class="thumb-mask">
        <div class="mask">
          <img
            src = "{$product.cover.medium.url}"
            alt = "{$product.cover.legend}"
            data-full-size-image-url = "{$product.cover.large.url}"
          >
        </div>
      </div>
      <div class="pack-product-name">
        {$product.name}
      </div>
      <div class="pack-product-price">
        <strong>{$product.price}</strong>
      </div>
      <div class="pack-product-quantity">
        <span>x {$product.pack_quantity}</span>
      </div>
    </div>
  </div>
</article>

      
<script>

    window.onload = function(){
         
        var subtotal = document.getElementById('idSubtotal').innerHTML;
        var total = document.getElementById('productmainidsumme').innerHTML;
        
         total = total.replace('.','').replace(',','.');
        
        if (total == 'undefined' || total == null || total == ''){
            total = 0;
        }
        if (subtotal == 'undefined' || subtotal == null || subtotal == ''){
            subtotal = 0;
        }
       
        total = parseFloat(total) + parseFloat(subtotal);
        
        total = ""+Number(total.toFixed(2));
        total = total.replace('.',',');
        document.getElementById('productmainidsumme').innerHTML = total+" &euro;"; 
          
    };

    function changeSubtotal(price, id){
        
        var subtotal = document.getElementById('idSubtotal').innerHTML;
        var total = document.getElementById('productmainidsumme').innerHTML;
        
        price = price.replace('.','').replace(',','.');
        total = total.replace('.','').replace(',','.');
        subtotal = subtotal.replace('.','').replace(',','.');
        
        if (total == 'undefined' || total == null || total == ''){
            total = 0;
        }
        if (subtotal == 'undefined' || subtotal == null || subtotal == ''){
            subtotal = 0;
        }
        
        if (price == 'undefined' || price == null || price == ''){
            price = 0;
        }
                
        if (document.getElementById(id).checked == true){
            total = parseFloat(total) + parseFloat(price);
            subtotal = parseFloat(subtotal) + parseFloat(price);
        }
        else {
            total = parseFloat(total) - parseFloat(price);
            subtotal = parseFloat(subtotal) - parseFloat(price);
        }
        
        subtotal = ""+Number(subtotal.toFixed(2));
        subtotal = subtotal.replace('.',',');
        document.getElementById('idSubtotal').innerHTML = subtotal+" &euro;";
        
        total = ""+Number(total.toFixed(2));
        total = total.replace('.',',');
        document.getElementById('productmainidsumme').innerHTML = total+" &euro;";
    }
</script>