{extends file='checkout/_partials/steps/checkout-step.tpl'}

{block name='step_content'}
  

    <p class="identity">
      Zusatzinformationen zur Bestellung
    </p>
<form
        class="clearfix"
        id="js-delivery"
        data-url-update="{url entity='order' params=['ajax' => 1, 'action' => 'selectOrdertypeOption']}"
        method="post"
      >
    <label for="test2">Art</label> 
            <select  name="order_type">
                <option  value="">Keine Auswahl</option>
                <option {if $order_type == "Cloud Integration"} selected {/if} value="Cloud Integration">Cloud Integration</option>
                <option {if $order_type == "Cloud Standort"} selected {/if} value="Cloud Standort">Ware f&uuml;r Cloud Standort</option>
                <option {if $order_type == "Standard"} selected {/if} value="Standard">Ware f&uuml;r nicht Cloud Standort</option>
                <option {if $order_type == "Projekt"} selected {/if} value="Projekt">Ware f&uuml;r Projekt</option>
            </select>

    <label for="test">Jira-Ticket</label> 
        <input type="text" placeholder="Ticket-ID" name="jira_ticket" value ="{$jira_ticket}">

        <button type="submit" class="continue btn btn-primary pull-xs-right" name="confirmOrdertypeOption" value="1">
          {l s='Continue' d='Shop.Theme.Actions'}
        </button>
      </form>
{/block}
