{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

 <select id="id_address_{$type}"  name="id_address_{$type}" class="filter" onchange="toggleAddress('{$type}');"> 
     {foreach $addresses as $address}
    <option value="{$address.id}" {if $address.id == $selected} selected{/if}>{$address.alias}</option>
     {/foreach}
 </select><p>&nbsp;</p>
   
{foreach $addresses as $address name=smartyloop}
  
  <article
    class="address-item{if $address.id == $selected} selected{/if} address-{$type}-items address-{$type}-item-{$address.id}"
    id="{$name|classname}-address-{$address.id}" style="display:{if $address.id == $selected}block{else}none{/if};">
    <header class="h4">
      <label class="radio-block">
        {*
        id="{$name|classname}-address-{$address.id}" style="display:{if $smarty.foreach.smartyloop.iteration == 1}block{else}block{/if};">
        <span class="custom-radio">
          <input
            type="radio"
            name="{$name}"
            value="{$address.id}"
            {if $address.id == $selected}checked{/if}
          >
          <span></span>
        </span>
        *}
        <span class="address-alias h4">{$address.alias}</span>
        <div class="address">{$address.formatted nofilter}</div>
      </label>
    </header>
    <hr>
    <footer class="address-footer">
      {if $interactive && $address.id_art == 1}
        <a
          class="edit-address text-muted"
          data-link-action="edit-address"
          href="{url entity='order' params=['id_address' => $address.id, 'editAddress' => $type, 'token' => $token]}"
        >
          <i class="material-icons edit">&#xE254;</i>{l s='Edit' d='Shop.Theme.Actions'}
        </a>

      {/if}
    </footer>
  </article>
{/foreach}
{if $interactive}
  <p>
    <button class="ps-hidden-by-js form-control-submit center-block" type="submit">{l s='Save' d='Shop.Theme.Actions'}</button>
  </p>
{/if}



<script>
    
 function toggleAddress(type){
   $('.address-'+type+'-items').removeClass('selected');
   $('.address-'+type+'-items').hide();
   var id = $( "#id_address_"+type ).val();
   $('.address-'+type+'-item-'+id).addClass('selected');
   $('.address-'+type+'-item-'+id).show();  
 }   
 
</script>