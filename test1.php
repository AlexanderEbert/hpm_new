<?php
ini_set('SMTP', '10.19.20.20');
ini_set('smtp_port', 25);

$empfaenger = "develop@pixabit.de,warenkorb@pixabit.de,martin.schmidt@pixabit.de"; 
$absender   = "develop@pixabit.de";
$betreff    = "Kanban-System";
 
$header  = "MIME-Version: 1.0\r\n";
$header .= "Content-type: text/html; charset=iso-8859-1\r\n";
$header .= "From: $absender\r\n";
$header .= "X-Mailer: PHP ". phpversion();

$tdstyle = "border-collapse: collapse; border:1px solid #D6D4D4;color:#333;padding:7px";
$odd = 'background-color:#E6E6E6';
$even ='';

 
if (!$link = mysql_connect('localhost', 'root', 'xwad01.')) {
    echo date('d.m.Y H:m:s').' - Kanban-System:'.' Keine Datenbank Verbindung';
    exit;
}

if (!mysql_select_db('shop', $link)) {
    echo date('d.m.Y H:m:s').' - Kanban-System:'.' Konnte Datenbank nicht selektieren';
    exit;
}

$sql    = 'SELECT sa.quantity, sa.ausloese, sa.bestellmenge, p.id_product, pl.name 
			FROM ps_stock_available as sa
			INNER JOIN ps_product as p ON p.id_product = sa.id_product 
			INNER JOIN ps_product_lang as pl ON p.id_product = pl.id_product 
			WHERE sa.quantity <= ausloese';

$result = mysql_query($sql, $link);

if (!$result) {
    echo  date('d.m.Y H:m:s').' - Kanban-System:'.' DB Fehler, konnte die Datenbank nicht abfragen\n MySQL Error:'. mysql_error();
    exit;
}

$message = "Hallo Zusammen, <br/><br/>folgende Artikel muessen Nachbestellt werden:<br/><br/>";	

$message .="<table >
			<tr style=\"$odd \">
				<th style=\"$tdstyle \">Produkt</th>
				<th style=\"$tdstyle \">Lagerbestand</th>
				<th style=\"$tdstyle \">Ausloesemenge</th>
				<th style=\"$tdstyle \">Bestellmenge</th>
			</tr>
			";
$i = 1; 

while ($row = mysql_fetch_assoc($result)) {

	
	$style = ($i % 2) ==  0 ? $odd : $even; 
	$i++;
	
	$message .="
				<tr style=\"$style\">
					<td style=\"$tdstyle\">".$row['name']."</td>
					<td align=\"center\" style=\"$tdstyle\">".$row['quantity']."</td>
					<td align=\"center\" style=\"$tdstyle\">".$row['ausloese']."</td>
					<td align=\"center\" style=\"$tdstyle\">".$row['bestellmenge']."</td>
				</tr>
				";
}

$message .= "</table> <br/><br/>Diese Mail wird automatisch generiert! Bei Rueckfragen an Alexander Ebert wenden.<br/>";

mail( $empfaenger, $betreff, $message, $header);
?>
