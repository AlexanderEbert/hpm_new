<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{packlink}prestashop>packlink_0b97a05d427162a2ee640649ec60cb29'] = 'Packlink PRO Versand';
$_MODULE['<{packlink}prestashop>packlink_ef18c7cd5a3502ed4bd1fbe0711f5b30'] = 'Sparen Sie bis zu 70% in Ihrem Versand. Kein Mindestbuchungsvolumen oder Versandlabelkosten. Zentralisieren Sie alle Ihre Versandaufträge auf einem einzigen Kontrollsystem. ';
$_MODULE['<{packlink}prestashop>packlink_462390017ab0938911d2d4e964c0cab7'] = 'Ihre Einstellungen wurden erfolgreich aktualisiert';
$_MODULE['<{packlink}prestashop>packlink_86d44abc0a7c7d72800225f5c802c76e'] = 'v1.1: Alle versandfertigen Sendungsaufträge werden ab sofort automatisch in Packlink PRO importiert.';
$_MODULE['<{packlink}prestashop>packlink_6ad1638f364f235339f4b634ab14536d'] = 'v1.2: Alle versendeten Inhalte werden automatisch für Ihre Packlink PRO Versandaufträge ausgefüllt.';
$_MODULE['<{packlink}prestashop>packlink_8d8dcf11f578fbd84885523b99dba612'] = 'v1.3: Versanddetails und Trackingnummer werden automatisch in PrestaShop Bestellungen importiert. Automatische Befüllung der fehlenden Produktdaten im Katalog (Gewicht/Maße)';
$_MODULE['<{packlink}prestashop>packlink_d5958a20aeb6864da743668bc8c987b5'] = 'v1.4: Synchronisierung der Packlink PRO Versandstatuse mit PrestaShop, um Ihre Sendungen auf den aktuellsten Stand zu haben.';
$_MODULE['<{packlink}prestashop>packlink_10f272913bfc6bcfefbffb97c8aa5b64'] = 'v1.5: Neugestaltung der Einstellungsseite. Es wird die Adresse gewählt, die Sie in Packlink PRO als \"Voreingestellte Adresse\" konfiguriert haben';
$_MODULE['<{packlink}prestashop>packlink_44749712dbec183e983dcd78a7736c41'] = 'Datum';
$_MODULE['<{packlink}prestashop>packlink_914419aa32f04011357d3b604a86d7eb'] = 'Versanddienstleister';
$_MODULE['<{packlink}prestashop>packlink_8c489d0946f66d17d73f26366a4bf620'] = 'Gewicht';
$_MODULE['<{packlink}prestashop>packlink_9f06b28a40790c4c4df5739bce3c1eb0'] = 'Versandkosten';
$_MODULE['<{packlink}prestashop>packlink_5068c162a60b5859f973f701333f45c5'] = 'Sendungs nummer';
$_MODULE['<{packlink}prestashop>packlink_956e128ed709e8e17ce6f5e66af145b9'] = 'In Packlink PRO ansehen';
$_MODULE['<{packlink}prestashop>back_263494a530bf342ae23ec21d28ed6f21'] = 'Versenden Sie Ihre versandfertigen Sendungsaufträge einfach und immer mit den besten Preisen auf Packlink PRO. Sie haben noch keinen Packlink PRO Account? ';
$_MODULE['<{packlink}prestashop>back_e0f179c9505c59254879b6ca513d35b9'] = 'Registrieren Sie ';
$_MODULE['<{packlink}prestashop>back_bb38b5c72a367e0fbbf98bfe4efdcbc2'] = 'sich kostenlos und in wenigen Sekungen.';
$_MODULE['<{packlink}prestashop>back_ad2376beebecdcf7846ba973fa1a005b'] = 'Einstellungen';
$_MODULE['<{packlink}prestashop>back_7d06182c98480873fd25664fb3f7a698'] = 'Absenderadresse';
$_MODULE['<{packlink}prestashop>back_33af8066d3c83110d4bd897f687cedd2'] = 'Versandstatus';
$_MODULE['<{packlink}prestashop>back_8d060c6c1f26e42643ba8d942ce8bb97'] = 'Dateneinheit';
$_MODULE['<{packlink}prestashop>back_3d3d0e1cf8a4804562a5f3b14a93218a'] = 'Hilfeportal';
$_MODULE['<{packlink}prestashop>back_3f184c818991971619eac510c58db516'] = 'PACKLINK PRO INTEGRATION';
$_MODULE['<{packlink}prestashop>back_c61086ce5733d930280e177e241460bf'] = 'Der Packlink PRO API-Schlüssel, welcher mit Ihrem Packlink PRO Benutzerprofil verknüpft ist, muss im unten genannten Feld eingepflegt werden. So können Sie alle Ihre versandbereiten Sendungsaufträge automatisch von Ihrem PrestaShop-Konto importiert werden. ';
$_MODULE['<{packlink}prestashop>back_0de704e37355374d02208e081a5452c6'] = 'Generieren Sie jetzt Ihren Packlink PRO API-Schlüssel.';
$_MODULE['<{packlink}prestashop>back_8121ed5d107fbbe4f4f5d4d2b889adbe'] = 'Packlink Pro API-Schlüssel';
$_MODULE['<{packlink}prestashop>back_c9cc8cce247e49bae79f15173ce97354'] = 'Speichern';
$_MODULE['<{packlink}prestashop>back_fe4c4ddd503c10a8fe9a8249dc1a2336'] = 'Absenderadresse';
$_MODULE['<{packlink}prestashop>back_6f7af3b914733e1f5ba45105287be347'] = 'Die Absenderadresse hilft Ihnen während des Versandprozesses Zeit zu sparen, indem diese Information automatisch in Packlink PRO ausgefüllt wird. Sie können dies in den';
$_MODULE['<{packlink}prestashop>back_2810557faa3bc1dd29cc1641541d4519'] = 'Packlink PRO Einstellungen ändern.  ';
$_MODULE['<{packlink}prestashop>back_e9f7009a3509f4da8ce78cbad712b8a5'] = 'Voreingestellt';
$_MODULE['<{packlink}prestashop>back_3e35c6d17b3f41d65732a32e85eb0c0d'] = 'Telefonnummer: ';
$_MODULE['<{packlink}prestashop>back_151994a8fad78d8d91387ac8c7885475'] = 'Die Absenderadresse wurde nicht auf Packlink PRO eingestellt! ';
$_MODULE['<{packlink}prestashop>back_ec211f7c20af43e742bf2570c3cb84f9'] = 'Hinzufügen';
$_MODULE['<{packlink}prestashop>back_cdf4b324673b77427ca416ac40d3da9a'] = 'Status der Synchronisierung';
$_MODULE['<{packlink}prestashop>back_348b4bdc33672e024a23cd3c12072c5d'] = 'Der Status des PrestaShop Auftrags';
$_MODULE['<{packlink}prestashop>back_3b6a80aad70166de7b5de4943b519c5e'] = 'synchronisiert sich mit dem Packlink PRO Versandstatus so, wie es in der unteren Tabelle eingestellt ist.';
$_MODULE['<{packlink}prestashop>back_afa76985e2458e32f329a1bf2a1ad523'] = 'Sobald sich der Status einer Sendung auf Packlink PRO ändert, aktualisiert sich dieser auch in Ihrem PrestaShop.';
$_MODULE['<{packlink}prestashop>back_3a96c81e606c0602b9fee629a0eeef24'] = 'Packlink PRO Versandstatus';
$_MODULE['<{packlink}prestashop>back_0c573dd42480d097bee61cdc975e16d8'] = 'Status des PrestaShop Auftrags ';
$_MODULE['<{packlink}prestashop>back_158bc559027a1bc2827e7da0d3ff32cd'] = 'Statut #1';
$_MODULE['<{packlink}prestashop>back_2d13df6f8b5e4c5af9f87e0dc39df69d'] = 'Wartend';
$_MODULE['<{packlink}prestashop>back_f8762460f4735a774ba593d36db8074c'] = '(Keine)';
$_MODULE['<{packlink}prestashop>back_6e45ffbef4b733a0b988165fc7cba296'] = 'Statut #2';
$_MODULE['<{packlink}prestashop>back_643562a9ae7099c8aabfdc93478db117'] = 'In Bearbeitung';
$_MODULE['<{packlink}prestashop>back_f4c513dd3babc5917becbdaf74fe7991'] = 'Statut #3';
$_MODULE['<{packlink}prestashop>back_de04ee99badd303f6b87abe736b3a973'] = 'Versandfertig';
$_MODULE['<{packlink}prestashop>back_26051d4300f2c053a39df713ef1ca675'] = 'Statut #4';
$_MODULE['<{packlink}prestashop>back_7ec4f8b296984ffe6ea829b7e1743577'] = 'Im Transit';
$_MODULE['<{packlink}prestashop>back_5ef6c1599201631174cbad0330aa6462'] = 'Statut #5';
$_MODULE['<{packlink}prestashop>back_67edd3b99247c9eb5884a02802a20fa7'] = 'Zugestellt';
$_MODULE['<{packlink}prestashop>back_5405b90a3e049fb630e63305d34ec924'] = 'Maßeinheiten umrechnen';
$_MODULE['<{packlink}prestashop>back_41e51bb942c3ed91a5ba95ef86977a7e'] = 'Packlink PRO funktioniert mit Kilogramm und Zentimetern. Ihr PrestaShop ist möglicherweise mit anderen';
$_MODULE['<{packlink}prestashop>back_ece834d9839ea190d2551135ada79921'] = 'Maßeinheit';
$_MODULE['<{packlink}prestashop>back_4493a6ab1434295fc2ee81980ee139a4'] = 'Bitte vergewissern Sie sich, dass die untere Tabelle korrekt ist, damit der Datenimport von PrestaShop zu Packlink PRO funktioniert.';
$_MODULE['<{packlink}prestashop>back_af28d67cbda82fc994e27524c43a7b6b'] = 'Gewichtseinheiten';
$_MODULE['<{packlink}prestashop>back_ebe86682666f2ab3da0843ed3097e4b3'] = 'kg';
$_MODULE['<{packlink}prestashop>back_76019d8b34c330c0dcca0bc489085d33'] = 'Abmaße';
$_MODULE['<{packlink}prestashop>back_820eb5b696ea2a657c0db1e258dc7d81'] = 'cm';
$_MODULE['<{packlink}prestashop>back_a44416bcc8b9c0109f9a895b79970482'] = 'Produkt Daten automatisch ausfüllen';
$_MODULE['<{packlink}prestashop>back_54fe3f3982ca0e1f6f497d7c6a320dab'] = 'Automatisch Gewicht und Maße in das PrestaShop Katalog von Packlink PRO einfügen, falls diese nicht für ein zu versendenes Produkt, verfügbar ist:';
$_MODULE['<{packlink}prestashop>back_68eec46437c384d8dad18d5464ebc35c'] = 'Immer';
$_MODULE['<{packlink}prestashop>back_6e7b34fa59e1bd229b207892956dc41c'] = 'Nie';
$_MODULE['<{packlink}prestashop>order_details_e7fae27fac3ad0e64be43219f5f4fd17'] = 'Neu gewählter Versanddienstleister';
$_MODULE['<{packlink}prestashop>order_details_e6e5fb296b6df4c8f7f831c7d3412240'] = 'Ihr ausgewählter Versanddienstleister';
$_MODULE['<{packlink}prestashop>order_details_4706040ad816e058d36a721e35301423'] = 'Der von uns ausgewählte Versanddienstleister';
$_MODULE['<{packlink}prestashop>order_details_44749712dbec183e983dcd78a7736c41'] = 'Datum';
$_MODULE['<{packlink}prestashop>order_details_914419aa32f04011357d3b604a86d7eb'] = ' Träger';
$_MODULE['<{packlink}prestashop>order_details_8c489d0946f66d17d73f26366a4bf620'] = 'Gewicht';
$_MODULE['<{packlink}prestashop>order_details_5068c162a60b5859f973f701333f45c5'] = 'Sendungs nummer';
