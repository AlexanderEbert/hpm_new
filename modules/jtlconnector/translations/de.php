<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{jtlconnector}prestashop>jtlconnector_2f2955ae26ff6ad87f627976b738cc44'] = 'Dieses Modul ermöglicht die Verbindung zwischen PrestaShop und der JTL Wawi.';
$_MODULE['<{jtlconnector}prestashop>jtlconnector_a00be4a2eb90a920659117b0fb1e4dc7'] = 'Passwort muss mindestens 8 Zeichen haben!';
$_MODULE['<{jtlconnector}prestashop>jtlconnector_9a7e58ee13cb727cb7c4acc68f4cafe5'] = 'Einstellungen wurden erfolgreich gespeichert.';
$_MODULE['<{jtlconnector}prestashop>jtlconnector_c762caa5a1828056eb6659b5cf95227a'] = 'Connector Einstellungen';
$_MODULE['<{jtlconnector}prestashop>jtlconnector_078ba319869505711dea75f34032ada3'] = 'Bitte geben Sie bei der Einrichtung des Connectors in der Wawi folgende URL an:';
$_MODULE['<{jtlconnector}prestashop>jtlconnector_dc647eb65e6711e155375218212b3964'] = 'Passwort';
$_MODULE['<{jtlconnector}prestashop>jtlconnector_58ed5d76a7fbd9eac4b41d39c33e363c'] = 'Die Datei "%s" muss über Schreibrechte verfügen.';
$_MODULE['<{jtlconnector}prestashop>jtlconnector_24d3598ba951dbad955130cc474ee59c'] = 'Das Verzeichnis "%s" muss über Schreibrechte verfügen.';
$_MODULE['<{jtlconnector}prestashop>jtlconnector_b0aa1879c3d1fc6f8add070de934cae2'] = 'Der Connector benötigt mindestens PHP 5.4. Ihr System läuft auf PHP %s.';
$_MODULE['<{jtlconnector}prestashop>jtlconnector_4a9002a2bb5a1e6402ede5d6e4152a51'] = 'Die benötigte SQLite3 PHP Extension ist nicht installiert.';
$_MODULE['<{jtlconnector}prestashop>jtlconnector_8433cd9cbd61d481a952f707a768b176'] = 'Bitte lesen Sie den %s für die Voraussetzungen und Installations-Anweisungen.';

return $_MODULE;
