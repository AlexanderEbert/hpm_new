<?php
/**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */


use Symfony\Component\Translation\TranslatorInterface;

class CheckoutOrdertypeStepCore extends AbstractCheckoutStep
{
    protected $template = 'checkout/_partials/steps/ordertype.tpl';
    private $jira_ticket = null;
    private $order_type = null;

    public function __construct(
        Context $context,
        TranslatorInterface $translator
    ) {
        parent::__construct($context, $translator);
    }
    
    
      public function getDataToPersist()
    {
        return array(
            'jira_ticket' => $this->jira_ticket,
            'order_type' => $this->order_type,
        );
    }

    public function restorePersistedData(array $data)
    {
        $data = array();
        if (array_key_exists('jira_ticket', $data)) {
            $this->jira_ticket = $data['jira_ticket'];
        }
       if (array_key_exists('order_type', $data)) {
            $this->order_type = $data['order_type'];
        }

        return $data;
    }

    public function handleRequest(array $requestParameters = array())
    {
        // personal info step is always reachable
        $this->step_is_reachable = true;
       
        if (isset($requestParameters['confirmOrdertypeOption'])) {
            
            $this->jira_ticket = null;
            $this->order_type = null;
             
            if (isset($requestParameters['jira_ticket'])) {
                $this->jira_ticket = $requestParameters['jira_ticket'];
            }

            if (isset($requestParameters['order_type'])) {
                $this->order_type = $requestParameters['order_type'];
            }
            
            $result = $this->getCheckoutSession()->setOrderType($this->jira_ticket, $this->order_type );
             
            $this->step_is_complete = true;
            
        }
        else{
            $data = $this->getCheckoutSession()->getOrderType();
            
            if (isset($data['jira_ticket'])) {
                $this->jira_ticket = $data['jira_ticket'];
            }

            if (isset($data['order_type'])) {
                $this->order_type = $data['order_type'];
            }
        }
        
        $this->setTitle(
            $this->getTranslator()->trans(
                'Zusatzinformationen',
                array(),
                'Shop.Theme.Checkout'
            )
        );
    }
    public function getTemplateParameters()
    {
        return array(
             'jira_ticket' => $this->jira_ticket,
            'order_type' => $this->order_type,
        );
    }
    
    
    public function render(array $extraParams = array())
    {
        return $this->renderTemplate(
            $this->getTemplate(), $extraParams, array(
             'jira_ticket' => $this->jira_ticket,
            'order_type' => $this->order_type,
        )
        );
    }
}
